$(document).ready(function(){

    $('input[type=text], input[type=password], textarea').focus(function(){
        $(this).parent().find("label").fadeOut();
    });
    $('input[type=text], input[type=password], textarea').blur(function(){
        if($(this).val() == ""){
            $(this).parent().find("label").fadeIn();
        }
    });
    
    $('#senha_text').val("");
    $('#usuario_text').val("");
});
