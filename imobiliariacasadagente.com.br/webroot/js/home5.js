/*inicialização*/
$(document).ready(function(){
    
    $('#fotos_home').slidesjs({
        width: 614,
        height: 292,
        play: {
          active: false,
          auto: true,
          interval: 4000,
          swap: false,
          pauseOnHover: true,
          restartDelay: 2500
        }
      });
    
});

function envia_busca_home(){

    /*$('#resultado_busca_interna').html("Carregando imóveis...");
    $.ajax({
        url: urlRoot+"imovel/getMaisImoveis3"
        , type: "POST"
        , data: {
            'data[Busca][tipo]': $("#tipo_busca").val()
            , 'data[Busca][codigo]': $("#codigo_busca").val()
            , 'data[Busca][extra]': $("#busca_imovel input:radio:checked").val()
            , 'data[Busca][cidade]': $("#cidade_busca").val()
            , 'data[Busca][bairro]': $("#bairro_busca").val()
            , 'data[Busca][dormitorios]': $("#dormitorios_busca").val()
            , 'data[Busca][garagem]': $("#garagem_busca").val()
            , 'data[Busca][valor_de]': $("#valor_de_busca").val()
            , 'data[Busca][valor_ate]': $("#valor_ate_busca").val()
            , 'data[Busca][offset]': offset
        }, success: function(data){
            $('#resultado_busca_interna').html(innerShiv(data, false));
            var i = 0;
            var animacoes = new Array();
            $(".imovel .imagem").each(function(){

                animacoes[i] = null;
                $(this).attr("animacao1", i);
                i++;
                animacoes[i] = null;
                $(this).attr("animacao2", i);
                i++;

            });
            $(".imovel .imagem").hover(
                function(){
                    
                    var objeto = $(this);
                    clearTimeout(animacoes[objeto.attr("animacao2")]);
                    animacoes[objeto.attr("animacao1")] = setTimeout(function(){
                        $(objeto).parent().find(".informacoes").animate({
                            marginLeft: "300px"
                            , 
                            width: "hide"
                        }, 500);
                    }, 500);

                }, function(){

                    var objeto = $(this);
                    clearTimeout(animacoes[objeto.attr("animacao1")]);
                    animacoes[objeto.attr("animacao2")] = setTimeout(function(){
                        $(objeto).parent().find(".informacoes").animate({
                            marginLeft: "129px"
                            , 
                            width: "show"
                        }, 500);
                    }, 500);

                }
            );
        }
    });*/
    urlBusca = urlRoot+"busca/index/";
    urlBusca += $("#tipo_busca").val()+"/";
    urlBusca += $("#codigo_busca").val()+"/";
    urlBusca += $("#cidade_busca").val()+"/";
    urlBusca += $("#bairro_busca").val()+"/";
    urlBusca += $("#dormitorios_busca").val()+"/";
    urlBusca += $("#garagem_busca").val()+"/";
    urlBusca += $("#valor_de_busca").val()+"/";
    urlBusca += $("#valor_ate_busca").val()+"/";
    urlBusca += $("#busca input:radio:checked").val()+"/0";
    window.location = urlBusca;
    
}