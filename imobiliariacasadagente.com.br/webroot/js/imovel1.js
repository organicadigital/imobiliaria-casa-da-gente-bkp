//busca
function processaBuscaInterna(offset){

    $('#resultado_busca_interna').html("Carregando imóveis...");
    $.ajax({
        url: urlRoot+"imovel/getMaisImoveis3"
        , type: "POST"
        , data: {
            'data[Busca][tipo]': $("#tipo_busca").val()
            , 'data[Busca][codigo]': $("#codigo_busca").val()
            , 'data[Busca][extra]': $("#busca_imovel input:radio:checked").val()
            , 'data[Busca][cidade]': $("#cidade_busca").val()
            , 'data[Busca][bairro]': $("#bairro_busca").val()
            , 'data[Busca][dormitorios]': $("#dormitorios_busca").val()
            , 'data[Busca][garagem]': $("#garagem_busca").val()
            , 'data[Busca][valor_de]': $("#valor_de_busca").val()
            , 'data[Busca][valor_ate]': $("#valor_ate_busca").val()
            , 'data[Busca][offset]': offset
        }, success: function(data){
            $('#resultado_busca_interna').html(innerShiv(data, false));
            var i = 0;
            var animacoes = new Array();
            $(".imovel .imagem").each(function(){

                animacoes[i] = null;
                $(this).attr("animacao1", i);
                i++;
                animacoes[i] = null;
                $(this).attr("animacao2", i);
                i++;

            });
            $(".imovel .imagem").hover(
                function(){
                    
                    var objeto = $(this);
                    clearTimeout(animacoes[objeto.attr("animacao2")]);
                    animacoes[objeto.attr("animacao1")] = setTimeout(function(){
                        $(objeto).parent().find(".informacoes").animate({
                            marginLeft: "300px"
                            , 
                            width: "hide"
                        }, 500);
                    }, 500);

                }, function(){

                    var objeto = $(this);
                    clearTimeout(animacoes[objeto.attr("animacao1")]);
                    animacoes[objeto.attr("animacao2")] = setTimeout(function(){
                        $(objeto).parent().find(".informacoes").animate({
                            marginLeft: "129px"
                            , 
                            width: "show"
                        }, 500);
                    }, 500);

                }
            );
        }
    });
    
}

function processaBuscaInterna2(offset){

    $('#resultado_busca_interna2').html("Carregando imóveis...");
    $.ajax({
        url: urlRoot+"imovel/getMaisImoveis2"
        , type: "POST"
        , data: {
            'data[Busca][offset]': offset
        }, success: function(data){
            $('#resultado_busca_interna2').html(innerShiv(data, false));
            var i = 0;
            var animacoes = new Array();
            $(".imovel .imagem").each(function(){

                animacoes[i] = null;
                $(this).attr("animacao1", i);
                i++;
                animacoes[i] = null;
                $(this).attr("animacao2", i);
                i++;

            });
            $(".imovel .imagem").hover(
                function(){
                    
                    var objeto = $(this);
                    clearTimeout(animacoes[objeto.attr("animacao2")]);
                    animacoes[objeto.attr("animacao1")] = setTimeout(function(){
                        $(objeto).parent().find(".informacoes").animate({
                            marginLeft: "300px"
                            , 
                            width: "hide"
                        }, 500);
                    }, 500);

                }, function(){

                    var objeto = $(this);
                    clearTimeout(animacoes[objeto.attr("animacao1")]);
                    animacoes[objeto.attr("animacao2")] = setTimeout(function(){
                        $(objeto).parent().find(".informacoes").animate({
                            marginLeft: "129px"
                            , 
                            width: "show"
                        }, 500);
                    }, 500);

                }
            );
        }
    });
    
}
function processaBuscaImoveisCondominios(offset, condominio_id){

    $('#resultado_imoveis_condominio').html("Carregando imóveis...");
    $.ajax({
        url: urlRoot+"imovel/getImoveisCondominio"
        , type: "POST"
        , data: {
            'data[Busca][offset]': offset
            , 'data[Busca][condominio_id]': condominio_id
        }, success: function(data){
            $('#resultado_imoveis_condominio').html(innerShiv(data, false));
            var i = 0;
            var animacoes = new Array();
            $(".imovel .imagem").each(function(){

                animacoes[i] = null;
                $(this).attr("animacao1", i);
                i++;
                animacoes[i] = null;
                $(this).attr("animacao2", i);
                i++;

            });
            $(".imovel .imagem").hover(
                function(){
                    
                    var objeto = $(this);
                    clearTimeout(animacoes[objeto.attr("animacao2")]);
                    animacoes[objeto.attr("animacao1")] = setTimeout(function(){
                        $(objeto).parent().find(".informacoes").animate({
                            marginLeft: "300px"
                            , 
                            width: "hide"
                        }, 500);
                    }, 500);

                }, function(){

                    var objeto = $(this);
                    clearTimeout(animacoes[objeto.attr("animacao1")]);
                    animacoes[objeto.attr("animacao2")] = setTimeout(function(){
                        $(objeto).parent().find(".informacoes").animate({
                            marginLeft: "129px"
                            , 
                            width: "show"
                        }, 500);
                    }, 500);

                }
            );
        }
    });
    
}
function processaBuscaImoveisObrasConcluidas(offset, obra_concluida_id){

    $('#resultado_imoveis_obra_concluida').html("Carregando imóveis...");
    $.ajax({
        url: urlRoot+"imovel/getImoveisObraConcluida"
        , type: "POST"
        , data: {
            'data[Busca][offset]': offset
            , 'data[Busca][obra_concluida_id]': obra_concluida_id
        }, success: function(data){
            $('#resultado_imoveis_obra_concluida').html(innerShiv(data, false));
            var i = 0;
            var animacoes = new Array();
            $(".imovel .imagem").each(function(){

                animacoes[i] = null;
                $(this).attr("animacao1", i);
                i++;
                animacoes[i] = null;
                $(this).attr("animacao2", i);
                i++;

            });
            $(".imovel .imagem").hover(
                function(){

                    var objeto = $(this);
                    clearTimeout(animacoes[objeto.attr("animacao2")]);
                    animacoes[objeto.attr("animacao1")] = setTimeout(function(){
                        $(objeto).parent().find(".informacoes").animate({
                            marginLeft: "300px"
                            ,
                            width: "hide"
                        }, 500);
                    }, 500);

                }, function(){

                    var objeto = $(this);
                    clearTimeout(animacoes[objeto.attr("animacao1")]);
                    animacoes[objeto.attr("animacao2")] = setTimeout(function(){
                        $(objeto).parent().find(".informacoes").animate({
                            marginLeft: "129px"
                            ,
                            width: "show"
                        }, 500);
                    }, 500);

                }
            );
        }
    });

}

/*inicialização*/
$(document).ready(function(){
    
    //galeria de imagens
    var pagina_atual_fotos = 0;
    $("#miniaturas ul").width(106*$("#miniaturas ul li").length+"px");
    $("#miniaturas ul li").click(function(){
        $("#miniaturas ul li").removeClass("atual");
        $(this).addClass("atual");
        var url = $(this).find("img").attr("src").substring(0, $(this).find("img").attr("src").length-5)+"1.jpg";
        var img = new Image();
        $(img).load(function(){
            $("#foto_grande").prepend(img);
            $("#foto_grande img").eq(1).fadeOut("none", function(){
                $(this).remove();
            });
        });
        $(img).attr('src', url)
        .attr('alt', 'foto');
    });
    if (!($.browser.msie && parseInt($.browser.version, 10)<8)){
        $("#seta_miniatura_anterior").hover(
            function(){
                $(this).animate({
                    left: '-=3px'
                });
            }, function(){
                $(this).animate({
                    left: '+=3px'
                });
            }
            );
        $("#seta_miniatura_proximo").hover(
            function(){
                $(this).animate({
                    left: '+=3px'
                });
            }, function(){
                $(this).animate({
                    left: '-=3px'
                });
            }
            );
    }
    $("#seta_miniatura_anterior").click(function(){
        var espacamento = 21;
        var largura_miniatura = 106;
        if(pagina_atual_fotos>0){
            pagina_atual_fotos--;
            $("#miniaturas ul").animate({
                marginLeft: '-'+((pagina_atual_fotos*largura_miniatura)+espacamento)+'px'
            });
        }
    });
    $("#seta_miniatura_proximo").click(function(){
        var espacamento = 21;
        var largura_miniatura = 106;
        var visiveis = 4;
        var total_paginas = $("#miniaturas ul li").length-visiveis;
        if(pagina_atual_fotos<total_paginas){
            pagina_atual_fotos++;
            $("#miniaturas ul").animate({
                marginLeft: '-'+((pagina_atual_fotos*largura_miniatura)+espacamento)+'px'
            });
        }
    });
    
    //busca
    processaBuscaInterna(0);
    //imoveis destaque
    processaBuscaInterna2(0);
    
});