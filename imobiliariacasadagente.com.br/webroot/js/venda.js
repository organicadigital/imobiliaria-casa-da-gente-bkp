function submit_venda_seu_imovel(){
    
    $.ajax({
        url: urlRoot+"vendaSeuImovel/processaEnviaVendaSeuImovel"
        , type: "POST"
        , data: {
            'data[Venda][nome]': $('#nome_text').val()
            , 'data[Venda][ddd]': $('#ddd_text').val()
            , 'data[Venda][telefone]': $('#telefone_text').val()
            , 'data[Venda][email]': $('#email_text').val()
            , 'data[Venda][tipo]': $('#tipo_text').val()
            , 'data[Venda][cidade]': $('#cidade_select').val()
        }
        , success: function(data){
            alert(data);
        }
    });
    
}