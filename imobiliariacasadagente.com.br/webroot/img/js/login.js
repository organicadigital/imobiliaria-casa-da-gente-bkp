function submit_login(){
    $.ajax({
        url: urlRoot+"adm/processaLogin"
        , type: "POST"
        , data: {
            'data[Usuario][usuario_text]': $('#usuario_text').val()
            , 'data[Usuario][senha_password]': $('#senha_password').val()
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });
}

$(document).ready(function(){
    $('#usuario_text').val("");
    $('#senha_password').val("");
});