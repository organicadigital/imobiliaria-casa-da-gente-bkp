//busca
function carrega_imoveis(offset){

    $('#resultado_busca_interna').html("Carregando imóveis...");
    $.ajax({
        url: urlRoot+"imovel/getMaisImoveis3"
        , type: "POST"
        , data: {
            'data[Busca][tipo]': $("#tipo_busca").val()
            , 'data[Busca][codigo]': $("#codigo_busca").val()
            , 'data[Busca][extra]': $("#busca_imovel input:radio:checked").val()
            , 'data[Busca][cidade]': $("#cidade_busca").val()
            , 'data[Busca][bairro]': $("#bairro_busca").val()
            , 'data[Busca][dormitorios]': $("#dormitorios_busca").val()
            , 'data[Busca][garagem]': $("#garagem_busca").val()
            , 'data[Busca][valor_de]': $("#valor_de_busca").val()
            , 'data[Busca][valor_ate]': $("#valor_ate_busca").val()
            , 'data[Busca][offset]': offset
        }, success: function(data){
            $('#resultado_busca_interna').html(innerShiv(data, false));
            var i = 0;
            var animacoes = new Array();
            $(".imovel .imagem").each(function(){

                animacoes[i] = null;
                $(this).attr("animacao1", i);
                i++;
                animacoes[i] = null;
                $(this).attr("animacao2", i);
                i++;

            });
            $(".imovel .imagem").hover(
                function(){
                    
                    var objeto = $(this);
                    clearTimeout(animacoes[objeto.attr("animacao2")]);
                    animacoes[objeto.attr("animacao1")] = setTimeout(function(){
                        $(objeto).parent().find(".informacoes").animate({
                            marginLeft: "300px"
                            , 
                            width: "hide"
                        }, 500);
                    }, 500);

                }, function(){

                    var objeto = $(this);
                    clearTimeout(animacoes[objeto.attr("animacao1")]);
                    animacoes[objeto.attr("animacao2")] = setTimeout(function(){
                        $(objeto).parent().find(".informacoes").animate({
                            marginLeft: "129px"
                            , 
                            width: "show"
                        }, 500);
                    }, 500);

                }
            );
        }
    });
    
}