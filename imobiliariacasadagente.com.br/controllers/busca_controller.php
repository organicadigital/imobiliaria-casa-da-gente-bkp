<?php
App::import('Sanitize');
class BuscaController extends AppController {
    
    var $name = 'Busca';
    var $uses = array("Cidade", "Imovel", "Endereco");
    var $helpers = array("Text");
    function beforeFilter() {
        $this->layout = "internas";
        $this->set("enderecos", $this->Endereco->getAll());
    }

    function index($tipo=null, $codigo=null, $cidade=null, $bairro=null, $dormitorios=null, $garagem=null, $de=null, $ate=null, $extra_txt=null, $offset=null) {
        
        $this->set("titulo", "Imóveis Encontrados para Busca de Imóveis");
        //geral
        $cidades2 = $this->Cidade->getAll2();
        $this->set("cidades2", $cidades2);
        $cidades = $this->Cidade->getAll();
        $this->set("cidades", $cidades);
        $this->set("tipos", $this->Imovel->getTipos());
        $tipos_cidade = array();
        foreach($cidades2 as $c){
            $tipos = $this->Cidade->getTipos($c["tb_imoveis"]["nome"]);
            foreach($tipos as $t){
                $tipos_cidade[$c["tb_imoveis"]["nome"]][] = $t;
            }
        }
        $this->set("tipos_cidade", $tipos_cidade);
        
        $this->set("offset", $offset);
        $this->set("javascript", "busca3");
        
        //setando para formulário
        $this->set("busca_tipo", $tipo);
        $this->set("busca_codigo", $codigo);
        $this->set("busca_extra", $extra_txt);
        $this->set("busca_cidade", $cidade);
        $this->set("busca_bairro", $bairro);
        $this->set("busca_dormitorios", $dormitorios);
        $this->set("busca_garagem", $garagem);
        $this->set("busca_valor_de", $de);
        $this->set("busca_valor_ate", $ate);
        
    }
    
    function getBairros($cidade){
        
        $this->layout = "ajax";
        $id = Sanitize::escape($cidade);
        $this->set("bairros", $this->Cidade->getBairros($cidade));
        
    }
    
}
?>