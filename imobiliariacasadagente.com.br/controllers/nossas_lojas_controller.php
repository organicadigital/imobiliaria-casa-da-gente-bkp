<?php
class NossasLojasController extends AppController {
    
    var $name = 'NossasLojas';
    var $uses = array("Cidade", "Imovel", "Texto", "Endereco");
    var $helpers = array("Text");

    function beforeFilter() {
        $this->layout = "internas";
        $this->set("enderecos", $this->Endereco->getAll());
    }

    function index() {

        $this->set("titulo", "Localização");
        $this->set("css", "nossas_lojas");
        $this->set("javascript", "nossas_lojas");
        //geral
        $cidades2 = $this->Cidade->getAll2();
        $this->set("cidades2", $cidades2);
        $cidades = $this->Cidade->getAll();
        $this->set("cidades", $cidades);
        $this->set("tipos", $this->Imovel->getTipos());
        $tipos_cidade = array();
        foreach($cidades2 as $c){
            $tipos = $this->Cidade->getTipos($c["tb_imoveis"]["nome"]);
            foreach($tipos as $t){
                $tipos_cidade[$c["tb_imoveis"]["nome"]][] = $t;
            }
        }
        $this->set("tipos_cidade", $tipos_cidade);
        $this->set("texto", $this->Texto->getAll());
        
    }
    
}
?>