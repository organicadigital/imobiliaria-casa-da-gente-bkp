<?php
class PagesController extends AppController {
    
    var $name = 'Pages';
    var $uses = array("Banner", "Cidade", "Imovel", "Indicador", "HomeDestaque", "Endereco");
    var $helpers = array("Text");

    function beforeFilter() {
        $this->layout = "home";
        $this->set("enderecos", $this->Endereco->getAll());
    }

    function display() {

        //geral
        $cidades2 = $this->Cidade->getAll2();
        $this->set("cidades2", $cidades2);
        $cidades = $this->Cidade->getAll();
        $this->set("cidades", $cidades);
        $this->set("tipos", $this->Imovel->getTipos());
        $tipos_cidade = array();
        foreach($cidades2 as $c){
            $tipos = $this->Cidade->getTipos($c["tb_imoveis"]["nome"]);
            foreach($tipos as $t){
                $tipos_cidade[$c["tb_imoveis"]["nome"]][] = $t;
            }
        }
        $this->set("tipos_cidade", $tipos_cidade);
        
        //home
        $this->set("banners", $this->Banner->getAll());
        $this->set("indicadores", $this->Indicador->get());
        $this->set("destaques", $this->HomeDestaque->getAll2());
        
    }
    
}
?>