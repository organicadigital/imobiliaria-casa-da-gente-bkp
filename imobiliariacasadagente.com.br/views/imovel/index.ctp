<section id="conteudo_principal" class="pagina_imovel">
    <span id="continuacao_conteudo_principal"></span>
    <article class="pagina conteudo_principal">
        <div class="esquerda">
            <div id="foto_grande">                
                <?php 
                    if($eh_condominio) {
                        $imagem_original = $this->webroot . "images?url_image=" . "img/condominios/" . $fotos[0]["fotos_condominios"]["id"] . "_1.jpg";
                    }elseif($eh_obra_concluida){
                        $imagem_original = $this->webroot . "images?url_image=" . "img/obras_concluidas/" . $fotos[0]["fotos_obras_concluidas"]["id"] . "_1.jpg";
                    }else{
                       $imagem_original = $this->webroot . "images?url_image=" . "img/imoveis/" . $fotos[0]["tb_imoveis_fotos"]["f_id_foto"] . "_1.jpg";
                    }
                ?>
                <?php if($eh_condominio){ ?>
                <img src="<?php echo $imagem_original ?>" alt="<?php echo $fotos[0]["fotos_condominios"]["legenda"]; ?>" />
                <?php }elseif($eh_obra_concluida){ ?>
                <img src="<?php echo $imagem_original ?>" alt="<?php echo $fotos[0]["fotos_obras_concluidas"]["legenda"]; ?>" />
                <?php }else{ ?>
                <img src="<?php echo $imagem_original ?>" alt="<?php echo $fotos[0]["tb_imoveis_fotos"]["f_legenda"]; ?>" />
                <?php } ?>
            </div>
            <span id="seta_miniatura_anterior"></span>
            <span id="seta_miniatura_proximo"></span>
            <div id="miniaturas">
                <ul>
                    <?php
                        $i=0;
                        foreach($fotos as $f){
                            if($i==0){
                                $i++;
                    ?>
                    <li class="atual">
                    <?php }else{ ?>
                    <li>
                    <?php
                    }
                    if($eh_condominio){
                    ?>
                        <img src="<?php echo $this->webroot; ?>images?url_image=img/condominios/<?php echo $f["fotos_condominios"]["id"]; ?>_3.jpg" alt="<?php echo $f["fotos_condominios"]["legenda"]; ?>" />
                    <?php }elseif($eh_obra_concluida){ ?>
                        <img src="<?php echo $this->webroot; ?>images?url_image=img/obras_concluidas/<?php echo $f["fotos_obras_concluidas"]["id"]; ?>_3.jpg" alt="<?php echo $f["fotos_obras_concluidas"]["legenda"]; ?>" />
                    <?php }else{ ?>
                        <img src="<?php echo $this->webroot; ?>images?url_image=img/imoveis/<?php echo $f["tb_imoveis_fotos"]["f_id_foto"]; ?>_3.jpg" alt="<?php echo $f["tb_imoveis_fotos"]["f_legenda"]; ?>" />
                    <?php } ?>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div id="divisoria_vertical">
            <span class="fundo_imovel_cima1"></span>
            <span class="fundo_imovel_cima2"></span>
            <span class="fundo_imovel_cima3"></span>
        </div>
        <div class="direita">
            <header>
                <span class="arabesco"></span>
                <h1><?php echo $titulo_imovel; ?></h1>
            </header>
            <p>
                <?php echo $descricao_imovel; ?>
            </p>
            <?php if(!$eh_condominio && !$eh_obra_concluida){ ?>
            <ul class="lista_esquerda">
                <li>- <?php echo utf8_encode($imovel["tb_imoveis"]["f_tipo"]); ?></li>
                <li>- Suítes: <?php echo utf8_encode($imovel["tb_imoveis"]["f_numero_suites"]); ?></li>
                <li>- Garagens: <?php echo utf8_encode($imovel["tb_imoveis"]["f_numero_garagens"]); ?></li>
            </ul>
            <?php } ?>
            <ul class="lista_baixo">
                <li class="primeiro">Cidade
                    <span><?php echo $cidade_imovel; ?></span>
                </li>
                <li>Valor do Imóvel
                <?php if($valor_imovel == '0,00'){
                    echo "<span>Valor sob consulta.</span>";
                } else {
                    echo '<span>'.$valor_imovel.'</span>';
                } ?>
                    <nav>
                        <ul>
                            <li>
                            	<?php
                            		$codigo = $imovel["tb_imoveis"]["f_codigo"];
                            		if ($codigo) {
                            	?>
                                	<a class="envio-email" href="<?php echo $this->webroot; ?>contato?cod=<?php echo $codigo; ?>" title="E-mail">
                                	<i></i><span>E-mail</span>
                                	</a>
                                <?php } else { ?>
                                	<a class="envio-email sem-cod" href="<?php echo $this->webroot; ?>contato" title="E-mail"><i></i><span>E-mail</span></a>
                            	<?php };?>
                            </li>
                        </ul>
                    </nav>
                </li>
            </ul>
            <?php if(!$eh_condominio && !$eh_obra_concluida){ ?>
            <span id="codigo_imovel">cód. <?php echo $imovel["tb_imoveis"]["f_codigo"]; ?></span>
            <?php } ?>
        </div>
    </article>
</section>
<div id="conteudo_secundario" class="clear">
    <?php if($eh_condominio){ ?>
    <section>
        <h2 class="titulo com_fundo">
            <span class="pagina">
                <span class="linha_h2 tamanho8"></span>
                <span class="texto">Imóveis no Condomínio</span>
            </span>
        </h2>
        <div class="pagina" id="resultado_imoveis_condominio">
        </div>
    </section>
    <?php } ?>
    <?php if($eh_obra_concluida){ ?>
    <section>
        <h2 class="titulo com_fundo">
            <span class="pagina">
                <span class="linha_h2 tamanho8"></span>
                <span class="texto">Imóveis na Obra Concluída</span>
            </span>
        </h2>
        <div class="pagina" id="resultado_imoveis_obra_concluida">
        </div>
    </section>
    <?php } ?>
    <?php echo $this->element("busca_interna", array("busca_imovel"=>1)); ?>
    <section>
        <h2 class="titulo">
            <span class="pagina">
                <span class="linha_h2 tamanho1"></span>
                <span class="texto">Ver mais imóveis</span>
            </span>
        </h2>
        <div class="pagina" id="resultado_busca_interna">
        </div>
    </section>
    <section style="display: none;">
        <h2 class="titulo">
            <span class="pagina">
                <span class="linha_h2 tamanho3"></span>
                <span class="texto">Outros imóveis</span>
            </span>
        </h2>
        <div class="pagina" id="resultado_busca_interna2">
        </div>
    </section>
</div>
<script>
$(document).ready(function(){
    <?php if($eh_condominio){ ?>
    processaBuscaImoveisCondominios(0, <?php echo $identificador; ?>);
    <?php } ?>
    <?php if($eh_obra_concluida){ ?>
    processaBuscaImoveisObrasConcluidas(0, <?php echo $identificador; ?>);
    <?php } ?>
});
</script>