<h1  class="titulo">
    <span class="pagina">
        <span class="texto">Localização</span>
        <span class="linha_h2 tamanho7"></span>
    </span>
</h1>
<span class="divisoria1">
    <span class="divisoria2"></span>
</span>
<?php echo $this->element("busca_interna", array("fundo_f2f2f2"=>1)); ?>
<span class="divisoria1 sem_espaco">
    <span class="divisoria2"></span>
</span>
<section id="conteudo_lojas" class="fundo_f2f2f2">
    <div class="pagina" style="height:<?php echo count($enderecos)>1 ? count($enderecos)*400 : '820'; ?>px">
        <div class="topo">
            <span id="linha_esquerda"></span>
            <span id="linha_direita"></span>
            <span id="arabesco"></span>
        </div>
		<div class="imagens-lojas" style="float:right;margin:20px 0 10px 20px; width:220px;">
			<?php
				foreach($enderecos as $endereco) {
					$nome = $endereco["c"]["nome"];
			?>
				<article class="imovel" style="margin:0 0 0 -100px;">
          <div class="informacoes" style="width:200px;">
              <span class="sombra"></span>
              <h3>
                  Loja <?php echo $nome; ?>
              </h3>
              <p>
								<?php 
                  echo $endereco["enderecos"]["endereco"]; 
                  echo "<br/>";
                  echo $endereco["enderecos"]["telefone"];
                  echo "<br/>";
                  echo $endereco["enderecos"]["cep"];
								?>
              </p>
              <span class="linha_horizontal"></span>
              <span class="cidade"><?php echo $nome; ?></span>
              <span class="linha_horizontal2"></span>
          </div>
          <div class="imagem">
          	<?php
              echo "<img src='img/lojas/" . $endereco["enderecos"]["id"]. ".jpg' alt='Loja " . $nome . "' />";
          	?>
          </div>
        </article>
        <br class="clear" />
				<?php
					};
				?>
	    </div>
	     <div class="textos" style="width:380px;">
        <?php 
        	echo $texto[0]["textos"]["texto"]; 
        ?>
        </div>
        <br class="clear" />
    </div>
</section>