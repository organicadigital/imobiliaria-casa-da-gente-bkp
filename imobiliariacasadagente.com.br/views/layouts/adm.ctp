<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title><?php echo $titulo; ?></title>
        <link rel="shortcut icon" href="<?php echo $this->webroot; ?>favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/reset.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/adm1.css">
        <script src="<?php echo $this->webroot; ?>js/modernizr.js"></script>
        <script src="<?php echo $this->webroot; ?>js/jquery.js"></script>
        <script src="<?php echo $this->webroot; ?>js/jquery.maskedinput-1.3.min.js"></script>
        <script src="<?php echo $this->webroot; ?>js/jquery.alphanumeric.pack.js"></script>
        <script>
            var urlRoot = "<?php echo $this->webroot; ?>";
        </script>
        <script src="<?php echo $this->webroot; ?>js/adm3.js"></script>
        <script src="<?php echo $this->webroot; ?>js/ajaxfileupload.js"></script>
    </head>
    <body>
        <div id="container">
            <div id="cabecalho">
                <img src="<?php echo $this->webroot; ?>img/casa_da_gente_logo.png" alt="Imobiliária Casa da Gente" />
                <div id="acesso">
                    Acesso: Imóveis de <?php echo $cidade_acesso; ?>
                </div>
                <a href="<?php echo $this->webroot; ?>adm/logout" title="SAIR" id="sair" class="botao">SAIR</a>
            </div>
            <?php if ($permissao == "ADMIN") { ?>
            <nav id="menu">
                <ul>
                    <li>
                        <a href="<?php echo $this->webroot; ?>adm/atualizarFotos" title="Atualizar Fotos">Atualizar Fotos</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->webroot; ?>adm/condominios" title="Condomínios">Condomínios</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->webroot; ?>adm/home" title="Home">Home</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->webroot; ?>adm/imoveisDeLuxo" title="Imóveis de Luxo">Imóveis de Luxo</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->webroot; ?>adm/obrasConcluidas" title="Obras Concluídas">Obras Concluídas</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->webroot; ?>adm/ofertas" title="Ofertas">Ofertas</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->webroot; ?>adm/news" title="News">News</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->webroot; ?>adm/lojas" title="Lojas">Lojas</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->webroot; ?>adm/cidades" title="Cidades">Cidades</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->webroot; ?>adm/usuarios" title="Usuarios">Usuários</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->webroot; ?>adm/atualizarSenha" title="Alterar Senha">Alterar Senha</a>
                    </li>
                </ul>
            </nav>
            <?php } else if ($permissao == "LITORAL") { ?>
            <nav id="menu">
                <ul>
                    <li>
                        <a href="<?php echo $this->webroot; ?>adm/condominios" title="Condomínios">Condomínios</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->webroot; ?>adm/ofertas" title="Ofertas">Ofertas</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->webroot; ?>adm/atualizarSenha" title="Alterar Senha">Alterar Senha</a>
                    </li>
                </ul>
            </nav>
            <?php }; ?>
            <?php echo $content_for_layout ?>
        </div>
        <div id="lad"></div>
        <div id="lad2"></div>
    </body>
</html>