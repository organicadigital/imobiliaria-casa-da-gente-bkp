<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title><?php echo $titulo; ?></title>
        <link rel="shortcut icon" href="<?php echo $this->webroot; ?>favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/reset.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/adm1.css">
        <script src="<?php echo $this->webroot; ?>js/modernizr.js"></script>
        <script src="<?php echo $this->webroot; ?>js/jquery.js"></script>
        <script>
            var urlRoot = "<?php echo $this->webroot; ?>";
        </script>
        <script src="<?php echo $this->webroot; ?>js/adm1.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/login.js"></script>
    </head>
    <body>
        <div id="container" class="login">
            <img src="<?php echo $this->webroot; ?>img/casa_da_gente_logo.png" alt="ImobiliÃ¡ria Casa da Gente" />
            <h1>área Restrita</h1>
            <form action="javascript:submit_login()" method="post">
                <div class="linha_formulario">
                    <label for="usuario_text">Usuário</label>
                    <input type="text" name="usuario_text" id="usuario_text" value="" />
                </div>
                <div class="linha_formulario">
                    <label for="senha_password">senha</label>
                    <input type="password" name="senha_password" id="senha_password" value="" />
                </div>
                <div class="linha_formulario">
                    <input type="submit" value="ENTRAR" />
                </div>
            </form>
        </div>
        <div id="lad"></div>
    </body>
</html>