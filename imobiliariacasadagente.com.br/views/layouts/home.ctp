<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title>Imobiliária em Gramado e Canela. Imóveis, Casas, Terrenos, Apartamentos, Salas Comerciais e muito mais.</title>
        <meta name="description" content="Imobiliária em Gramado e Canela. Imóveis, Casas, Terrenos, Apartamentos, Salas Comerciais e muito mais." />
        <meta name="keywords" content="imobiliaria, gramado, canela, imóveis, rs, serra gaucha, terrenos, apartamento, apartamentos, lago negro, região hortensias, hortencias, natal luz, festa colonial, festival de cinema, férias, julho, inverno, neve, quarto, casa geminada, próximo, rua coberta, carrieri, locação, venda, preço, baixo, oportunidade, investimento, promocao, promocoes, desconto, verao, janeiro, nova petropolis, ferias, lojas, salas, comercial, negócios" />
        <link rel="shortcut icon" href="<?php echo $this->webroot; ?>favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/reset.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/geral1.css">
        <!--[if IE]>
            <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/ie.css">
        <![endif]-->
        <!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/ie7.css">
        <![endif]-->
        <!--[if IE 8]>
            <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/ie8.css">
        <![endif]-->
        <script src="<?php echo $this->webroot; ?>js/modernizr.js"></script>
        <script src="<?php echo $this->webroot; ?>js/jquery-1.7.1.min.js"></script>
        <script>
            var urlRoot = "<?php echo $this->webroot; ?>";
        </script>
        <script src="<?php echo $this->webroot; ?>js/geral1.js"></script>
        <script src="<?php echo $this->webroot; ?>js/home5.js"></script>
        <script src="<?php echo $this->webroot; ?>js/jquery.slides.min.js"></script>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-75434081-1', 'auto');
          ga('send', 'pageview');

        </script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//cdn.zopim.com/?1CogBQfTjGFtI7DkgzcrzhzuyTf5ymSn';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
    </head>
    <body> 
        <div class="pagina">
            <header id="header_principal">
                <?php if ($_SERVER['REQUEST_URI'] == '/'){ ?>
                    <span class="logo">
                        <img src="<?php echo $this->webroot; ?>img/casa_da_gente_logo.png" alt="Imobiliária Casa da Gente" />
                    </span>
                <?php } else { ?>
                    <span class="logo">
                        <a href="/">
                            <img src="<?php echo $this->webroot; ?>img/imobiliaria_casa_da_gente.png" alt="Imobiliária Casa da Gente" />
                        </a>
                    </span>                    
                <?php } ?>

                <div class="logodezanos">
                    &nbsp;
                </div>
                <h1 class="telefones" style="padding-top:10px">
                    <img src="<?php echo $this->webroot; ?>img/phone-gramado.png" alt="telefones" />
                </h1>
              </h1>
                <?php echo $this->element("menus"); ?>
                <section id="busca" class="busca">
                    <form action="javascript:envia_busca_home()" method="post">
                        <h2>Encontre seu imóvel</h2>
                        <div class="linha_formulario">
                            <div class="coluna">
                                <label for="tipo_busca" class="formatacao2">tipo</label>
                                <select id="tipo_busca" name="data[Busca][tipo]" size="8">
                                    <option value="tipo" selected="selected">tipo</option>
                                    <?php foreach($tipos as $t){ ?>
                                    <option value="<?php echo utf8_encode($t); ?>"><?php echo utf8_encode($t); ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="coluna coluna2" id="coluna_codigo">
                                <label for="codigo_busca" class="oculto">código</label>
                                <input type="text" id="codigo_busca" name="data[Busca][codigo]" value="código" />
                            </div>
                        </div>
                        <div class="linha_formulario extra">
                            <div class="coluna">
                                <input type="radio" name="data[Busca][extra]" id="todos_extra_radio" checked="checked" value="todos" />
                                <label for="todos_extra_radio">todos</label>
                            </div>
                            <div class="coluna">
                                <input type="radio" name="data[Busca][extra]" id="lancamentos_extra_radio"  value="lancamentos" />
                                <label for="lancamentos_extra_radio">lançamentos</label>
                            </div>
                            <div class="coluna">
                                <input type="radio" name="data[Busca][extra]" id="ofertas_extra_radio"  value="ofertas" />
                                <label for="ofertas_extra_radio">ofertas</label>
                            </div>
                        </div>
                        <h3>Localização</h3>
                        <div class="linha_formulario">
                            <div class="coluna">
                                <label for="cidade_busca" class="formatacao3">cidade</label>
                                <select id="cidade_busca" name="data[Busca][cidade]" size="<?php echo count($cidades)+1; ?>">
                                    <option value="cidade" selected="selected">cidade</option>
                                    <?php foreach($cidades2 as $c){ ?>
                                    <option value="<?php echo $c["tb_imoveis"]["nome"]; ?>"><?php echo $c["tb_imoveis"]["nome"]; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="linha_formulario oculto">
                            <div class="coluna">
                                <label for="bairro_busca" class="formatacao3">bairro</label>
                                <select id="bairro_busca" name="data[Busca][bairro]" size="<?php echo count($cidades)+1; ?>">
                                    <option value="bairro" selected="selected">selecione uma cidade</option>
                                </select>
                            </div>
                        </div>
                        <div class="linha_formulario">
                            <div class="coluna">
                                <label for="dormitorios_busca" class="formatacao2">dormitórios</label>
                                <select id="dormitorios_busca" name="data[Busca][dormitorios]" size="4">
                                    <option value="dormitorios" selected="selected">dormitórios</option>
                                    <option value="1 Dorm">1 dormitório</option>
                                    <option value="2 Dorm">2 dormitórios</option>
                                    <option value="3 Dorm">3 dormitórios</option>
                                    <option value="4 Dorm">4 dormitórios</option>
                                    <option value="5 Dorm">5 dormitórios</option>
                                    <option value="6 Dorm">6 dormitórios</option>
                                    <option value="7 Dorm">7 dormitórios</option>
                                </select>
                            </div>
                            <div class="coluna coluna2">
                                <label for="garagem_busca" class="formatacao2">garagem</label>
                                <select id="garagem_busca" name="data[Busca][garagem]" size="3">
                                    <option value="garagem" selected="selected">garagem</option>
                                    <option value="1">com garagem</option>
                                    <option value="0">sem garagem</option>
                                </select>
                            </div>
                        </div>
                        <h3>Valor</h3>
                        <div class="linha_formulario">
                            <div class="coluna">
                                <label for="valor_de_busca" class="formatacao2">de</label>
                                <select id="valor_de_busca" name="data[Busca][valor_de]" size="5">
                                    <option value="de">de</option>
                                    <option value="0,00" selected="selected">R$ 0,00</option>
                                    <option value="50000,00">R$ 50 mil</option>
                                    <option value="100000,00">R$ 100 mil</option>
                                    <option value="150000,00">R$ 150 mil</option>
                                    <option value="200000,00">R$ 200 mil</option>
                                    <option value="250000,00">R$ 250 mil</option>
                                    <option value="300000,00">R$ 300 mil</option>
                                    <option value="350000,00">R$ 350 mil</option>
                                    <option value="400000,00">R$ 400 mil</option>
                                    <option value="450000,00">R$ 450 mil</option>
                                    <option value="500000,00">R$ 500 mil</option>
                                    <option value="550000,00">R$ 550 mil</option>
                                    <option value="600000,00">R$ 600 mil</option>
                                    <option value="650000,00">R$ 650 mil</option>
                                    <option value="700000,00">R$ 700 mil</option>
                                    <option value="750000,00">R$ 750 mil</option>
                                    <option value="800000,00">R$ 800 mil</option>
                                    <option value="850000,00">R$ 850 mil</option>
                                    <option value="900000,00">R$ 900 mil</option>
                                    <option value="950000,00">R$ 950 mil</option>
                                    <option value="1000000,00">R$ 1 milhão</option>
                                </select>
                            </div>
                            <div class="coluna coluna2">
                                <label for="valor_ate_busca" class="formatacao2">até</label>
                                <select id="valor_ate_busca" name="data[Busca][valor_ate]" size="5">
                                    <option value="99999999999,00" selected="selected">até</option>
                                    <option value="50000,00">R$ 50 mil</option>
                                    <option value="100000,00">R$ 100 mil</option>
                                    <option value="150000,00">R$ 150 mil</option>
                                    <option value="200000,00">R$ 200 mil</option>
                                    <option value="250000,00">R$ 250 mil</option>
                                    <option value="300000,00">R$ 300 mil</option>
                                    <option value="350000,00">R$ 350 mil</option>
                                    <option value="400000,00">R$ 400 mil</option>
                                    <option value="450000,00">R$ 450 mil</option>
                                    <option value="500000,00">R$ 500 mil</option>
                                    <option value="550000,00">R$ 550 mil</option>
                                    <option value="600000,00">R$ 600 mil</option>
                                    <option value="650000,00">R$ 650 mil</option>
                                    <option value="700000,00">R$ 700 mil</option>
                                    <option value="750000,00">R$ 750 mil</option>
                                    <option value="800000,00">R$ 800 mil</option>
                                    <option value="850000,00">R$ 850 mil</option>
                                    <option value="900000,00">R$ 900 mil</option>
                                    <option value="950000,00">R$ 950 mil</option>
                                    <option value="1000000,00">R$ 1 milhão</option>
                                    <option value="99999999999,00">sem limite</option>
                                </select>
                            </div>
                        </div>
                        <div class="linha_formulario">
                            <input type="hidden" name="data[Busca][offset]" value="0" />
                            <label for="btn_buscar" class="btn_buscar">
                                <input type="submit" value="Buscar" id="btn_buscar" />
                            </label>
                        </div>
                    </form>
                </section>
            </header>
            <section id="banners">
                <ul id="fotos_home">
                    <?php foreach($banners as $b){ ?>
                    <li>
                        <a href="<?php echo $b["banners"]["link"]; ?>" title="<?php echo $b["banners"]["linha1"]; ?>">
                            <div>
                                <h4><?php echo $b["banners"]["linha1"]; ?></h4>
                                <p><?php echo $b["banners"]["linha2"]; ?></p>
                                <span class="saiba_mais">saiba mais ></span>
                            </div>
                            <img src="<?php echo $this->webroot; ?>img/banners/<?php echo $b["banners"]["id"]; ?>.jpg" alt="<?php echo $b["banners"]["linha1"]; ?>" />
                        </a>
                    </li>
                    <?php } ?>
                </ul>
            </section>
        </div>
        <span class="divisoria1">
            <span class="divisoria2"></span>
        </span>
        <div id="conteudo">
            <section class="pagina">
                <?php
                $limite= 4;
                if(count($destaques)<5){
                    $limite= count($destaques)-1;
                }
                for($i=0; $i<=$limite; $i++){
                    $d = $destaques[$i];
                ?>
                <article class="imovel">
                    <?php if($d["eh_condominio"]){ ?>
                    <span class="selo selo_condominios">
                        <span class="oculto">Condomínios</span>
                    </span>
                    <?php } ?>
                    <?php if($d["eh_lancamento"]){ ?>
                    <span class="selo selo_lancamentos">
                        <span class="oculto">Lançamentos</span>
                    </span>
                    <?php } ?>
                    <?php if($d["eh_oferta"]){ ?>
                    <span class="selo selo_ofertas">
                        <span class="oculto">Ofertas</span>
                    </span>
                    <?php } ?>
                    <?php if($d["eh_construtora"]){ ?>
                    <span class="selo selo_construtora">
                        <span class="oculto">Construtora</span>
                    </span>
                    <?php } ?>
                    <?php if($d["eh_luxo"]){ ?>
                    <span class="selo selo_luxo">
                        <span class="oculto">Imóveis de Luxo</span>
                    </span>
                    <?php } ?>
                    <div class="informacoes">
                        <span class="sombra"></span>
                        <h3>
                            <a href="<?php echo $this->webroot; ?>imovel/<?php echo $d["url_formatada"]."/".$d["id"]; ?>/<?php echo $d["eh_condominio"]; ?>/0/<?php echo $d["filial"]; ?>" title="<?php echo $d["titulo"]; ?>"><?php echo $d["titulo"]; ?></a>
                        </h3>
                        <p>
                            <?php echo $text->truncate($d["descricao"], 150, array("ending"=>"...", "exact" => false)); ?>
                            <br />
                            <br />R$ <?php echo $d["valor_imovel"]; ?>
                        </p>
                        <span class="linha_horizontal"></span>
                        <span class="cidade"><?php echo $d["cidade"]; ?></span>
                        <span class="linha_horizontal2"></span>
                        <a href="<?php echo $this->webroot; ?>imovel/<?php echo $d["url_formatada"]."/".$d["id"]; ?>/<?php echo $d["eh_condominio"]; ?>/0/<?php echo $d["filial"]; ?>" title="<?php echo $d["titulo"]; ?>">
                            <span class="seta"></span>
                            saiba mais
                        </a>
                    </div>
                    <div class="imagem">
                        <a href="<?php echo $this->webroot; ?>imovel/<?php echo $d["url_formatada"]."/".$d["id"]; ?>/<?php echo $d["eh_condominio"]; ?>/0/<?php echo $d["filial"]; ?>" title="<?php echo $d["titulo"]; ?>">
                            <?php if($d["eh_condominio"]){ ?>
                            <img src="<?php echo $this->webroot; ?>images?url_image=img/condominios/<?php echo $d["imagem_id"]; ?>_2.jpg" alt="<?php echo $d["titulo"]; ?>" />
                            <?php }else{ ?>
                            <img src="<?php echo $this->webroot; ?>images?url_image=img/imoveis/<?php echo $d["imagem_id"]; ?>_2.jpg" alt="<?php echo $d["titulo"]; ?>" />
                            <?php } ?>
                        </a>
                    </div>
                </article>
                <?php } ?>
                <aside id="aside">
                    <div id="previsao_do_tempo">
                        <span class="icone"></span>
                        <h5>Previsão do tempo</h5>
                        <div class="caixa">
                            <a href="http://www.climatempo.com.br/previsao-do-tempo/cidade/780/gramado-rs" title="Previsão do tempo para Gramado e Canela" rel="external">
                                Gramado / Canela
                            </a>
                        </div>
                    </div>
                    <span id="divisao_previsao_indicadores">
                        <span class="esquerda"></span>
                        <span class="direita"></span>
                    </span>
                    <div id="indicadores">
                        <span class="icone"></span>
                        <h5>Indicadores</h5>
                        <div class="caixa">
                            <span>
                                <?php echo $indicadores[0]["indicadores"]["linha1"]; ?>
                                <br /><?php echo $indicadores[0]["indicadores"]["linha2"]; ?>
                            </span>
                        </div>
                    </div>
                </aside>
            </section>
            <?php echo $this->element("rodape"); ?>
        </div>
        <img src="<?php echo $this->webroot; ?>img/submenu.png" alt="submenu" class="oculto" />
    </body>
</html>