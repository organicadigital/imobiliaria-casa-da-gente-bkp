<h1  class="titulo">
    <span class="pagina">
        <span class="texto">Imóveis encontrados</span>
        <span class="linha_h2 tamanho4"></span>
    </span>
</h1>
<span class="divisoria1">
    <span class="divisoria2"></span>
</span>
<?php echo $this->element("busca_interna", array("fundo_f2f2f2" => 1, "busca_imovel2"=>1)); ?>
<span class="divisoria1 sem_espaco">
    <span class="divisoria2"></span>
</span>
<div>
</div>
<section class="fundo_f2f2f2">
    <div class="pagina" id="resultado_busca_interna">
    </div>
</section>
<script>
$(document).ready(function(){
    
    $("#tipo_busca").val("<?php echo $busca_tipo; ?>");
    $("#codigo_busca").val("<?php echo $busca_codigo; ?>");
    $("#busca_imovel input[value=<?php echo $busca_extra; ?>]:radio").attr('checked', true);
    $("#cidade_busca").val("<?php echo $busca_cidade; ?>");
    $("#dormitorios_busca").val("<?php echo $busca_dormitorios; ?>");
    $("#garagem_busca").val("<?php echo $busca_garagem; ?>");
    $("#valor_de_busca").val("<?php echo $busca_valor_de; ?>");
    $("#valor_ate_busca").val("<?php echo $busca_valor_ate; ?>");
    if("<?php echo $busca_bairro; ?>" != "bairro"){
        $("#bairro_busca").html("<option value='bairro'>bairro</option><option value='<?php echo $busca_bairro; ?>' selected='selected'><?php echo $busca_bairro; ?></option>");
    }
    carrega_imoveis(<?php echo $offset; ?>);
    
    
});
</script>