<div id="container_interno" class="recuo1">
    <h1>Ofertas</h1>
    <div class="recuo2">
        <?php if(count($imoveis)>0){ ?>
        <table>
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Título</th>
                    <th>É uma oferta?</th>
                </tr>
            </thead>
            <tbody>
               <?php
               		$permissao_acesso = $permissao == 'LITORAL' ? 1 : 0;
                    $i = 0;
                    $i2 = 0;
                    foreach($imoveis as $im){
                    	
                        $i++;
                        if($im["cidades"]["eh_litoral"]==$permissao_acesso || $permissao == 'ADMIN'){
                        $i2++;
                        $informacoes = explode("!", $im["tb_imoveis"]["f_descricao"], 2);
                        $titulo = trim(utf8_encode($informacoes[0]));
                        $descricao = trim(utf8_encode($informacoes[1]));
                        if($i2%2){
                ?>
                <tr class="listra">
                        <?php }else{ ?>
                <tr>
                        <?php } ?>
                    <td><?php echo $im["tb_imoveis"]["f_codigo"]; ?></td>
                    <td><?php echo $titulo; ?></td>
                    <td>
                        <a href="javascript:updateOfertas(<?php echo $im["tb_imoveis"]["f_id"]; ?>, <?php if($im[0]["eh_oferta"]){echo "1";}else{echo "0";} ?>, '<?php echo $im["tb_imoveis"]["f_filial"]; ?>')" title="Atualizar">
                            <?php if($im[0]["eh_oferta"]){echo "Sim";}else{echo "Não";} ?>
                        </a>
                    </td>
                </tr>
                <?php }} ?>
            </tbody>
        </table>
        <?php } ?>
    </div>
</div>