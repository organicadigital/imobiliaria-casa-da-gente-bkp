<div id="container_interno" class="recuo1">
    <h1>Cidades - Cadastrar</h1>
    <form action="javascript:cidadesCadastrar()" class="recuo2" method="post">
        <div class="linha_formulario">
            <label for="nome_text">Nome</label>
            <input type="text" name="data[Cidade][nome]" id="nome_text" maxlength="255" />
        </div>
        <div class="linha_formulario">
            <label for="emails_text">E-mails - separar por vírgulas</label>
            <input type="text" name="data[Cidade][emails]" id="emails_text" maxlength="255" style="width:500px;"/>
        </div>
        <div class="linha_formulario">
            <select name="data[Cidade][litoral]" id="litoral_combo">
            	<option value="">Litoral</option>
            	<option value="1">Sim</option>
            	<option value="0">N&atilde;o</option>
            </select>
        </div>
        <div class="linha_formulario">
            <a href="javascript:cidadesCadastrar()" title="CADASTRAR" class="botao botao_cadastrar alinha_esquerda">CADASTRAR</a>
            <a href="<?php echo $this->webroot; ?>adm/cidades" title="CANCELAR" class="botao botao_cancelar alinha_esquerda">CANCELAR</a>
        </div>
    </form>
</div>