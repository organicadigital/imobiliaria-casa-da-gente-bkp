<script>
$(document).ready(function(){
    <?php if($cidade["0"]["cidades"]["nome"]!=""){ ?>
    $("label[for=nome_text]").hide();
    <?php } ?>
    <?php if($cidade["0"]["cidades"]["emails"]!=""){ ?>
    $("label[for=emails_text]").hide();
    <?php } ?>
});
</script>
<div id="container_interno" class="recuo1">
    <h1>Cidades - Editar</h1>
    <form action="javascript:cidadesEditar()" class="recuo2" method="post">
        <div class="linha_formulario">
            <label for="nome_text">Nome</label>
            <input type="text" name="data[Cidade][nome]" id="nome_text" maxlength="255" value="<?php echo $cidade["0"]["cidades"]["nome"]; ?>" class="edit" />
        </div>
        <div class="linha_formulario">
            <label for="emails_text">E-mails - separar por vírgulas</label>
            <input type="text" name="data[Cidade][emails]" id="emails_text" maxlength="255" style="width:500px;" value="<?php echo $cidade["0"]["cidades"]["emails"]; ?>" class="edit"/>
        </div>
        <div class="linha_formulario">
            <select name="data[Cidade][litoral]" id="litoral_combo">
            	<option value="">Litoral</option>
            	<option value="1" <?php echo $cidade[0]["cidades"]["litoral"] == 1 ? 'selected="selected"' : ''; ?>>Sim</option>
            	<option value="0" <?php echo $cidade[0]["cidades"]["litoral"] == 0 ? 'selected="selected"' : ''; ?>>N&atilde;o</option>
            </select>
        </div>
        <div class="linha_formulario">
        	<input type="hidden" name="data[Cidade][id]" id="cidade_id" value="<?php echo $cidade["0"]["cidades"]["id"]; ?>" />
            <a href="javascript:cidadesEditar()" title="CADASTRAR" class="botao botao_cadastrar alinha_esquerda">SALVAR</a> 
            <a href="<?php echo $this->webroot; ?>adm/cidades" title="CANCELAR" class="botao botao_cancelar alinha_esquerda">CANCELAR</a>
        </div>
    </form>
</div>