<div id="container_interno" class="recuo1">
    <h1>Condomínios - Cadastrar</h1>
    <form action="javascript:condominios_cadastrar()" class="recuo2" method="post">
    	<div class="linha_formulario">
            <select name="data[Condominio][cidade_id]" id="cidade_combo">
            	<option value="">Cidade</option>
            	<?php
				foreach($cidades as $cidade){
					$permissao_acesso = $permissao == 'LITORAL' ? 1 : 0;
					if($cidade["cidades"]["litoral"] == $permissao_acesso || $permissao == 'ADMIN'){
						?>
						<option value="<?php echo $cidade["cidades"]["id"]; ?>"><?php echo $cidade["cidades"]["nome"]; ?></option>
						<?php
					}
				};
				?>
            </select>
        </div>
        <div class="linha_formulario">
            <label for="titulo_text">título</label>
            <input type="text" name="data[Condominio][titulo]" id="titulo_text" maxlength="255" />
        </div>
        <div class="linha_formulario">
            <label for="descricao_textarea">descrição</label>
            <textarea name="data[Condominio][descricao]" id="descricao_textarea" cols="1" rows="1"></textarea>
        </div>
        <div class="linha_formulario">
            <label for="imoveis_textarea">imóveis - separar por vírgulas</label>
            <textarea name="data[Condominio][imoveis]" id="imoveis_textarea" cols="1" rows="1"></textarea>
        </div>
        <div class="linha_formulario">
            <div class="linha_formulario2">
                <input type="checkbox" name="data[Condominio][lancamento_checkbox]" id="lancamento_checkbox" />
                <label for="lancamento_checkbox" class="reset">Lançamento</label>
            </div>
            <div class="linha_formulario2">
                <input type="checkbox" name="data[Condominio][destaque_checkbox]" id="destaque_checkbox" />
                <label for="destaque_checkbox" class="reset">Destaque</label>
            </div>
        </div>
        <div class="linha_formulario">
            <a href="javascript:condominios_cadastrar()" title="CADASTRAR" class="botao botao_cadastrar alinha_esquerda">CADASTRAR</a>
            <a href="javascript:condominios_cadastrar2()" title="CADASTRAR E ADICIONAR FOTOS" class="botao botao_cadastrar_fotos alinha_esquerda">CADASTRAR E ADICIONAR FOTOS</a>
        </div>
    </form>
</div>