<div id="container_interno" class="recuo1">
    <h1>Home - Banners - Cadastrar</h1>
    <form action="javascript:home_banners_cadastrar()" class="recuo2 espaco_superior" method="post">
        <div class="linha_formulario">
            <input type="file" name="data[Foto][arquivo]" id="foto_file" />
            <span class="obs">Imagem deve estar na extensão .jpg e com tamanho inferior a 1 Mb.</span>
        </div>
        <div class="linha_formulario">
            <label for="linha1_text">linha1</label>
            <input type="text" name="data[Banner][linha1]" id="linha1_text" maxlength="255" />
        </div>
        <div class="linha_formulario">
            <label for="linha2_text">linha2</label>
            <input type="text" name="data[Banner][linha2]" id="linha2_text" maxlength="255" />
        </div>
        <div class="linha_formulario">
            <label for="link_text">link</label>
            <input type="text" name="data[Banner][link]" id="link_text" maxlength="255" />
        </div>
        <div class="linha_formulario">
            <label for="ordem_text">ordem</label>
            <input type="text" name="data[Banner][ordem]" id="ordem_text" class="numerico" />
        </div>
        <div class="linha_formulario">
            <a href="javascript:home_banners_cadastrar()" title="CADASTRAR" class="botao botao_cadastrar alinha_esquerda">CADASTRAR</a>
        </div>
    </form>
</div>