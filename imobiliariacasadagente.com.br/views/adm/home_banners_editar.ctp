<script>
$(document).ready(function(){
    <?php if($banner["0"]["banners"]["linha1"]!=""){ ?>
    $("label[for=linha1_text]").hide();
    <?php } ?>
    <?php if($banner["0"]["banners"]["linha2"]!=""){ ?>
    $("label[for=linha2_text]").hide();
    <?php } ?>
    <?php if($banner["0"]["banners"]["link"]!=""){ ?>
    $("label[for=link_text]").hide();
    <?php } ?>
    $("label[for=ordem_text]").hide();
});
</script>
<div id="container_interno" class="recuo1">
    <h1>Home - Banners - Editar</h1>
    
    <form action="javascript:home_banners_editar()" class="recuo2 espaco_superior" method="post">
        <div class="linha_formulario">
            <label for="linha1_text">linha1</label>
            <input type="text" name="data[Banner][linha1]" id="linha1_text" maxlength="255" value="<?php echo $banner[0]["banners"]["linha1"]; ?>" class="edit" />
        </div>
        <div class="linha_formulario">
            <label for="linha2_text">linha2</label>
            <input type="text" name="data[Banner][linha2]" id="linha2_text" maxlength="255" value="<?php echo $banner[0]["banners"]["linha2"]; ?>" class="edit" />
        </div>
        <div class="linha_formulario">
            <label for="link_text">link</label>
            <input type="text" name="data[Banner][link]" id="link_text" maxlength="255" value="<?php echo $banner[0]["banners"]["link"]; ?>" class="edit" />
        </div>
        <div class="linha_formulario">
            <label for="ordem_text">ordem</label>
            <input type="text" name="data[Banner][ordem]" id="ordem_text" class="numerico edit" value="<?php echo $banner[0]["banners"]["ordem"]; ?>" />
        </div>
        <div class="linha_formulario">
            <input type="hidden" name="data[Banner][id]" id="banner_id" value="<?php echo $banner["0"]["banners"]["id"]; ?>" />
            <a href="javascript:home_banners_editar()" title="SALVAR" class="botao botao_salvar alinha_esquerda">SALVAR</a>
        </div>
    </form>
</div>