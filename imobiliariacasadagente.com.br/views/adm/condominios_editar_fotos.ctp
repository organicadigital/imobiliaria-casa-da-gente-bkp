<div id="container_interno" class="recuo1">
    <h1>Condomínios - Editar Fotos</h1>
    <form action="javascript:condominios_cadastrar_foto()" class="recuo2 espaco_superior" method="post">
        <div class="linha_formulario">
            <input type="file" name="data[Foto][arquivo]" id="foto_file" />
            <span class="obs">Imagem deve estar na extensão .jpg e com tamanho inferior a 1 Mb.</span>
        </div>
        <div class="linha_formulario">
            <label for="legenda_text">legenda</label>
            <input type="text" name="data[Foto][legenda]" id="legenda_text" maxlength="150" />
        </div>
        <div class="linha_formulario">
            <label for="ordem_text">ordem</label>
            <input type="text" name="data[Foto][ordem]" id="ordem_text" class="numerico" />
        </div>
        <div class="linha_formulario">
            <div class="linha_formulario2">
                <input type="checkbox" name="data[Foto][destaque_checkbox]" id="destaque_checkbox" />
                <label for="destaque_checkbox" class="reset">Destaque</label>
            </div>
        </div>
        <div class="linha_formulario">
            <input type="hidden" name="data[Foto][condominio_id]" id="condominio_id" value="<?php echo $condominio_id; ?>" />
            <a href="javascript:condominios_cadastrar_foto()" title="ADICIONAR FOTO" class="botao botao_foto alinha_esquerda">ADICIONAR FOTO</a>
        </div>
    </form>
    <div class="recuo2">
        <?php if(count($fotos)>0){ ?>
        <table>
            <thead>
                <tr>
                    <th>Foto</th>
                    <th>Legenda</th>
                    <th>Ordem</th>
                    <th>Destaque</th>
                    <th colspan="2">Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 0;
                    foreach($fotos as $f){
                        $i++;
                        if($i%2){
                ?>
                <tr class="listra">
                        <?php }else{ ?>
                <tr>
                        <?php } ?>
                    <td>
                        <img src="<?php echo $this->webroot; ?>img/condominios/<?php echo $f["fotos_condominios"]["id"]; ?>_3.jpg" alt="foto" />
                    </td>
                    <td><?php echo $f["fotos_condominios"]["legenda"]; ?></td>
                    <td><?php echo $f["fotos_condominios"]["ordem"]; ?></td>
                    <td>
                        <?php if($f["fotos_condominios"]["destaque"]){echo "sim";}else{echo "não";} ?>
                    </td>
                    <td>
                        <a href="<?php echo $this->webroot; ?>adm/condominiosEditarFoto/<?php echo $f["fotos_condominios"]["id"]; ?>" title="Editar">Editar</a>
                    </td>
                    <td>
                        <a href="<?php echo $this->webroot; ?>adm/condominiosExcluirFoto/<?php echo $f["fotos_condominios"]["id"]; ?>/<?php echo $f["fotos_condominios"]["condominio_id"]; ?>" title="Excluir">Excluir</a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php } ?>
    </div>
</div>