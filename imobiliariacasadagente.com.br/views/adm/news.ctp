<div id="container_interno" class="recuo1">
    <h1>News</h1>
    <div class="recuo2">
        <form action="javascript:criar_news()" method="post" class="envio-news">
            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th>Ordem</th>
                        <th>Selecione o imóvel que deseja incluir</th>
                        <th></th>
                        <th>Selecione a foto que deseja</th>
                    </tr>
                </thead>
                <tbody>
                   <?php
                        $i2 = 0;
                        foreach($imoveis as $im){
                            if($im["informacoes"]["tb_imoveis"]["f_cidade"]==$cidade_acesso){
                            $i2++;
                            $informacoes = explode("!", $im["informacoes"]["tb_imoveis"]["f_descricao"], 2);
                            $titulo = trim(utf8_encode($informacoes[0]));
                            $descricao = trim(utf8_encode($informacoes[1]));
                            $url_formatada = str_replace(array("/", " ", '"', ","), array("-", "-", "", ""), $titulo);
                            if($i2%2){
                    ?>
                    <tr class="listra">
                            <?php }else{ ?>
                    <tr>
                            <?php } ?>
                        <td>
                            <input type="hidden" id="id_<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" value="<?php echo $im["informacoes"]["tb_imoveis"]["f_id"]; ?>" />
                            <input type="hidden" id="url_<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" value="<?php echo $url_formatada; ?>" />
                            <input type="hidden" id="titulo_<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" value="<?php echo $titulo; ?>" />
                            <input type="hidden" id="cidade_<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" value="<?php echo $im["informacoes"]["tb_imoveis"]["f_cidade"]; ?>" />
                            <input type="hidden" id="filial_<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" value="<?php echo $im["informacoes"]["tb_imoveis"]["f_filial"]; ?>" />
                            <input type="checkbox" id="imovel_<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" value="<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" />
                        </td>
                        <td><input class="ordem numerico" type="text" id="ordem_<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" value="" size="1" /></td>
                        <td><?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?></td>
                        <td><textarea id="descricao_<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" rows="10" cols="10" class="edit"><?php echo $descricao; ?></textarea></td>
                        <td>
                            <ul>
                                <?php $i=0; foreach($im["fotos"] as $f){ ?>
                                <li>
                                    <input type="radio" name="fotos_<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" value="<?php echo $f["tb_imoveis_fotos"]["f_id_foto"]; ?>" <?php if($i==0){ ?>checked="checked"<?php }; ?> />
                                </li>
                                <?php if($i==0){$i++;}} ?>
                            </ul>
                        </td>
                    </tr>
                    <?php }} ?>
                    <?php
                        foreach($imoveis as $im){
                            if($im["informacoes"]["tb_imoveis"]["f_cidade"]!=$cidade_acesso){
                            $i2++;
                            $informacoes = explode("!", $im["informacoes"]["tb_imoveis"]["f_descricao"], 2);
                            $titulo = trim(utf8_encode($informacoes[0]));
                            $descricao = trim(utf8_encode($informacoes[1]));
                            $url_formatada = str_replace(array("/", " ", '"', ","), array("-", "-", "", ""), $titulo);
                            if($i2%2){
                    ?>
                    <tr class="listra">
                            <?php }else{ ?>
                    <tr>
                            <?php } ?>
                        <td>
                            <input type="hidden" id="id_<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" value="<?php echo $im["informacoes"]["tb_imoveis"]["f_id"]; ?>" />
                            <input type="hidden" id="url_<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" value="<?php echo $url_formatada; ?>" />
                            <input type="hidden" id="titulo_<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" value="<?php echo $titulo; ?>" />
                            <input type="hidden" id="cidade_<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" value="<?php echo $im["informacoes"]["tb_imoveis"]["f_cidade"]; ?>" />
                            <input type="hidden" id="filial_<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" value="<?php echo $im["informacoes"]["tb_imoveis"]["f_filial"]; ?>" />
                            <input type="checkbox" id="imovel_<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" value="<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" />
                        </td>
                        <td><input class="ordem numerico" type="text" id="ordem_<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" value="" size="1" /></td>
                        <td><?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?></td>
                        <td><textarea id="descricao_<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" rows="10" cols="10" class="edit"><?php echo $descricao; ?></textarea></td>
                        <td>
                            <ul>
                                <?php $i=0; foreach($im["fotos"] as $f){ ?>
                                <li>
                                    <input type="radio" name="fotos_<?php echo $im["informacoes"]["tb_imoveis"]["f_codigo"]; ?>" value="<?php echo $f["tb_imoveis_fotos"]["f_id_foto"]; ?>" <?php if($i==0){ ?>checked="checked"<?php }; ?> />
                                    <img src="" />
                                </li>
                                <?php if($i==0){$i++;}} ?>
                            </ul>
                        </td>
                    </tr>
                    <?php }} ?>
                </tbody>
            </table>
            <div class="linha_formulario">
                <label for="email_text">e-mail</label>
                <input type="text" maxlength="255" id="email_text" name="data[News][email]" />
            </div>
            <div class="linha_formulario">
                <a class="botao botao_criar_news" title="CRIAR NEWS" href="javascript:criar_news()">CRIAR NEWS</a>
            </div>
        </form>
    </div>
</div>