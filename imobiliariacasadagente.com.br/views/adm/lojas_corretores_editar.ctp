<script>
$(document).ready(function(){
    <?php if($corretor["0"]["corretores"]["texto"]!=""){ ?>
    $("label[for=texto_textarea]").hide();
    <?php } ?>
});
</script>
<div id="container_interno" class="recuo1">
    <h1>Corretores - Editar</h1>
    <form action="javascript:lojas_corretor_editar()" class="recuo2" method="post">
        <div class="linha_formulario">
        	<select name="data[Corretor][filial]" id="filial_corretor_select">
            	<option value="">Cidade</option>
            	<?php
				foreach($cidades as $cidade){
					?>
					<option value="<?php echo $cidade["cidades"]["id"]; ?>" <?php if($corretor[0]["corretores"]["filial"] == $cidade["cidades"]["id"]){ echo "selected='selected'"; } ?>><?php echo $cidade["cidades"]["nome"]; ?></option>
					<?php
				};
				?>
            	
            </select>
        </div>
        <div class="linha_formulario">
            <label for="texto_textarea">texto</label>
            <textarea name="data[Corretor][texto]" id="texto_corretor_text" cols="1" rows="1" class="edit"><?php echo $corretor[0]["corretores"]["texto"]; ?></textarea>
        </div>
        <div class="linha_formulario">
            <input type="hidden" name="data[Corretor][id]" id="corretor_id" value="<?php echo $corretor["0"]["corretores"]["id"]; ?>" />
            <a href="javascript:lojas_corretor_editar()" title="SALVAR" class="botao botao_salvar alinha_esquerda">SALVAR</a>
            <a href="<?php echo $this->webroot; ?>adm/lojasCorretores" title="CANCELAR" class="botao botao_cancelar alinha_esquerda">CANCELAR</a>
        </div>
    </form>
</div>