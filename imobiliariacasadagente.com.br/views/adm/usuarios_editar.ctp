<script>
$(document).ready(function(){
    <?php if($usuario["0"]["usuarios"]["login"]!=""){ ?>
    $("label[for=login_text]").hide();
    <?php } ?>
    <?php if($usuario["0"]["usuarios"]["cidade"]!=""){ ?>
    $("label[for=descricao_textarea]").hide();
    <?php } ?>
    <?php if($usuario["0"]["usuarios"]["permissao"]!=""){ ?>
    $("label[for=imoveis_textarea]").hide();
    <?php } ?>
});
</script>
<div id="container_interno" class="recuo1">
    <h1>Usuários - Cadastrar</h1>
    <form action="javascript:obras_concluidas_cadastrar()" class="recuo2" method="post">
        <div class="linha_formulario">
            <label for="login_text">Login</label>
            <input type="text" name="data[Usuario][login]" id="login_text" maxlength="255" value="<?php echo $usuario[0]["usuarios"]["login"]; ?>" class="edit" />
        </div>
        <!--
        <div class="linha_formulario">
            <label for="senha_text">Senha</label>
            <input type="password" name="data[Usuario][senha]" id="senha_text" maxlength="255" />
        </div>
        -->
        <div class="linha_formulario">
            <select name="data[Usuario][cidade]" id="cidade_combo">
            	<option value="">Cidade</option>
            	<?php
				foreach($cidades as $cidade){
					?>
					<option value="<?php echo $cidade["cidades"]["id"]; ?>" <?php echo $usuario[0]["usuarios"]["cidade_id"] == $cidade["cidades"]["id"] ? 'selected="selected"' : ''; ?>><?php echo $cidade["cidades"]["nome"]; ?></option>
					<?php
				};
				?>
            	
            </select>
        </div>
        <div class="linha_formulario">
            <select name="data[Usuario][permissao]" id="permissao_combo">
            	<option value="">Permiss&atilde;o</option>
            	<option value="ADMIN" <?php echo $usuario[0]["usuarios"]["permissao"] == 'ADMIN' ? 'selected="selected"' : ''; ?>>Administrador</option>
            	<option value="LITORAL" <?php echo $usuario[0]["usuarios"]["permissao"] == 'LITORAL' ? 'selected="selected"' : ''; ?>>Litoral</option>
            </select>
        </div>
        <div class="linha_formulario">
        	<input type="hidden" name="data[Usuario][id]" id="usuario_id" value="<?php echo $usuario["0"]["usuarios"]["id"]; ?>" />
            <a href="javascript:usuariosEditar()" title="CADASTRAR" class="botao botao_cadastrar alinha_esquerda">SALVAR</a> 
            <a href="<?php echo $this->webroot; ?>adm/usuarios" title="CADASTRAR" class="botao botao_cancelar alinha_esquerda">CANCELAR</a>
        </div>
    </form>
</div>