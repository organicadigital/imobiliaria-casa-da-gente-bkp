<div id="container_interno" class="recuo1">
    <h1>Usuários - Cadastrar</h1>
    <form action="javascript:obras_concluidas_cadastrar()" class="recuo2" method="post">
        <div class="linha_formulario">
            <label for="login_text">Login</label>
            <input type="text" name="data[Usuario][login]" id="login_text" maxlength="255" />
        </div>
        <!--
        <div class="linha_formulario">
            <label for="senha_text">Senha</label>
            <input type="password" name="data[Usuario][senha]" id="senha_text" maxlength="255" />
        </div>
        -->
        <div class="linha_formulario">
            <select name="data[Usuario][cidade]" id="cidade_combo">
            	<option value="">Cidade</option>
            	<?php
				foreach($cidades as $cidade){
					?>
					<option value="<?php echo $cidade["cidades"]["id"]; ?>"><?php echo $cidade["cidades"]["nome"]; ?></option>
					<?php
				};
				?>
            	
            </select>
        </div>
        <div class="linha_formulario">
            <select name="data[Usuario][permissao]" id="permissao_combo">
            	<option value="">Permiss&atilde;o</option>
            	<option value="ADMIN">Administrador</option>
            	<option value="LITORAL">Litoral</option>
            </select>
        </div>
        <div class="linha_formulario">
            <a href="javascript:usuariosCadastrar()" title="CADASTRAR" class="botao botao_cadastrar alinha_esquerda">CADASTRAR</a>
        </div>
    </form>
</div>