<div id="container_interno" class="recuo1">
    <h1>Corretores - Cadastrar</h1>
    <form action="javascript:lojas_corretor_cadastrar()" class="recuo2" method="post">
        <div class="linha_formulario">
        	<select name="data[Corretor][filial]" id="filial_corretor_select">
            	<option value="">Cidade</option>
            	<?php
				foreach($cidades as $cidade){
					?>
					<option value="<?php echo $cidade["cidades"]["id"]; ?>"><?php echo $cidade["cidades"]["nome"]; ?></option>
					<?php
				};
				?>
            	
            </select>
        </div>
        <div class="linha_formulario">
            <label for="texto_textarea">texto</label>
            <textarea name="data[Corretor][texto]" id="texto_corretor_text" cols="1" rows="1"></textarea>
        </div>
        <div class="linha_formulario">
            <a href="javascript:lojas_corretor_cadastrar()" title="CADASTRAR" class="botao botao_cadastrar alinha_esquerda">CADASTRAR</a>
            <a href="<?php echo $this->webroot; ?>adm/lojasCorretores" title="CANCELAR" class="botao botao_cancelar alinha_esquerda">CANCELAR</a>
        </div>
    </form>
</div>