<div id="container_interno" class="recuo1">
    <h1>Obras Concluídas - Cadastrar</h1>
    <form action="javascript:obras_concluidas_cadastrar()" class="recuo2" method="post">
        <div class="linha_formulario">
            <label for="titulo_text">título</label>
            <input type="text" name="data[ObraConcluida][titulo]" id="titulo_text" maxlength="255" />
        </div>
        <div class="linha_formulario">
            <label for="descricao_textarea">descrição</label>
            <textarea name="data[ObraConcluida][descricao]" id="descricao_textarea" cols="1" rows="1"></textarea>
        </div>
        <div class="linha_formulario">
            <label for="imoveis_textarea">imóveis - separar por vírgulas</label>
            <textarea name="data[ObraConcluida][imoveis]" id="imoveis_textarea" cols="1" rows="1"></textarea>
        </div>
        <div class="linha_formulario">
            <div class="linha_formulario2">
                <input type="checkbox" name="data[ObraConcluida][lancamento_checkbox]" id="lancamento_checkbox" />
                <label for="lancamento_checkbox" class="reset">Lançamento</label>
            </div>
            <div class="linha_formulario2">
                <input type="checkbox" name="data[ObraConcluida][destaque_checkbox]" id="destaque_checkbox" />
                <label for="destaque_checkbox" class="reset">Destaque</label>
            </div>
        </div>
        <div class="linha_formulario">
            <a href="javascript:obras_concluidas_cadastrar()" title="CADASTRAR" class="botao botao_cadastrar alinha_esquerda">CADASTRAR</a>
            <a href="javascript:obras_concluidas_cadastrar2()" title="CADASTRAR E ADICIONAR FOTOS" class="botao botao_cadastrar_fotos alinha_esquerda">CADASTRAR E ADICIONAR FOTOS</a>
        </div>
    </form>
</div>