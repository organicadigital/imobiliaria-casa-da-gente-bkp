<script>
$(document).ready(function(){
    <?php if($foto["0"]["fotos_obras_concluidas"]["legenda"]!=""){ ?>
    $("label[for=legenda_text]").hide();
    <?php } ?>
    $("label[for=ordem_text]").hide();
});
</script>
<div id="container_interno" class="recuo1">
    <h1>Obras Concluídas - Editar Foto</h1>
    <form action="javascript:obras_concluidas_editar_foto()" class="recuo2 espaco_superior" method="post">
        <div class="linha_formulario">
            <img src="<?php echo $this->webroot; ?>img/obras_concluidas/<?php echo $foto["0"]["fotos_obras_concluidas"]["id"]; ?>_3.jpg" alt="foto" />
        </div>
        <div class="linha_formulario">
            <label for="legenda_text">legenda</label>
            <input type="text" name="data[Foto][legenda]" id="legenda_text" maxlength="150" value="<?php echo $foto["0"]["fotos_obras_concluidas"]["legenda"]; ?>" class="edit" />
        </div>
        <div class="linha_formulario">
            <label for="ordem_text">ordem</label>
            <input type="text" name="data[Foto][ordem]" id="ordem_text" class="numerico edit" value="<?php echo $foto["0"]["fotos_obras_concluidas"]["ordem"]; ?>" />
        </div>
        <div class="linha_formulario">
            <div class="linha_formulario2">
                <?php if($foto["0"]["fotos_obras_concluidas"]["destaque"]){ ?>
                <input type="checkbox" name="data[Foto][destaque_checkbox]" id="destaque_checkbox" checked="checked" />
                <?php }else{ ?>
                <input type="checkbox" name="data[Foto][destaque_checkbox]" id="destaque_checkbox" />
                <?php } ?>
                <label for="destaque_checkbox" class="reset">Destaque</label>
            </div>
        </div>
        <div class="linha_formulario">
            <input type="hidden" name="data[Foto][id]" id="foto_id" value="<?php echo $foto["0"]["fotos_obras_concluidas"]["id"]; ?>" />
            <input type="hidden" name="data[Foto][obra_concluida_id]" id="obra_concluida_id" value="<?php echo $foto["0"]["fotos_obras_concluidas"]["obra_concluida_id"]; ?>" />
            <a href="javascript:obras_concluidas_editar_foto()" title="SALVAR FOTO" class="botao botao_salvar_foto alinha_esquerda">SALVAR FOTO</a>
        </div>
    </form>
</div>