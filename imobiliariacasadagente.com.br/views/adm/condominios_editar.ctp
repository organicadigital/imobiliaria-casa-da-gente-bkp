<script>
$(document).ready(function(){
    <?php if($condominio["0"]["condominios"]["titulo"]!=""){ ?>
    $("label[for=titulo_text]").hide();
    <?php } ?>
    <?php if($condominio["0"]["condominios"]["descricao"]!=""){ ?>
    $("label[for=descricao_textarea]").hide();
    <?php } ?>
    <?php if($condominio["0"]["condominios"]["imoveis"]!=""){ ?>
    $("label[for=imoveis_textarea]").hide();
    <?php } ?>
});
</script>
<div id="container_interno" class="recuo1">
    <h1>Condomínios - Editar</h1>
    <form action="javascript:condominios_editar()" class="recuo2" method="post">
    	<div class="linha_formulario">
            <select name="data[Condominio][cidade_id]" id="cidade_combo">
            	<option value="">Cidade</option>
            	<?php
				foreach($cidades as $cidade){
					$permissao_acesso = $permissao == 'LITORAL' ? 1 : 0;
					if($cidade["cidades"]["litoral"] == $permissao_acesso || $permissao == 'ADMIN'){
						?>
						<option value="<?php echo $cidade["cidades"]["id"]; ?>" <?php echo $condominio[0]["condominios"]["cidade_id"] == $cidade["cidades"]["id"] ? 'selected="selected"' : ''; ?>><?php echo $cidade["cidades"]["nome"]; ?></option>
						<?php
					}
				};
				?>
            </select>
        </div>
        <div class="linha_formulario">
            <label for="titulo_text">título</label>
            <input type="text" name="data[Condominio][titulo]" id="titulo_text" maxlength="255" value="<?php echo $condominio[0]["condominios"]["titulo"]; ?>" class="edit" />
        </div>
        <div class="linha_formulario">
            <label for="descricao_textarea">descrição</label>
            <textarea name="data[Condominio][descricao]" id="descricao_textarea" cols="1" rows="1"  class="edit"><?php echo $condominio[0]["condominios"]["descricao"]; ?></textarea>
        </div>
        <div class="linha_formulario">
            <label for="imoveis_textarea">imóveis - separar por vírgulas</label>
            <textarea name="data[Condominio][imoveis]" id="imoveis_textarea" cols="1" rows="1"  class="edit"><?php echo $condominio[0]["condominios"]["imoveis"]; ?></textarea>
        </div>
        <div class="linha_formulario">
            <div class="linha_formulario2">
                <?php if($condominio[0]["condominios"]["lancamento"]){ ?>
                <input type="checkbox" name="data[Condominio][lancamento_checkbox]" id="lancamento_checkbox" checked="checked" />
                <?php }else{ ?>
                <input type="checkbox" name="data[Condominio][lancamento_checkbox]" id="lancamento_checkbox" />
                <?php } ?>
                <label for="lancamento_checkbox" class="reset">Lançamento</label>
            </div>
            <div class="linha_formulario2">
                <?php if($condominio[0]["condominios"]["destaque"]){ ?>
                <input type="checkbox" name="data[Condominio][destaque_checkbox]" id="destaque_checkbox" checked="checked" />
                <?php }else{ ?>
                <input type="checkbox" name="data[Condominio][destaque_checkbox]" id="destaque_checkbox" />
                <?php } ?>
                <label for="destaque_checkbox" class="reset">Destaque</label>
            </div>
        </div>
        <div class="linha_formulario">
            <input type="hidden" name="data[Condominio][id]" id="condominio_id" value="<?php echo $condominio["0"]["condominios"]["id"]; ?>" />
            <a href="javascript:condominios_editar()" title="SALVAR" class="botao botao_salvar alinha_esquerda">SALVAR</a>
        </div>
    </form>
</div>