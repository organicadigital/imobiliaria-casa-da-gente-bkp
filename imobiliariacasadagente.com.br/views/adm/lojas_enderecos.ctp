<div id="container_interno" class="recuo1">
    <h1>Lojas Endereços</h1>
    <div class="recuo2">
        <a href="<?php echo $this->webroot; ?>adm/lojasEnderecosCadastrar" title="CADASTRAR" class="botao botao_cadastrar">CADASTRAR</a>
        <?php if(count($enderecos)>0){ ?>
	        <table>
	            <thead>
	                <tr>
	                	<th>Cidade</th>
	                    <th>Endereço</th>
	                    <th>Telefone</th>
	                    <th>CEP</th>
	                    <th colspan="2">Ações</th>
	                </tr>
	            </thead>
	           
	            <tbody>
	            	<?php
	            	$i = 0;
					foreach($enderecos as $endereco){
						?>
						<tr<?php echo $i % 2 == 0 ? ' class="listra"' : '';?>>
							<td><?php echo $endereco["c"]["nome"]; ?></td>
		                    <td><?php echo $endereco["enderecos"]["endereco"]; ?></td>
		                    <td><?php echo $endereco["enderecos"]["telefone"]; ?></td>
		                    <td><?php echo $endereco["enderecos"]["cep"]; ?></td>
		                    <td>
		                        <a href="<?php echo $this->webroot; ?>adm/lojasEnderecosEditar/<?php echo $endereco["enderecos"]["id"]; ?>" title="Editar">Editar</a>
		                    </td>
		                    <td>
		                        <a href="javascript:confirmarExclusaoEndereco(<?php echo $endereco["enderecos"]["id"]; ?>)" title="Excluir">Excluir</a>
		                    </td>
		                </tr>
						<?php
						$i++;
					};
					?>
	            </tbody>
	        </table>
        <?php } ?>
    </div>
</div>