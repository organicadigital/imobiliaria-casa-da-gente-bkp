<script>
$(document).ready(function(){
    <?php if($foto["0"]["fotos_condominios"]["legenda"]!=""){ ?>
    $("label[for=legenda_text]").hide();
    <?php } ?>
    $("label[for=ordem_text]").hide();
});
</script>
<div id="container_interno" class="recuo1">
    <h1>Condomínios - Editar Foto</h1>
    <form action="javascript:condominios_editar_foto()" class="recuo2 espaco_superior" method="post">
        <div class="linha_formulario">
            <img src="<?php echo $this->webroot; ?>img/condominios/<?php echo $foto["0"]["fotos_condominios"]["id"]; ?>_3.jpg" alt="foto" />
        </div>
        <div class="linha_formulario">
            <label for="legenda_text">legenda</label>
            <input type="text" name="data[Foto][legenda]" id="legenda_text" maxlength="150" value="<?php echo $foto["0"]["fotos_condominios"]["legenda"]; ?>" class="edit" />
        </div>
        <div class="linha_formulario">
            <label for="ordem_text">ordem</label>
            <input type="text" name="data[Foto][ordem]" id="ordem_text" class="numerico edit" value="<?php echo $foto["0"]["fotos_condominios"]["ordem"]; ?>" />
        </div>
        <div class="linha_formulario">
            <div class="linha_formulario2">
                <?php if($foto["0"]["fotos_condominios"]["destaque"]){ ?>
                <input type="checkbox" name="data[Foto][destaque_checkbox]" id="destaque_checkbox" checked="checked" />
                <?php }else{ ?>
                <input type="checkbox" name="data[Foto][destaque_checkbox]" id="destaque_checkbox" />
                <?php } ?>
                <label for="destaque_checkbox" class="reset">Destaque</label>
            </div>
        </div>
        <div class="linha_formulario">
            <input type="hidden" name="data[Foto][id]" id="foto_id" value="<?php echo $foto["0"]["fotos_condominios"]["id"]; ?>" />
            <input type="hidden" name="data[Foto][condominio_id]" id="condominio_id" value="<?php echo $foto["0"]["fotos_condominios"]["condominio_id"]; ?>" />
            <a href="javascript:condominios_editar_foto()" title="SALVAR FOTO" class="botao botao_salvar_foto alinha_esquerda">SALVAR FOTO</a>
        </div>
    </form>
</div>