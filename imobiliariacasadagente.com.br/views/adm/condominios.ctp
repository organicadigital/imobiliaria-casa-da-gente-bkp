<div id="container_interno" class="recuo1">
    <h1>Condomínios</h1>
    <div class="recuo2">
        <a href="<?php echo $this->webroot; ?>adm/condominiosCadastrar" title="CADASTRAR" class="botao botao_cadastrar">CADASTRAR</a>
        <?php if(count($condominios)>0){ ?>
        <table>
            <thead>
                <tr>
                    <th>Título</th>
                    <th colspan="3">Ações</th>
                </tr>
            </thead>
            <tbody>
               <?php
               		$permissao_acesso = $permissao == 'LITORAL' ? 1 : 0;
                    $i = 0;
                    $i2 = 0;
                    foreach($condominios as $c){
                        $i++;
                        if($c["cidade"]["litoral"]==$permissao_acesso || $permissao == 'ADMIN'){
                        $i2++;
                        if($i2%2){
                ?>
                <tr class="listra">
                        <?php }else{ ?>
                <tr>
                        <?php } ?>
                    <td><?php echo $c["condominios"]["titulo"]; ?></td>
                    <td>
                        <a href="<?php echo $this->webroot; ?>adm/condominiosEditar/<?php echo $c["condominios"]["id"]; ?>" title="Editar">Editar</a>
                    </td>
                    <td>
                        <a href="<?php echo $this->webroot; ?>adm/condominiosEditarFotos/<?php echo $c["condominios"]["id"]; ?>" title="Editar Fotos">Editar Fotos</a>
                    </td>
                    <td>
                        <a href="<?php echo $this->webroot; ?>adm/condominiosExcluir/<?php echo $c["condominios"]["id"]; ?>" title="Excluir">Excluir</a>
                    </td>
                </tr>
                <?php }} ?>
            </tbody>
        </table>
        <?php } ?>
    </div>
</div>