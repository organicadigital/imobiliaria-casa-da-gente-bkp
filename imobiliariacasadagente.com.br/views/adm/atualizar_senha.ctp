<div id="container_interno" class="recuo1">
    <h1>Atualizar Senha</h1>
    <div class="recuo2">
        <br/><br/>
        <form action="javascript:atualizar_senha()" method="post">
        	<div class="linha_formulario">
                <label for="senha_atual">Senha Atual</label>
                <input type="password" name="senha_atual" id="senha_atual" />
            </div>
            <div class="linha_formulario">
                <label for="nova_senha">Nova Senha</label>
                <input type="password" name="nova_senha" id="nova_senha" />
            </div>
            <div class="linha_formulario">
                <label for="confirma_nova_senha">Confirma&ccedil;&atilde;o Nova Senha</label>
                <input type="password" name="confirma_nova_senha" id="confirma_nova_senha" />
            </div>
            <div class="linha_formulario">
                <input type="submit" value="ATUALIZAR" />
            </div>
        </form>
    </div>
</div>