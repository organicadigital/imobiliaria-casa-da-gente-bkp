<?php if(isset($fundo_f2f2f2)){ ?>
<section id="busca_imovel" class="busca fundo_f2f2f2">
<?php }else{ ?>
<section id="busca_imovel" class="busca pagina_imovel">
<?php } ?>
    <?php if(isset($busca_imovel)){ ?>
    <h2 class="titulo com_fundo">
        <span class="pagina">
            <span class="linha_h2 tamanho2"></span>
            <span class="texto">Busque seu imóvel</span>
        </span>
    </h2>
    <?php } ?>
    <div class="pagina">
        <form action="javascript:processaBuscaInterna(0)" method="post">
            <span class="titulo2">Filtre sua busca</span>
            <div class="coluna_formulario">
                <div class="linha2">
                    <label for="tipo_busca" class="formatacao2">tipo</label>
                    <select id="tipo_busca" name="data[Busca][tipo]" size="8">
                        <option value="tipo" selected="selected">tipos</option>
                        <?php foreach ($tipos as $t) { ?>
                            <option value="<?php echo utf8_encode($t); ?>"><?php echo utf8_encode($t); ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="linha">
                    <label for="codigo_busca" class="oculto">código</label>
                    <input type="text" id="codigo_busca" name="data[Busca][codigo]" value="código" />
                </div>
                <div class="linha_formulario extra" id="extra_busca">
                    <div class="coluna">
                        <input type="radio" name="data[Busca][extra]" id="todos_extra_radio" value="todos" checked="checked" />
                        <label for="todos_extra_radio">todos</label>
                    </div>
                    <div class="coluna">
                        <input type="radio" name="data[Busca][extra]" id="lancamentos_extra_radio"  value="lancamentos" />
                        <label for="lancamentos_extra_radio">lançamentos</label>
                    </div>
                    <div class="coluna">
                        <input type="radio" name="data[Busca][extra]" id="ofertas_extra_radio"  value="ofertas" />
                        <label for="ofertas_extra_radio">ofertas</label>
                    </div>
                </div>
            </div>
            <div class="coluna_formulario">
                <h3>Localização</h3>
                <div class="linha">
                    <label for="cidade_busca" class="formatacao2">cidade</label>
                    <select id="cidade_busca" name="data[Busca][cidade]" size="<?php echo count($cidades)+1; ?>">
                        <option value="cidade" selected="selected">cidade</option>
                        <?php foreach ($cidades2 as $c) { ?>
                            <option value="<?php echo $c["tb_imoveis"]["nome"]; ?>"><?php echo $c["tb_imoveis"]["nome"]; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="linha oculto">
                    <label for="bairro_busca" class="formatacao2">bairro</label>
                    <select id="bairro_busca" name="data[Busca][bairro]" size="5">
                        <option value="bairro" selected="selected">selecione uma cidade</option>
                    </select>
                </div>
            </div>
            <div class="coluna_formulario coluna2">
                <div class="linha2">
                    <label for="dormitorios_busca" class="formatacao2">dormitórios</label>
                    <select id="dormitorios_busca" name="data[Busca][dormitorios]" size="4">
                        <option value="dormitorios" selected="selected">dormitórios</option>
                        <option value="1 Dorm">1 dormitório</option>
                        <option value="2 Dorm">2 dormitórios</option>
                        <option value="3 Dorm">3 dormitórios</option>
                        <option value="4 Dorm">4 dormitórios</option>
                        <option value="5 Dorm">5 dormitórios</option>
                        <option value="6 Dorm">6 dormitórios</option>
                        <option value="7 Dorm">7 dormitórios</option>
                    </select>
                </div>
                <div class="linha">
                    <label for="garagem_busca" class="formatacao2">garagem</label>
                    <select id="garagem_busca" name="data[Busca][garagem]" size="3">
                        <option value="garagem" selected="selected">garagem</option>
                        <option value="1">com garagem</option>
                        <option value="0">sem garagem</option>
                    </select>
                </div>
            </div>
            <div class="coluna_formulario">
                <h3>Valores</h3>
                <div class="linha">
                    <label for="valor_de_busca" class="formatacao2">de</label>
                    <select id="valor_de_busca" name="data[Busca][valor_de]" size="5">
                        <option value="de">de</option>
                        <option value="0,00" selected="selected">R$ 0,00</option>
                        <option value="50000,00">R$ 50 mil</option>
                        <option value="100000,00">R$ 100 mil</option>
                        <option value="150000,00">R$ 150 mil</option>
                        <option value="200000,00">R$ 200 mil</option>
                        <option value="250000,00">R$ 250 mil</option>
                        <option value="300000,00">R$ 300 mil</option>
                        <option value="350000,00">R$ 350 mil</option>
                        <option value="400000,00">R$ 400 mil</option>
                        <option value="450000,00">R$ 450 mil</option>
                        <option value="500000,00">R$ 500 mil</option>
                        <option value="550000,00">R$ 550 mil</option>
                        <option value="600000,00">R$ 600 mil</option>
                        <option value="650000,00">R$ 650 mil</option>
                        <option value="700000,00">R$ 700 mil</option>
                        <option value="750000,00">R$ 750 mil</option>
                        <option value="800000,00">R$ 800 mil</option>
                        <option value="850000,00">R$ 850 mil</option>
                        <option value="900000,00">R$ 900 mil</option>
                        <option value="950000,00">R$ 950 mil</option>
                        <option value="1000000,00">R$ 1 milhão</option>
                    </select>
                </div>
                <div class="linha">
                    <label for="valor_ate_busca" class="formatacao2">até</label>
                    <select id="valor_ate_busca" name="data[Busca][valor_ate]" size="5">
                        <option value="99999999999,00" selected="selected">até</option>
                        <option value="50000,00">R$ 50 mil</option>
                        <option value="100000,00">R$ 100 mil</option>
                        <option value="150000,00">R$ 150 mil</option>
                        <option value="200000,00">R$ 200 mil</option>
                        <option value="250000,00">R$ 250 mil</option>
                        <option value="300000,00">R$ 300 mil</option>
                        <option value="350000,00">R$ 350 mil</option>
                        <option value="400000,00">R$ 400 mil</option>
                        <option value="450000,00">R$ 450 mil</option>
                        <option value="500000,00">R$ 500 mil</option>
                        <option value="550000,00">R$ 550 mil</option>
                        <option value="600000,00">R$ 600 mil</option>
                        <option value="650000,00">R$ 650 mil</option>
                        <option value="700000,00">R$ 700 mil</option>
                        <option value="750000,00">R$ 750 mil</option>
                        <option value="800000,00">R$ 800 mil</option>
                        <option value="850000,00">R$ 850 mil</option>
                        <option value="900000,00">R$ 900 mil</option>
                        <option value="950000,00">R$ 950 mil</option>
                        <option value="1000000,00">R$ 1 milhão</option>
                        <option value="99999999999,00">sem limite</option>
                    </select>
                </div>
            </div>
            <div class="coluna_formulario">
                <div class="linha">
                    <input type="hidden" name="data[Busca][offset]" value="0" />
                    <label for="btn_buscar" class="btn_buscar">
                        <input type="submit" value="Buscar" id="btn_buscar" />
                    </label>
                </div>
            </div>
        </form>
    </div>
</section>