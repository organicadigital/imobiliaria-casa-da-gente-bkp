<footer style="height:<?php echo count($enderecos)>2 ? count($enderecos)*85 : '200'; ?>px">
    <nav>
        <ul class="pagina">
            <li>
                <a href="<?php echo $this->webroot; ?>imoveis/null-null-null/9/0" title="imóveis">imóveis</a>
            </li>
            <li>
                <a href="<?php echo $this->webroot; ?>lancamentos" title="lançamentos">lançamentos</a>
            </li>
            <li>
                <a href="<?php echo $this->webroot; ?>imoveis/null-condominios-null/9/0" title="condomínios">condomínios</a>
            </li>
            <li>
                <a href="<?php echo $this->webroot; ?>imoveis/null-obras_concluidas-null/9/0" title="obras concluídas">obras concluídas</a>
            </li>
            <li>
                <a href="<?php echo $this->webroot; ?>vendaSeuImovel" title="venda seu imóvel">venda seu imóvel</a>
            </li>
            <li>
                <a href="<?php echo $this->webroot; ?>nossasLojas" title="localização">localização</a>
            </li>
            <li class="ultimo">
                <a href="<?php echo $this->webroot; ?>contato" title="contato">contato</a>
            </li>
        </ul>
    </nav>
    <div class="pagina">
        <section class="lojas">
            <h5>Localização</h5>
            <?php
				foreach($enderecos as $endereco){
					?>
					<dl class="endereco">
		                <dt><?php echo $endereco["c"]["nome"]; ?></dt>
		                <dd><?php echo $endereco["enderecos"]["endereco"]; ?> . <?php echo $endereco["enderecos"]["cep"]; ?></dd>
		                <dd><?php echo $endereco["enderecos"]["telefone"]; ?></dd>
		            </dl>
					<?php
				};
			?>
        </section>
        <span class="linha_vertical"></span>
        <section class="mais_acessados">
            <h5>Mais Acessados</h5>
            <ul>
                <li>
                    <a href="<?php echo $this->webroot; ?>imoveis/Gramado-Casa-null/9/0" title="Casa">Casa</a>
                </li>
                <li>
                    <a href="<?php echo $this->webroot; ?>imoveis/Gramado-null-null/9/0" title="Gramado">Gramado</a>
                </li>
                <li>
                    <a href="<?php echo $this->webroot; ?>imoveis/Gramado-Flat-null/9/0" title="Flat">Flat</a>
                </li>
                <li>
                    <a href="<?php echo $this->webroot; ?>imoveis/Gramado-ofertas-null/9/0" title="Ofertas">Ofertas</a>
                </li>
                <li>
                    <a href="<?php echo $this->webroot; ?>imoveis/Canela-null-null/9/0" title="Canela">Canela</a>
                </li>
                <li>
                    <a href="<?php echo $this->webroot; ?>lancamentos" title="Lançamentos">Lançamentos</a>
                </li>
            </ul>
        </section>
        <span class="linha_vertical"></span>
        <section class="acompanhe">
            <h5>Acompanhe</h5>
            <ul id="acompanhe">
                <li>
                    <span class="mac"></span>
                    <span id="caixa_mac">
                        este site também pode ser
                        <br />acessado via iPad e iPhone
                    </span>
                </li>
            </ul>
        </section>
        <span class="linha_vertical"></span>
        </a>
    </div>
</footer>