<?php $url_site = "http://www.imobiliariacasadagente.com.br/"; ?>
<html>
    <head>
        <style>
            body { font-family:arial, verdana, sans-serif; background:#ffffff;}
            a { display: block; color:#808080;}
            p { font-size:12px; color:#808080; text-align: justify;}
            h2 { font-size:15px; color:#333; }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" >
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <table width="750" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <td>
								<a href="http://www.imobiliariacasadagente.com.br/">
									<img src="http://thumbit.site321.com.br/img-e1c083acc5001d4f.jpg" alt="News de Notícias" border="0" width="750" height="115" style="display:block; margin:0; padding:0;" border="0" width="750" height="115" />
								</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <?php $i=0; foreach($imoveis as $im){ $i++; ?>
            <tr>
                <td>
                    <table width="750" cellpadding="0" cellspacing="0" align="center" <?php if($i%2){ ?>bgcolor="#f2f2f2"<?php } ?>>
                        <tr>
                            <td style="padding-left:40; padding-top:31; padding-bottom:23; padding-right:20">
                                <a href="<?php echo $url_site; ?>imovel/<?php echo $im["url"]; ?>/<?php echo $im["codigo"]; ?>/0/0/<?php echo $im["filial"]; ?>" title="<?php echo $im["titulo"]; ?>" style="padding:6; border:1px solid #cccccc; background-color:#ffffff;">
                                    <img src="<?php echo $url_site; ?>img/imoveis/<?php echo $im["foto"]; ?>_2.jpg" alt="<?php echo $im["titulo"]; ?>" width="276" height="239" border="0" />
                                </a>
                            </td>
                            <td style="padding-right:40;">
                                <h2>
                                    <?php echo $im["titulo"]; ?>
                                </h2>
                                <p><?php echo $im["descricao"]; ?></p>
                                <div style="text-align:right;">
                                    <?php if($i%2){ ?>
                                    <?php if($im["cidade"]=="Gramado"){ ?>
                                    <img src="<?php echo $url_site; ?>img/news/gramado_cinza.jpg" alt="saiba mais" border="0" width="160" height="38" />
                                    <?php }else{ ?>
                                    <img src="<?php echo $url_site; ?>img/news/canela_cinza.jpg" alt="saiba mais" border="0" width="160" height="38" />
                                    <?php } ?>
                                    <?php }else{ ?>
                                    <?php if($im["cidade"]=="Gramado"){ ?>
                                    <img src="<?php echo $url_site; ?>img/news/gramado_branco.jpg" alt="saiba mais" border="0" width="160" height="38" />
                                    <?php }else{ ?>
                                    <img src="<?php echo $url_site; ?>img/news/canela_branco.jpg" alt="saiba mais" border="0" width="160" height="38" />
                                    <?php } ?>
                                    <?php } ?>
                                </div>
                                <div style="text-align:right;">
                                    <?php if($im["cidade"]=="Gramado"){ ?>
                                    <a href="mailto:gramado@imobiliariacasadagente.com.br" title="<?php echo $im["titulo"]; ?>" style="text-align:right;">
                                    <?php }else{ ?>
                                    <a href="mailto:gramado@imobiliariacasadagente.com.br" title="<?php echo $im["titulo"]; ?>" style="text-align:right;">
                                    <?php } ?>
                                        <?php if($i%2){ ?>
                                        <img src="<?php echo $url_site; ?>img/news/saiba_mais_cinza.jpg" alt="saiba mais" border="0" width="94" height="32" />
                                        <?php }else{ ?>
                                        <img src="<?php echo $url_site; ?>img/news/saiba_mais_branco.jpg" alt="saiba mais" border="0" width="94" height="32" />
                                        <?php } ?>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td>
                    <table width="750" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <td>
                                <img src="http://thumbit.site321.com.br/img-38abbde5eb6b472c.jpg" alt="Casa da Gente" border="0" width="750" height="110" style="display:block; margin:0; padding:0;" border="0" width="750" height="110" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>