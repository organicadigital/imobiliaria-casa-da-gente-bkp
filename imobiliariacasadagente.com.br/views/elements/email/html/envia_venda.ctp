<html>
    <head>
        <style>
            body { font-family:arial, verdana, sans-serif; color:#84867b; background:#ffffff;}
            p { font-size:12px; margin:10px 0 0 0; }
        </style>
    </head>
    <body>
        <h3><strong>Nome: </strong><?php echo $nome; ?></h3>
        <h3><strong>Telefone: </strong><?php echo $ddd." ".$telefone; ?></h3>
        <h3><strong>E-mail: </strong><?php echo $email; ?></h3>
        <h3><strong>Tipo: </strong><?php echo $tipo; ?></h3>
        <h3><strong>Cidade: </strong><?php echo $cidade; ?></h3>
    </body>
</html>