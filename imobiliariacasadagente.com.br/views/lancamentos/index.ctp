<h1  class="titulo">
    <span class="pagina">
        <span class="texto">Lançamentos</span>
        <span class="linha_h2 tamanho7"></span>
    </span>
</h1>
<span class="divisoria1">
    <span class="divisoria2"></span>
</span>
<?php echo $this->element("busca_interna", array("fundo_f2f2f2"=>1)); ?>
<span class="divisoria1 sem_espaco">
    <span class="divisoria2"></span>
</span>
<section class="fundo_f2f2f2">
    <div class="pagina">
        <?php if (count($imoveis) == 0) { ?>
            <p>Nenhum imóvel encontrado.</p>
        <?php } ?>
            <nav class="paginacao">
        <?php if ($totalPaginas > 1) { ?>
            
                <ul style="width: <?php echo 172 + ($totalPaginas * 26); ?>px">
                    <li class="anterior">
                        <?php if ($paginaAtual == 1) { ?>
                            <a href="javascript:vazio()" title="Anterior">Anterior</a>
                        <?php } else { ?>
                            <a href="<?php echo $this->webroot; ?>lancamentos/<?php echo $limit . "/" . ($offset - $limit); ?>" title="Anterior">Anterior</a>
                        <?php } ?>
                    </li>
                    <?php for ($i = 1; $i <= $totalPaginas; $i++) {
                    if ($paginaAtual == $i) { ?>
                    <li class="atual">
                    <?php } else { ?>
                    <li>    
                    <?php } ?>
                        <a href="<?php echo $this->webroot; ?>lancamentos/<?php echo $limit . "/" . ($i - 1) * $limit; ?>" title="ir para página <?php echo $i; ?>"><?php echo $i; ?></a>
                    </li>
                    <?php } ?>
                    <li class="proxima">
                    <?php if ($paginaAtual == $totalPaginas) { ?>
                        <a href="javascript:vazio()" title="Próxima">Próxima</a>
                    <?php } else { ?>
                        <a href="<?php echo $this->webroot; ?>lancamentos/<?php echo $limit . "/" . ($offset + $limit); ?>" title="Próxima">Próxima</a>
                    <?php } ?>
                    </li>
                </ul>
            
        <?php } ?>
                </nav>
        <?php
        foreach ($imoveis as $i) {
            $informacoes = explode("!", $i["i"]["f_descricao"], 2);
            $tit = trim(utf8_encode($informacoes[0]));
            $des = trim(utf8_encode($informacoes[1]));
            $url_formatada = str_replace(array("/", " ", '"', ","), array("-", "-", "", ""), $tit);
            ?>
            <article class="imovel">
    <?php if ($i["i"]["f_lancamento"]) { ?>
                    <span class="selo selo_lancamentos">
                        <span class="oculto">Lançamentos</span>
                    </span>
                <?php } ?>
    <?php if ($i[0]["eh_oferta"]) { ?>
                    <span class="selo selo_ofertas">
                        <span class="oculto">Ofertas</span>
                    </span>
                <?php } ?>
    <?php if ($i[0]["eh_construtora"]) { ?>
                    <span class="selo selo_construtora">
                        <span class="oculto">Construtora</span>
                    </span>
                <?php } ?>
    <?php if ($i[0]["eh_luxo"]) { ?>
                    <span class="selo selo_luxo">
                        <span class="oculto">Imóveis de Luxo</span>
                    </span>
    <?php } ?>
                <div class="informacoes">
                    <span class="sombra"></span>
                    <h3>
                        <a href="<?php echo $this->webroot; ?>imovel/<?php echo $url_formatada . "/" . $i["i"]["f_id"]; ?>/0/0/<?php echo $i["i"]["f_filial"]; ?>" title="<?php echo $tit; ?>"><?php echo $tit; ?></a>
                    </h3>
                    <p>
                        <?php echo $text->truncate($des, 150, array("ending" => "...", "exact" => false)); ?>
                        <br />
                        <br /><?php
                        $valor="Consultar";
                        if($i["i"]["f_tipo"] != ""){
                            $valor = number_format($i["i"]["f_valor"],2,",",".");
                        }
                        if ($i["i"]["f_valor"] <= 100) {
                            echo "Valor sob consulta";
                        } else {
                            echo "R$ " . $valor;
                        }
                        ?>
                    </p>
                    <span class="linha_horizontal"></span>
                    <span class="cidade"><?php echo $i["i"]["f_cidade"]; ?></span>
                    <span class="linha_horizontal2"></span>
                    <a href="<?php echo $this->webroot; ?>imovel/<?php echo $url_formatada . "/" . $i["i"]["f_id"]; ?>/0/0/<?php echo $i["i"]["f_filial"]; ?>" title="<?php echo $tit; ?>">
                        <span class="seta"></span>
                        saiba mais
                    </a>
                </div>
                <div class="imagem">
                    <a href="<?php echo $this->webroot; ?>imovel/<?php echo $url_formatada . "/" . $i["i"]["f_id"]; ?>/0/0/<?php echo $i["i"]["f_filial"]; ?>" title="<?php echo $tit; ?>">
                        <img src="<?php echo $this->webroot; ?>images?url_image=img/imoveis/<?php echo $i[0]["imagem_id"]; ?>_2.jpg" alt="<?php echo $tit; ?>" />
                    </a>
                </div>
            </article>
<?php } ?>
            <nav class="paginacao">
        <?php if ($totalPaginas > 1) { ?>
            
                <ul style="width: <?php echo 172 + ($totalPaginas * 26); ?>px">
                    <li class="anterior">
                        <?php if ($paginaAtual == 1) { ?>
                            <a href="javascript:vazio()" title="Anterior">Anterior</a>
                        <?php } else { ?>
                            <a href="<?php echo $this->webroot; ?>lancamentos/<?php echo $limit . "/" . ($offset - $limit); ?>" title="Anterior">Anterior</a>
                        <?php } ?>
                    </li>
                    <?php for ($i = 1; $i <= $totalPaginas; $i++) {
                    if ($paginaAtual == $i) { ?>
                    <li class="atual">
                    <?php } else { ?>
                    <li>    
                    <?php } ?>
                        <a href="<?php echo $this->webroot; ?>lancamentos/<?php echo $limit . "/" . ($i - 1) * $limit; ?>" title="ir para página <?php echo $i; ?>"><?php echo $i; ?></a>
                    </li>
                    <?php } ?>
                    <li class="proxima">
                    <?php if ($paginaAtual == $totalPaginas) { ?>
                        <a href="javascript:vazio()" title="Próxima">Próxima</a>
                    <?php } else { ?>
                        <a href="<?php echo $this->webroot; ?>lancamentos/<?php echo $limit . "/" . ($offset + $limit); ?>" title="Próxima">Próxima</a>
                    <?php } ?>
                    </li>
                </ul>
        <?php } ?>
                </nav>
    </div>
</section>