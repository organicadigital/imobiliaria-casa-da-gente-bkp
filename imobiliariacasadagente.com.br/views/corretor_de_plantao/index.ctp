<h1  class="titulo">
    <span class="pagina">
        <span class="texto">Corretor de plantão</span>
        <span class="linha_h2 tamanho6"></span>
    </span>
</h1>
<span class="divisoria1">
    <span class="divisoria2"></span>
</span>
<?php echo $this->element("busca_interna", array("fundo_f2f2f2"=>1)); ?>
<span class="divisoria1 sem_espaco">
    <span class="divisoria2"></span>
</span>
<section id="conteudo_contato" class="fundo_e6e6e6">
    <div class="pagina">
        <h2>
            <span class="imagem"></span>
            <span class="pequeno">corretor de</span>
            <span class="grande">PLANTÃO</span>
        </h2>
        <div class="coluna1">
            <section class="imobiliaria">
                <h3>Gramado</h2>
                    <p>
                        Rua Garibaldi, 580
                        <br />Cep: 95670-000
                        <br />(54) 3295.5555
                    </p>
                    <ul>
                        <li>
                            <address>
                                Carla Behling . Corretora
                                <br />(54) 9634.9264
                                <br />carla@casadagente.net
                                <br />CRECI: 31.045
                            </address>
                        </li>
                        <li>
                            <address>
                                Denise Santos . Gerente
                                <br />(54) 8134.5846
                                <br />denise@casadagente.net
                                <br />CRECI: 34.268
                            </address>
                        </li>
                        <li>
                            <address>
                                Lígia Kingeski . Corretora
                                <br />(54) 9962.8400
                                <br />ligia@casadagente.net
                                <br />CRECI: 10.824
                            </address>
                        </li>
                        <li>
                            <address>
                                Rodrigo Rolim . Corretor
                                <br />(54) 9605.3656
                                <br />rodrigo@casadagente.net
                                <br />CRECI: 31.709
                            </address>
                        </li>
                    </ul>
            </section>
        </div>
        <div class="coluna2">
            <section class="imobiliaria">
                <h3>Canela</h2>
                    <p>
                        Rua Paul Harris, 55 - Sala 10
                        <br />Cep: 95680-000
                        <br />(54) 3278.2009
                    </p>
                    <ul>
                        <li>
                            <address>
                                Alessandra Famoso
                                <br />(54) 8125.2943
                                <br />alessandra@casadagente.net
                                <br />CRECI: 34.237
                            </address>
                        </li>
                        <li>
                            <address>
                                André Behling
                                <br />(54) 9603.8888
                                <br />andre@casadagente.net
                                <br />CRECI: 19.439
                            </address>
                        </li>
						<li>
                            <address>
                                Carol Pedroso
                                <br />(54) 9144.4558
                                <br />carol@casadagente.net
                                <br />CRECI: 40.806
                            </address>
                        </li>
                    </ul>
            </section>
        </div>
    </div>
</section>