<?php
class Endereco extends AppModel {
    var $name = 'Endereco';

    function getAll(){
        return $this->query(
"
    select
        *
    from
        enderecos, cidades as c
    where 
    	cidade_id = c.id
"
        );
    }
    
    function get($id){
        return $this->query(
"
    select
        *
    from
        enderecos
    where
    	id = ".$id."
"
        );
    }
    
    function update($id, $cidade, $endereco, $telefone, $cep){
        return $this->query(
"
    update
        enderecos set
        	cidade_id = ".$cidade."
            ,endereco = '".$endereco."'
            , telefone = '".$telefone."'
            , cep = '".$cep."'
        where
            id = '".$id."';
"
        );
    }
    
    function insert($cidade, $endereco, $telefone, $cep){
        return $this->query(
"
    insert into
        enderecos (cidade_id, endereco, telefone, cep)
    values
    	(".$cidade.",'".$endereco."','".$telefone."','".$cep."');
"
        );
    }
    
	function delete($id){
        return $this->query(
"
    delete from enderecos where id = ".$id.";
"
        );
    }
}
?>