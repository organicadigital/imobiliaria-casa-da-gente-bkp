<?php
class Imovel extends AppModel {
    var $name = 'Imovel';
    var $useTable = "tb_imoveis";

    function getAll(){
        return $this->query(
"
    select
        *
        , (
            exists (select * from imoveis_de_luxo where imovel_id = f_id and filial = f_filial)
        )as eh_luxo
        , (
            exists (select * from imoveis_da_construtora where imovel_id = f_id and filial = f_filial)
        )as eh_construtora
        , (
            exists (select * from ofertas where imovel_id = f_id and filial = f_filial)
        )as eh_oferta
    from
        tb_imoveis
"
        );
    }

    #Existia anteriormente um problema sério de perfomance no qual a página não carregava em virtude das consultas de fotos
    #que a função de definir o array de imóveis antes de passar pra view causava. Ele pegava todos os imóveis, iterava sobre
    #eles e fazia uma consulta no banco pra buscar as fotos associadas a esse imóvel. A abordagem agora é realizar duas
    #consultas no banco trazendo todos os imóveis e todas as fotos, tratando de relacionar os dois dentro do PHP.
    function crossJoin($imoveis, $fotos) {
        #Create final return array
        $imoveis_fotos = array();
        #Para cada imóvel
        foreach($imoveis as $im){
            #Array de fotos temporárias que tem o mesmo f_id que o imovel em questao
            $fotos_temp = array();
            #Para cada foto
            foreach($fotos as $f) {
                #Se o imovel e a foto tiverem o mesmo id, adiciona a foto em questão no array temporario
                if($im["tb_imoveis"]["f_id"] == $f["tb_imoveis_fotos"]["f_id"]){
                    $fotos_temp[] = $f;
                }
            }
            #Para cada foto do imóvel
            foreach($fotos_temp as $foto){
                #Se a foto em questão for uma foto destaque, limpa o array temporário e coloca essa foto
                if($foto["tb_fotos_imoveis"]["f_destaque"] == 1){
                    $fotos_temp = [];
                    $fotos_temp = $foto;
                }
            }
            #Se nenhum foto tiver f_destaque setado (o que implica no tamanho dele ser maior que um), coloca a primeira foto
            # encontrada como item da variável $fotos_temp. 
            if(sizeof($fotos_temp) > 1){
                $fotos_temp = $fotos_temp[0];
            }
            #Para cada imovel, adiciona um elemento no array que é o id, as informaçoes do imovel em questão e a foto.
            $imoveis_fotos[] = array(
                "id" => $im["tb_imoveis"]["f_id"],
                "info" => $im,
                "fotos" => $fotos_temp,
            );
            $fotos_temp = [];
        }
        return $imoveis_fotos;
    }

    function getFotos($f_id, $f_filial){
      return $this->query(
      "
        SELECT
          *
        FROM
          tb_imoveis_fotos
        WHERE
          f_id = ".$f_id."
          AND 
            f_filial = '".$f_filial."';
        ORDER BY
          f_destaque DESC, ordem ASC;
      "
        );
    }
    
    function getAllFotos(){
        return $this->query(
"
    select
        *
    from
        tb_imoveis_fotos
    order by
        f_codigo
"
        );
    }
    
    function getTipos(){
        return $this->query(
"
    select
        retorna_tipos(f_tipo) as f_tipos
    from
        tb_imoveis
    group by
        f_tipos
    order by
        f_tipos;
"
        );
    }
    
    function get($f_id, $f_filial){
        return $this->query(
"
    select
        *
    from
        tb_imoveis
    where
        f_id = ".$f_id."
        and f_filial = '".$f_filial."';
"
        );
    }
    
    function getFotos($f_id, $f_filial){
        return $this->query(
"
    select
        *
    from
        tb_imoveis_fotos
    where
        f_id = ".$f_id."
        and f_filial = '".$f_filial."'
    order by
        f_destaque DESC, ordem ASC;
"
        );
    }
    
    function getMaisImoveis(
            $tipo
            , $codigo
            , $extra
            , $cidade
            , $bairro
            , $dormitorios
            , $garagem
            , $valor_de
            , $valor_ate
            , $offset
            , $limit
            ){
        //valores
        $valor_de = str_replace(",", ".", $valor_de);
        $valor_ate = str_replace(",", ".", $valor_ate);
        if($valor_de!="de" && $valor_ate!="ate"){
            $valores_txt = "i.f_valor BETWEEN ".$valor_de." AND ".$valor_ate;
        }elseif($valor_de!="de" && $valor_ate=="ate"){
            $valores_txt = "i.f_valor > ".$valor_de;
        }elseif($valor_de=="de" && $valor_ate!="ate"){
            $valores_txt = "i.f_valor < ".$valor_ate;
        }else{
            $valores_txt = "i.f_valor > 0";
        }
        //tipos
        $tipo_txt = "";
        if($tipo!="tipo"){
            $tipo_txt = "and i.f_tipo like '".$tipo."%'";
        }
        //codigo
        $codigo_txt = "";
        if($codigo!="código"){
            $codigo_txt = "and i.f_codigo like '%".$codigo."%'";
        }
        //extra
        $extra_txt = "";
        if($extra!="todos"){
            $extra_txt = "having eh_oferta";
            if($extra=="lancamentos"){
                $extra_txt = "and i.f_lancamento";
            }
        }
        //cidade
        $cidade_txt = "";
        if($cidade!="cidade"){
            $cidade_txt = "and i.f_cidade = '".$cidade."'";
        }
        //bairro
        $bairro_txt = "";
        if($bairro!="bairro"){
            $bairro_txt = "and i.f_bairro = '".$bairro."'";
        }
        //dormitorios
        $dormitorios_txt = "";
        if($dormitorios!="dormitorios"){
            $dormitorios_txt = "and i.f_tipo like '%".$dormitorios."'";
        }
        //garagem
        $garagem_txt = "";
        if($garagem!="garagem"){
            $garagem_txt = "and i.f_numero_garagens <> 0";
            if($garagem == "0"){
                $garagem_txt = "and i.f_numero_garagens = 0";
            }
        }
        $consulta = 
"
    SELECT
        i.*
        , (
            select 
                f_id_foto
            from
                tb_imoveis_fotos
            where
                f_id = i.f_id
                and f_filial = i.f_filial
            order by
                f_destaque DESC, ordem ASC
            limit
                1
        ) as imagem_id
        , (
            exists (select * from imoveis_de_luxo where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_luxo
        , (
            exists (select * from imoveis_da_construtora where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_construtora
        , (
            exists (select * from ofertas where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_oferta
    FROM
        tb_imoveis as i
    WHERE
        ".$valores_txt."
        ".$tipo_txt."
        ".$codigo_txt."
        ".$cidade_txt."
        ".$bairro_txt."
        ".$dormitorios_txt."
        ".$garagem_txt."
        ".$extra_txt."
     ORDER BY
        i.f_valor
     LIMIT
        ".$limit."
     OFFSET
        ".$offset."
";
        
        return $this->query($consulta);
        
    }
    
    function getQtdPaginasMaisImoveis(
            $tipo
            , $codigo
            , $extra
            , $cidade
            , $bairro
            , $dormitorios
            , $garagem
            , $valor_de
            , $valor_ate
            , $itens_por_pagina
            ){
        //valores
        $valor_de = str_replace(",", ".", $valor_de);
        $valor_ate = str_replace(",", ".", $valor_ate);
        if($valor_de!="de" && $valor_ate!="ate"){
            $valores_txt = "i.f_valor BETWEEN ".$valor_de." AND ".$valor_ate;
        }elseif($valor_de!="de" && $valor_ate=="ate"){
            $valores_txt = "i.f_valor > ".$valor_de;
        }elseif($valor_de=="de" && $valor_ate!="ate"){
            $valores_txt = "i.f_valor < ".$valor_ate;
        }else{
            $valores_txt = "i.f_valor > 0";
        }
        //tipos
        $tipo_txt = "";
        if($tipo!="tipo"){
            $tipo_txt = "and i.f_tipo like '".$tipo."%'";
        }
        //codigo
        $codigo_txt = "";
        if($codigo!="código"){
            $codigo_txt = "and i.f_codigo like '%".$codigo."%'";
        }
        //extra
        $extra_txt = "";
        if($extra!="todos"){
            $extra_txt = "having eh_oferta";
            if($extra=="lancamentos"){
                $extra_txt = "and i.f_lancamento";
            }
        }
        //cidade
        $cidade_txt = "";
        if($cidade!="cidade"){
            $cidade_txt = "and i.f_cidade = '".$cidade."'";
        }
        //bairro
        $bairro_txt = "";
        if($bairro!="bairro"){
            $bairro_txt = "and i.f_bairro = '".$bairro."'";
        }
        //dormitorios
        $dormitorios_txt = "";
        if($dormitorios!="dormitorios"){
            $dormitorios_txt = "and i.f_tipo like '%".$dormitorios."'";
        }
        //garagem
        $garagem_txt = "";
        if($garagem!="garagem"){
            $garagem_txt = "and i.f_numero_garagens <> 0";
            if($garagem == "0"){
                $garagem_txt = "and i.f_numero_garagens = 0";
            }
        }
        $consulta = 
"
select count(*)/".$itens_por_pagina." as qtd from (
    SELECT
        i.*
        , (
            exists (select * from ofertas where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_oferta
    FROM
        tb_imoveis as i
    WHERE
        ".$valores_txt."
        ".$tipo_txt."
        ".$codigo_txt."
        ".$cidade_txt."
        ".$bairro_txt."
        ".$dormitorios_txt."
        ".$garagem_txt."
        ".$extra_txt."
) as i2
";
        
        return $this->query($consulta);
        
    }
    
    function getDestaques($offset, $limit){
        $consulta = 
"
    SELECT
        i.*
        , (
            select 
                f_id_foto
            from
                tb_imoveis_fotos
            where
                f_id = i.f_id
                and f_filial = i.f_filial
            order by
                f_destaque DESC, ordem ASC
            limit
                1
        ) as imagem_id
        , (
            exists (select * from imoveis_de_luxo where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_luxo
        , (
            exists (select * from imoveis_da_construtora where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_construtora
        , (
            exists (select * from ofertas where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_oferta
    FROM
        tb_imoveis as i
    WHERE
        i.f_destaque
     ORDER BY
        i.f_valor
     LIMIT
        ".$limit."
     OFFSET
        ".$offset."
";
        
        return $this->query($consulta);
        
    }
    
    function getQtdDestaques($itens_por_pagina){
        $consulta = 
"
select count(*)/".$itens_por_pagina." as qtd from (
    SELECT
        i.*
        , (
            exists (select * from ofertas where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_oferta
    FROM
        tb_imoveis as i
    WHERE
        i.f_destaque
) as i2
";
        
        return $this->query($consulta);
        
    }
    
    function getByCidadeTipoExtraLancamentos($cidade, $tipo, $limit, $offset){
        return $this->query(
"
    SELECT
        i.*
        , (
            select 
                f_id_foto
            from
                tb_imoveis_fotos
            where
                f_id = i.f_id
                and f_filial = i.f_filial
            order by
                f_destaque DESC, ordem ASC
            limit
                1
        ) as imagem_id
        , (
            exists (select * from imoveis_de_luxo where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_luxo
        , (
            exists (select * from imoveis_da_construtora where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_construtora
        , (
            exists (select * from ofertas where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_oferta
    FROM
        tb_imoveis as i
    WHERE
        i.f_cidade = '".$cidade."'
        and i.f_tipo like '".$tipo."%'
        and i.f_lancamento = 1
    ORDER BY
        i.f_valor
     LIMIT
        ".$limit."
     OFFSET
        ".$offset."
"
        );
    }
    
    function getQtdByCidadeTipoExtraLancamentos($cidade, $tipo, $itens_por_pagina){
        return $this->query(
"
select count(*)/".$itens_por_pagina." as qtd from (
    SELECT
        i.*
    FROM
        tb_imoveis as i
    WHERE
        i.f_cidade = '".$cidade."'
        and i.f_tipo like '".$tipo."%'
        and i.f_lancamento = 1
) as i2;
"
        );
    }
    
    function getByCidadeTipoExtraOfertas($cidade, $tipo, $limit, $offset){
        return $this->query(
"
    SELECT
        i.*
        , (
            select 
                f_id_foto
            from
                tb_imoveis_fotos
            where
                f_id = i.f_id
                and f_filial = i.f_filial
            order by
                f_destaque DESC, ordem ASC
            limit
                1
        ) as imagem_id
        , (
            exists (select * from imoveis_de_luxo where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_luxo
        , (
            exists (select * from imoveis_da_construtora where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_construtora
        , (
            exists (select * from ofertas where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_oferta
    FROM
        tb_imoveis as i
    WHERE
        i.f_cidade = '".$cidade."'
        and i.f_tipo like '".$tipo."%'
    HAVING
        eh_oferta
    ORDER BY
        i.f_valor
     LIMIT
        ".$limit."
     OFFSET
        ".$offset."
"
        );
    }
    
    function getQtdByCidadeTipoExtraOfertas($cidade, $tipo, $itens_por_pagina){
        return $this->query(
"
select count(*)/".$itens_por_pagina." as qtd from (
    SELECT
        i.*
        , (
            exists (select * from ofertas where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_oferta
    FROM
        tb_imoveis as i
    WHERE
        i.f_cidade = '".$cidade."'
        and i.f_tipo like '".$tipo."%'
    HAVING
        eh_oferta
)as i2;
"
        );
    }
    
    function getByCidadeExtraLancamentos($cidade, $limit, $offset){
        return $this->query(
"
    SELECT
        i.*
        , (
            select 
                f_id_foto
            from
                tb_imoveis_fotos
            where
                f_id = i.f_id
                and f_filial = i.f_filial
            order by
                f_destaque DESC, ordem ASC
            limit
                1
        ) as imagem_id
        , (
            exists (select * from imoveis_de_luxo where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_luxo
        , (
            exists (select * from imoveis_da_construtora where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_construtora
        , (
            exists (select * from ofertas where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_oferta
    FROM
        tb_imoveis as i
    WHERE
        i.f_cidade = '".$cidade."'
        and i.f_lancamento = 1
    ORDER BY
        i.f_valor
     LIMIT
        ".$limit."
     OFFSET
        ".$offset."
"
        );
    }
    
    function getQtdByCidadeExtraLancamentos($cidade, $itens_por_pagina){
        return $this->query(
"
select count(*)/".$itens_por_pagina." as qtd from (
    SELECT
        i.*
    FROM
        tb_imoveis as i
    WHERE
        i.f_cidade = '".$cidade."'
        and i.f_lancamento = 1
) as i2;
"
        );
    }
    
    function getByCidadeExtraOfertas($cidade, $limit, $offset){
        return $this->query(
"
    SELECT
        i.*
        , (
            select 
                f_id_foto
            from
                tb_imoveis_fotos
            where
                f_id = i.f_id
                and f_filial = i.f_filial
            order by
                f_destaque DESC, ordem ASC
            limit
                1
        ) as imagem_id
        , (
            exists (select * from imoveis_de_luxo where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_luxo
        , (
            exists (select * from imoveis_da_construtora where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_construtora
        , (
            exists (select * from ofertas where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_oferta
    FROM
        tb_imoveis as i
    WHERE
        i.f_cidade = '".$cidade."'
    HAVING
        eh_oferta
    ORDER BY
        i.f_valor
     LIMIT
        ".$limit."
     OFFSET
        ".$offset."
"
        );
    }
    
    function getQtdByCidadeExtraOfertas($cidade, $itens_por_pagina){
        return $this->query(
"
select count(*)/".$itens_por_pagina." as qtd from (
    SELECT
        i.*
        , (
            exists (select * from ofertas where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_oferta
    FROM
        tb_imoveis as i
    WHERE
        i.f_cidade = '".$cidade."'
    HAVING
        eh_oferta
)as i2
"
        );
    }
    
    function getByCidadeExtraCondominios($cidade, $limit, $offset){
        $cidades = "";
        if($cidade != "null"){
            $cidades = "and cidades.nome = '".$cidade."'";
        }
        return $this->query(
"
    select
        condominios.*
        , cidades.nome as nome
        , fotos_condominios.id as imagem_id
    from
        condominios
        , cidades
        , fotos_condominios
    where
        condominios.cidade_id = cidades.id
        ".$cidades."
        and fotos_condominios.condominio_id = condominios.id
        and fotos_condominios.destaque = 1
     LIMIT
        ".$limit."
     OFFSET
        ".$offset."
"
        );
    }
    
    function getQtdByCidadeExtraCondominios($cidade, $itens_por_pagina){
        $cidades = "";
        if($cidade != "null"){
            $cidades = "and cidades.nome = '".$cidade."'";
        }
        return $this->query(
"
select count(*)/".$itens_por_pagina." as qtd from (
    select
        condominios.*
    from
        condominios
        , cidades
    where
        condominios.cidade_id = cidades.id
        ".$cidades."
)as i2
"
        );
    }









    function getByCidadeExtraObrasConcluidas($cidade, $limit, $offset){
        $cidades = "";
        if($cidade != "null"){
            $cidades = "and cidades.nome = '".$cidade."'";
        }
        return $this->query(
"
    select
        obras_concluidas.*
        , cidades.nome as nome
        , fotos_obras_concluidas.id as imagem_id
    from
        obras_concluidas
        , cidades
        , fotos_obras_concluidas
    where
        obras_concluidas.cidade_id = cidades.id
        ".$cidades."
        and fotos_obras_concluidas.obra_concluida_id = obras_concluidas.id
        and fotos_obras_concluidas.destaque = 1
     LIMIT
        ".$limit."
     OFFSET
        ".$offset."
"
        );
    }

    function getQtdByCidadeExtraObrasConcluidas($cidade, $itens_por_pagina){
        $cidades = "";
        if($cidade != "null"){
            $cidades = "and cidades.nome = '".$cidade."'";
        }
        return $this->query(
"
select count(*)/".$itens_por_pagina." as qtd from (
    select
        obras_concluidas.*
    from
        obras_concluidas
        , cidades
    where
        obras_concluidas.cidade_id = cidades.id
        ".$cidades."
)as i2
"
        );
    }








    function getByCidadeTipo($cidade, $tipo, $limit, $offset){
        return $this->query(
"
    SELECT
        i.*
        , (
            select 
                f_id_foto
            from
                tb_imoveis_fotos
            where
                f_id = i.f_id
                and f_filial = i.f_filial
            order by
                f_destaque DESC, ordem ASC
            limit
                1
        ) as imagem_id
        , (
            exists (select * from imoveis_de_luxo where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_luxo
        , (
            exists (select * from imoveis_da_construtora where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_construtora
        , (
            exists (select * from ofertas where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_oferta
    FROM
        tb_imoveis as i
    WHERE
        i.f_cidade = '".$cidade."'
        and i.f_tipo like '".$tipo."%'
    ORDER BY
        i.f_valor
     LIMIT
        ".$limit."
     OFFSET
        ".$offset."
"
        );
    }
    
    function getQtdByCidadeTipo($cidade, $tipo, $itens_por_pagina){
        return $this->query(
"
select count(*)/".$itens_por_pagina." as qtd from (
    SELECT
        i.*
    FROM
        tb_imoveis as i
    WHERE
        i.f_cidade = '".$cidade."'
        and i.f_tipo like '".$tipo."%'
)as i2;
"
        );
    }
    
    function getByCidade($cidade, $limit, $offset){
        return $this->query(
"
    SELECT
        i.*
        , (
            select 
                f_id_foto
            from
                tb_imoveis_fotos
            where
                f_id = i.f_id
                and f_filial = i.f_filial
            order by
                f_destaque DESC, ordem ASC
            limit
                1
        ) as imagem_id
        , (
            exists (select * from imoveis_de_luxo where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_luxo
        , (
            exists (select * from imoveis_da_construtora where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_construtora
        , (
            exists (select * from ofertas where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_oferta
    FROM
        tb_imoveis as i
    WHERE
        i.f_cidade = '".$cidade."'
    ORDER BY
        i.f_valor
     LIMIT
        ".$limit."
     OFFSET
        ".$offset."
"
        );
    }
    
    function getQtdByCidade($cidade, $itens_por_pagina){
        return $this->query(
"
select count(*)/".$itens_por_pagina." as qtd from (
    SELECT
        i.*
    FROM
        tb_imoveis as i
    WHERE
        i.f_cidade = '".$cidade."'
)as i2;
"
        );
    }
    
    function getAll3($limit, $offset){
        return $this->query(
"
    SELECT
        i.*
        , (
            select 
                f_id_foto
            from
                tb_imoveis_fotos
            where
                f_id = i.f_id
                and f_filial = i.f_filial
            order by
                f_destaque DESC, ordem ASC
            limit
                1
        ) as imagem_id
        , (
            exists (select * from imoveis_de_luxo where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_luxo
        , (
            exists (select * from imoveis_da_construtora where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_construtora
        , (
            exists (select * from ofertas where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_oferta
    FROM
        tb_imoveis as i
    ORDER BY
        i.f_valor
    LIMIT
        ".$limit."
    OFFSET
        ".$offset."
"
        );
    }
    
    function getQtdAll3($itens_por_pagina){
        return $this->query(
"
select count(*)/".$itens_por_pagina." as qtd from (
    SELECT
        i.*
    FROM
        tb_imoveis as i
)as i2;
"
        );
    }
    
    function getLancamentos($limit, $offset){
        return $this->query(
"
    SELECT
        i.*
        , (
            select 
                f_id_foto
            from
                tb_imoveis_fotos
            where
                f_id = i.f_id
                and f_filial = i.f_filial
            order by
                f_destaque DESC, ordem ASC
            limit
                1
        ) as imagem_id
        , (
            exists (select * from imoveis_de_luxo where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_luxo
        , (
            exists (select * from imoveis_da_construtora where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_construtora
        , (
            exists (select * from ofertas where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_oferta
    FROM
        tb_imoveis as i
    WHERE
        i.f_lancamento = 1
    ORDER BY
        i.f_valor
     LIMIT
        ".$limit."
     OFFSET
        ".$offset."
"
        );
    }
    
    function getQtdLancamentos($itens_por_pagina){
        return $this->query(
"
select count(*)/".$itens_por_pagina." as qtd from (
    SELECT
        i.*
    FROM
        tb_imoveis as i
    WHERE
        i.f_lancamento = 1
) as i2;
"
        );
    }
    
    function getObrasConcluidas($limit, $offset){
        return $this->query(
"
    SELECT
        i.*
        , (
            select 
                f_id_foto
            from
                tb_imoveis_fotos
            where
                f_id = i.f_id
                and f_filial = i.f_filial
            order by
                f_destaque DESC, ordem ASC
            limit
                1
        ) as imagem_id
        , (
            exists (select * from imoveis_de_luxo where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_luxo
        , (
            exists (select * from imoveis_da_construtora where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_construtora
        , (
            exists (select * from ofertas where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_oferta
    FROM
        tb_imoveis as i
    WHERE
        exists (select * from imoveis_da_construtora where imovel_id = i.f_id and filial = i.f_filial)
    ORDER BY
        i.f_valor
     LIMIT
        ".$limit."
     OFFSET
        ".$offset."
"
        );
    }
    
    function getQtdObrasConcluidas($itens_por_pagina){
        return $this->query(
"
select count(*)/".$itens_por_pagina." as qtd from (
    SELECT
        i.*
    FROM
        tb_imoveis as i
    WHERE
        exists (select * from imoveis_da_construtora where imovel_id = i.f_id and filial = i.f_filial)
        
) as i2;
"
        );
    }
    
    function getMaisImoveis4($codigo){
        $consulta = 
"
    SELECT
        i.*
        , (
            select 
                f_id_foto
            from
                tb_imoveis_fotos
            where
                f_id = i.f_id
                and f_filial = i.f_filial
            order by
                f_destaque DESC, ordem ASC
            limit
                1
        ) as imagem_id
        , (
            exists (select * from imoveis_de_luxo where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_luxo
        , (
            exists (select * from imoveis_da_construtora where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_construtora
        , (
            exists (select * from ofertas where imovel_id = i.f_id and filial = i.f_filial)
        )as eh_oferta
    FROM
        tb_imoveis as i
    WHERE
        i.f_codigo = '".$codigo."'
";
        
        return $this->query($consulta);
        
    }
    
}
?>