<?php
class Condominio extends AppModel {
    var $name = 'Condominio';

    function getAll(){
        return $this->query(
"
    select
        *
    from
        condominios;
"
        );
    }
    
    function insert($cidade_id, $titulo, $descricao, $imoveis, $lancamento, $destaque){
        return $this->query(
"
    insert into
        condominios(
            cidade_id
            , titulo
            , descricao
            , imoveis
            , lancamento
            , destaque
        )values(
            '".$cidade_id."'
            , '".$titulo."'
            , '".$descricao."'
            , '".$imoveis."'
            , ".$lancamento."
            , ".$destaque."
        );
"
        );
    }
    
    function getLastInsertId(){
        return $this->query(
"
    select last_insert_id() as id;
"
        );
    }
    
    function update($id, $titulo, $descricao, $imoveis, $lancamento, $destaque){
        return $this->query(
"
    update
        condominios set
            titulo = '".$titulo."'
            , descricao = '".$descricao."'
            , imoveis = '".$imoveis."'
            , lancamento = ".$lancamento."
            , destaque = ".$destaque."
        where
            id = '".$id."';
"
        );
    }
    
    function get($id){
        return $this->query(
"
    select
        *
    from
        condominios
        , cidades
    where
        condominios.id = '".$id."'
        and condominios.cidade_id = cidades.id;
"
        );
    }
    
    function delete($id){
        return $this->query(
"
    delete from condominios where id = '".$id."';
"
        );
    }
    
    function getFotos($id){
        return $this->query(
"
    select * from fotos_condominios where condominio_id= '".$id."' order by destaque DESC, ordem ASC;
"
        );
    }
    
    function insertFoto($condominio_id, $legenda, $destaque, $ordem){
        return $this->query(
"
    insert into
        fotos_condominios(
            condominio_id
            , legenda
            , destaque
            , ordem
        )values(
            '".$condominio_id."'
            , '".$legenda."'
            , ".$destaque."
            , '".$ordem."'
        );
"
        );
    }
    
    function deleteFoto($id){
        return $this->query(
"
    delete from fotos_condominios where id = '".$id."';
"
        );
    }
    
    function desmarcaDestaque($condominio_id){
        return $this->query(
"
    update
        fotos_condominios set
            destaque = 0
        where
            condominio_id = '".$condominio_id."';
"
        );
    }
    
    function getFoto($id){
        return $this->query(
"
    select
        *
    from
        fotos_condominios
    where
        id = '".$id."';
"
        );
    }
    
    function updateFoto($id, $legenda, $ordem, $destaque){
        return $this->query(
"
    update
        fotos_condominios set
            legenda = '".$legenda."'
            , ordem = '".$ordem."'
            , destaque = ".$destaque."
        where
            id = '".$id."';
"
        );
    }
    
}
?>