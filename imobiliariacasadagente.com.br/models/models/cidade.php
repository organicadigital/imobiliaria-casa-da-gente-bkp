<?php
class Cidade extends AppModel {
    var $name = 'Cidade';

    function get($id){
        return $this->query(
"
    select
        *
    from
        cidades
    where
        id = '".$id."';
"
        );
    }
    
    function getAll(){
        return $this->query(
"
    select
        *
    from
        cidades;
"
        );
    }

    function getAll2(){
        return $this->query(
"
    SELECT f_cidade as nome FROM imobiliariacas6.tb_imoveis group by f_cidade;
"
        );
    }
    
    function getBairros($cidade){
        return $this->query(
"
    select
        f_bairro
    from
        tb_imoveis
    where
        f_cidade = '".$cidade."'
    group by
        f_bairro
    order by
        f_bairro;
"
        );
    }
    
    function getTipos($cidade){
        return $this->query(
"
    select
        retorna_tipos(f_tipo) as f_tipos
    from
        tb_imoveis
    where
        f_cidade = '".$cidade."'
    group by
        f_tipos
    order by
        f_tipos;
"
        );
    }
    
}
?>