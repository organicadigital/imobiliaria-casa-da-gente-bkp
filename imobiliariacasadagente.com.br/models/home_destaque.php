<?php
class HomeDestaque extends AppModel {
    var $name = 'HomeDestaque';

    function getAll(){
        
        //consultas ao banco
        $resultado = $this->query(
"
    select
        tb_imoveis.*
        , (
            select
                f_id_foto
            from
                tb_imoveis_fotos
            where
                tb_imoveis_fotos.f_id = tb_imoveis.f_id
                and tb_imoveis_fotos.f_filial = tb_imoveis.f_filial
            order by
                tb_imoveis_fotos.ordem
            limit 1
        )as imagem_id
    from
        tb_imoveis
    order by
        tb_imoveis.f_valor;
"
        );
        $resultado2 = $this->query(
"
    select
        c.id as id
        , c.cidade_id as cidade_id
        , c.titulo as titulo
        , c.descricao as descricao
        , c.lancamento as lancamento
        , c.destaque as destaque
        , ci.nome as cidade
        , (
            select
                fc.id
            from
                fotos_condominios as fc
            where
                fc.condominio_id = c.id
            order by
                fc.destaque
            limit
                1
        ) as imagem_id
    from
        condominios as c
        , cidades as ci
    where
        c.cidade_id = ci.id
    order by
        c.id;
"
        );
        $resultado3 = $this->query(
"
    select
        *
    from
        home_destaques;
"
        );
        $resultado_ofertas = $this->query(
"
    select
        *
    from
        ofertas;
"
        );
        $ofertas = array();
        foreach($resultado_ofertas as $r){
            $ofertas[] = $r["ofertas"]["id"];
        }
        $resultado_construtora = $this->query(
"
    select
        *
    from
        imoveis_da_construtora;
"
        );
        $construtora = array();
        foreach($resultado_construtora as $r){
            $construtora[] = $r["imoveis_da_construtora"]["id"];
        }
        $resultado_luxo = $this->query(
"
    select
        *
    from
        imoveis_de_luxo;
"
        );
        $luxo = array();
        foreach($resultado_luxo as $r){
            $luxo[] = $r["imoveis_de_luxo"]["id"];
        }
        
        //montagem do array destaques
        $destaques = array();
        $i=-1;
        foreach($resultado as $r){
            
            $i++;
            $destaques[$i]["id"] = $r["tb_imoveis"]["f_id"];
            $destaques[$i]["filial"] = $r["tb_imoveis"]["f_filial"];
            $destaques[$i]["eh_condominio"] = false;
            $destaques[$i]["eh_lancamento"] = $r["tb_imoveis"]["f_lancamento"];
            $destaques[$i]["eh_oferta"] = in_array($r["tb_imoveis"]["f_id"], $ofertas);
            $destaques[$i]["eh_construtora"] = in_array($r["tb_imoveis"]["f_id"], $construtora);
            $destaques[$i]["eh_luxo"] = in_array($r["tb_imoveis"]["f_id"], $luxo);
            $destaques[$i]["imagem_id"] = $r[0]["imagem_id"];
            $informacoes = explode("!", $r["tb_imoveis"]["f_descricao"], 2);
            $destaques[$i]["titulo"] = trim(utf8_encode($informacoes[0]));
            $destaques[$i]["descricao"] = trim(utf8_encode($informacoes[1]));
            $destaques[$i]["cidade"] = trim(utf8_encode($r["tb_imoveis"]["f_cidade"]));
            $eh_destaque = false;
            foreach($resultado3 as $r2){
                
                if(
                        $r2["home_destaques"]["identificador"]==$r["tb_imoveis"]["f_id"]
                        && $r2["home_destaques"]["eh_condominio"]==false
                ){
                    $eh_destaque = true;
                }
                
            }
            $destaques[$i]["eh_destaque"] = $eh_destaque;
            
        }
        foreach($resultado2 as $r){
            
            $i++;
            $destaques[$i]["id"] = $r["c"]["id"];
            $destaques[$i]["filial"] = "0";
            $destaques[$i]["eh_condominio"] = true;
            $destaques[$i]["eh_lancamento"] = false;
            $destaques[$i]["eh_oferta"] = false;
            $destaques[$i]["eh_construtora"] = false;
            $destaques[$i]["eh_luxo"] = false;
            $destaques[$i]["imagem_id"] = $r[0]["imagem_id"];
            $destaques[$i]["titulo"] = $r["c"]["titulo"];
            $destaques[$i]["descricao"] = $r["c"]["descricao"];
            $destaques[$i]["cidade"] = $r["ci"]["cidade"];
            $eh_destaque = false;
            foreach($resultado3 as $r2){
                
                if(
                        $r2["home_destaques"]["identificador"]==$r["c"]["id"]
                        && $r2["home_destaques"]["eh_condominio"]==true
                ){
                    $eh_destaque = true;
                }
                
            }
            $destaques[$i]["eh_destaque"] = $eh_destaque;
            
        }
            
        return $destaques;
        
    }
    
    function insert($identificador, $eh_condominio, $filial){
        return $this->query(
"
    insert
        into
            home_destaques(
                identificador
                , eh_condominio
                , filial
            )values(
                ".$identificador."
                , ".$eh_condominio."
                , '".$filial."'
            );
"
        );
    }
    
    function delete($identificador, $eh_condominio, $filial){
        return $this->query(
"
    delete
        from
            home_destaques
        where
            identificador = ".$identificador."
            and eh_condominio = ".$eh_condominio."
            and filial = '".$filial."';
        
"
        );
    }

    function deleteAll(){
        return $this->query(
"
    delete from home_destaques;
"
        );
    }
    
    function getAll2(){
        
        
        //consultas ao banco
        $resultado = $this->query(
"
    select
        tb_imoveis.*
        , (
            select
                f_id_foto
            from
                tb_imoveis_fotos
            where
                tb_imoveis_fotos.f_id = tb_imoveis.f_id
                and tb_imoveis_fotos.f_filial = tb_imoveis.f_filial
            order by
                tb_imoveis_fotos.ordem
            limit 1
        )as imagem_id
    from
        tb_imoveis
        , home_destaques
    where
        home_destaques.identificador = tb_imoveis.f_id
        and home_destaques.eh_condominio = 0
    order by
        tb_imoveis.f_id;
"
        );
        $resultado2 = $this->query(
"
    select
        c.id as id
        , c.cidade_id as cidade_id
        , c.titulo as titulo
        , c.descricao as descricao
        , c.lancamento as lancamento
        , c.destaque as destaque
        , ci.nome as cidade
        , (
            select
                fc.id
            from
                fotos_condominios as fc
            where
                fc.condominio_id = c.id
            order by
                fc.destaque
            limit
                1
        ) as imagem_id
    from
        condominios as c
        , cidades as ci
        , home_destaques
    where
        c.cidade_id = ci.id
        and home_destaques.identificador = c.id
        and home_destaques.eh_condominio = 1
    order by
        c.id;
"
        );
        $resultado_ofertas = $this->query(
"
    select
        *
    from
        ofertas;
"
        );
        $ofertas = array();
        foreach($resultado_ofertas as $r){
            $ofertas[] = array($r["ofertas"]["imovel_id"], $r["ofertas"]["filial"]);
        }
        $resultado_construtora = $this->query(
"
    select
        *
    from
        imoveis_da_construtora;
"
        );
        $construtora = array();
        foreach($resultado_construtora as $r){
            $construtora[] = array($r["imoveis_da_construtora"]["imovel_id"], $r["imoveis_da_construtora"]["filial"]);
        }
        $resultado_luxo = $this->query(
"
    select
        *
    from
        imoveis_de_luxo;
"
        );
        $luxo = array();
        foreach($resultado_luxo as $r){
            $luxo[] = array($r["imoveis_de_luxo"]["imovel_id"], $r["imoveis_de_luxo"]["filial"]);
        }
        
        //montagem do array destaques
        $destaques = array();
        $i=-1;
        foreach($resultado as $r){
            
            $i++;
            $destaques[$i]["id"] = $r["tb_imoveis"]["f_id"];
            $destaques[$i]["filial"] = $r["tb_imoveis"]["f_filial"];
            $destaques[$i]["eh_condominio"] = "0";
            $destaques[$i]["eh_lancamento"] = $r["tb_imoveis"]["f_lancamento"];
            $destaques[$i]["eh_oferta"] = in_array(array($r["tb_imoveis"]["f_id"], $r["tb_imoveis"]["f_filial"]), $ofertas);
            $destaques[$i]["eh_construtora"] = in_array(array($r["tb_imoveis"]["f_id"], $r["tb_imoveis"]["f_filial"]), $construtora);
            $destaques[$i]["eh_luxo"] = in_array(array($r["tb_imoveis"]["f_id"], $r["tb_imoveis"]["f_filial"]), $luxo);
            $destaques[$i]["imagem_id"] = $r[0]["imagem_id"];
            $informacoes = explode("!", $r["tb_imoveis"]["f_descricao"], 2);
            $destaques[$i]["titulo"] = trim(utf8_encode($informacoes[0]));
            $destaques[$i]["url_formatada"] = str_replace(array("/", " ", '"', ","), array("-", "-", "", ""), trim(utf8_encode($informacoes[0])));
            $destaques[$i]["descricao"] = trim(utf8_encode($informacoes[1]));
            $destaques[$i]["cidade"] = trim(utf8_encode($r["tb_imoveis"]["f_cidade"]));
            $valor="Consultar";
            if($r["tb_imoveis"]["f_tipo"] != ""){
                $valor = number_format($r["tb_imoveis"]["f_valor"],2,",",".");
            }
            $destaques[$i]["valor_imovel"] = $valor;
            
        }
        foreach($resultado2 as $r){
            
            $i++;
            $destaques[$i]["id"] = $r["c"]["id"];
            $destaques[$i]["filial"] = "0";
            $destaques[$i]["eh_condominio"] = "1";
            $destaques[$i]["eh_lancamento"] = false;
            $destaques[$i]["eh_oferta"] = false;
            $destaques[$i]["eh_construtora"] = false;
            $destaques[$i]["eh_luxo"] = false;
            $destaques[$i]["imagem_id"] = $r[0]["imagem_id"];
            $destaques[$i]["titulo"] = $r["c"]["titulo"];
            $destaques[$i]["url_formatada"] = str_replace(array("/", " "), array("-", "-"), $r["c"]["titulo"]);
            $destaques[$i]["descricao"] = $r["c"]["descricao"];
            $destaques[$i]["cidade"] = $r["ci"]["cidade"];
            $destaques[$i]["valor_imovel"] = "Consultar";
            
        }
            
        return $destaques;
        
    }
    
}
?>