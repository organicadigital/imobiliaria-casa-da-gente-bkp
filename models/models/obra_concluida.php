<?php
class ObraConcluida extends AppModel {
    var $name = 'ObraConcluida';
    var $useTable = "obras_concluidas";

    function getAll(){
        return $this->query(
"
    select
        *
    from
        obras_concluidas;
"
        );
    }
    
    function insert($cidade_id, $titulo, $descricao, $imoveis, $lancamento, $destaque){
        return $this->query(
"
    insert into
        obras_concluidas(
            cidade_id
            , titulo
            , descricao
            , imoveis
            , lancamento
            , destaque
        )values(
            '".$cidade_id."'
            , '".$titulo."'
            , '".$descricao."'
            , '".$imoveis."'
            , ".$lancamento."
            , ".$destaque."
        );
"
        );
    }
    
    function getLastInsertId(){
        return $this->query(
"
    select last_insert_id() as id;
"
        );
    }
    
    function update($id, $titulo, $descricao, $imoveis, $lancamento, $destaque){
        return $this->query(
"
    update
        obras_concluidas set
            titulo = '".$titulo."'
            , descricao = '".$descricao."'
            , imoveis = '".$imoveis."'
            , lancamento = ".$lancamento."
            , destaque = ".$destaque."
        where
            id = '".$id."';
"
        );
    }
    
    function get($id){
        return $this->query(
"
    select
        *
    from
        obras_concluidas
        , cidades
    where
        obras_concluidas.id = '".$id."'
        and obras_concluidas.cidade_id = cidades.id;
"
        );
    }
    
    function delete($id){
        return $this->query(
"
    delete from obras_concluidas where id = '".$id."';
"
        );
    }
    
    function getFotos($id){
        return $this->query(
"
    select * from fotos_obras_concluidas where obra_concluida_id= '".$id."' order by destaque DESC, ordem ASC;
"
        );
    }
    
    function insertFoto($obra_concluida_id, $legenda, $destaque, $ordem){
        return $this->query(
"
    insert into
        fotos_obras_concluidas(
            obra_concluida_id
            , legenda
            , destaque
            , ordem
        )values(
            '".$obra_concluida_id."'
            , '".$legenda."'
            , ".$destaque."
            , '".$ordem."'
        );
"
        );
    }
    
    function deleteFoto($id){
        return $this->query(
"
    delete from fotos_obras_concluidas where id = '".$id."';
"
        );
    }
    
    function desmarcaDestaque($obra_concluida_id){
        return $this->query(
"
    update
        fotos_obras_concluidas set
            destaque = 0
        where
            condominio_id = '".$obra_concluida_id."';
"
        );
    }
    
    function getFoto($id){
        return $this->query(
"
    select
        *
    from
        fotos_obras_concluidas
    where
        id = '".$id."';
"
        );
    }
    
    function updateFoto($id, $legenda, $ordem, $destaque){
        return $this->query(
"
    update
        fotos_obras_concluidas set
            legenda = '".$legenda."'
            , ordem = '".$ordem."'
            , destaque = ".$destaque."
        where
            id = '".$id."';
"
        );
    }
    
}
?>