<?php
class Banner extends AppModel {
    var $name = 'Banner';

    function getAll(){
        return $this->query(
"
    select
        *
    from
        banners
    order by
        ordem
"
        );
    }
    
    function get($id){
        return $this->query(
"
    select
        *
    from
        banners
    where
        id = '".$id."';
"
        );
    }
    
    function insert($linha1, $linha2, $link, $ordem){
        return $this->query(
"
    insert into
        banners(
            linha1
            , linha2
            , link
            , ordem
        )values(
            '".$linha1."'
            , '".$linha2."'
            , '".$link."'
            , '".$ordem."'
        );
"
        );
    }
    
    function update($id, $linha1, $linha2, $link, $ordem){
        return $this->query(
"
    update
        banners set
            linha1 = '".$linha1."'
            , linha2 = '".$linha2."'
            , link = '".$link."'
            , ordem = '".$ordem."'
        where
            id = '".$id."';
"
        );
    }
    
    function delete($id){
        return $this->query(
"
    delete from banners where id = '".$id."';
"
        );
    }
    
}
?>