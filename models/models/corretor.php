<?php
class Corretor extends AppModel {
    var $name = 'Corretor';

    function getAll(){
        return $this->query(
"
    select
        *
    from
        corretores
    order by
        filial, texto
"
        );
    }

    function get($id){
        return $this->query(
"
    select
        *
    from
        corretores
    where
        corretores.id = '".$id."';
"
        );
    }

    function getAllByFilial($filial){
        return $this->query(
"
    select
        *
    from
        corretores
    where
        corretores.filial = '".$filial."'
    order by
        texto;
"
        );
    }
    
    function insert($filial, $texto){
        return $this->query(
"
    insert into
        corretores(
            filial
            , texto
        )values(
            ".$filial."
            , '".$texto."'
        );
"
        );
    }
    
    function update($id, $filial, $texto){
        return $this->query(
"
    update
        corretores set
            filial = ".$filial."
            , texto = '".$texto."'
        where
            id = '".$id."';
"
        );
    }
    
    function delete($id){
        return $this->query(
"
    delete from corretores where id = '".$id."';
"
        );
    }
    
}
?>