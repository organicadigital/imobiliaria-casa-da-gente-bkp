<?php
class Cidade extends AppModel {
    var $name = 'Cidade';

    function get($id){
        return $this->query(
"
    select
        *
    from
        cidades
    where
        id = '".$id."';
"
        );
    }
    
    function getAll(){
        return $this->query(
"
    select
        *
    from
        cidades;
"
        );
    }
    
    function getCidadePorNome($nome){
        return $this->query(
"
    select
        *
    from
        cidades
    where
        nome = '".$nome."';
"
        );
    }
    
    function insert($nome, $email, $litoral){
        return $this->query(
"
	insert into
        cidades(nome, emails, litoral)
    values
        ('".$nome."', '".$email."', ".$litoral.");
"
        );
    }
    
    function update($id, $nome, $emails, $litoral){
        return $this->query(
"
	update
        cidades
    set
    	nome = '".$nome."'
    	, emails = '".$emails."'
    	, litoral = ".$litoral."
    where
        id = ".$id.";
"
        );
    }
    
    function setLitoral($id, $litoral){
        return $this->query(
"
	update
        cidades
    set
    	litoral = ".$litoral."
    where
        id = ".$id.";
"
        );
    }

    function getAll2(){
        return $this->query(
"
    SELECT f_cidade as nome FROM imobiliariacas6.tb_imoveis group by f_cidade;
"
        );
    }
    
    function getBairros($cidade){
        return $this->query(
"
    select
        f_bairro
    from
        tb_imoveis
    where
        f_cidade = '".$cidade."'
    group by
        f_bairro
    order by
        f_bairro;
"
        );
    }
    
    function getTiposOld($cidade){
        return $this->query(
"
    select
        retorna_tipos(f_tipo) as f_tipos
    from
        tb_imoveis
    where
        f_cidade = '".$cidade."'
    group by
        f_tipos
    order by
        f_tipos;
"
        );
    }

    function getTipos(){
      #Array final, temporário e retorno da busca no banco
      $tipos = array();
      $tipos_temp = array();
      $consulta =  $this->query(
        "
          select distinct f_tipo
            from tb_imoveis
            order by f_tipo;
        "
      );
      //Procura 'X Dorm' e substitui por ''
      foreach($consulta as $t){
        $pattern = '/\s\d+\s[A-Z][a-z]+/';
        $replacement = ' ';
        $string = $t["tb_imoveis"]["f_tipo"];
        $string = preg_replace($pattern, $replacement, $string);
        preg_replace($pattern, $replacement, $string);
        $tipos_temp[] = $string;
      }
      //Remove extra space created on replace
      foreach($tipos_temp as $t){
          $tipos[] = trim($t);
      }
      #Return 
      return array_unique($tipos);
    }
  
	function delete($id){
        return $this->query(
"
    delete from cidades where id = ".$id.";
"
        );
    }
    
    function canCidadeDeleted($idCidade){
        return $this->query(
"
    select
        *
	from
		cidades as ci
	where
		ci.id = ".$idCidade."
		and ( exists (
					select 1 from condominios co where co.cidade_id = ci.id
				)
				or exists (
					select 1 from corretores cor where cor.filial = ci.id
				)
				or exists (
					select 1 from enderecos en where en.cidade_id = ci.id
				)
				or exists (
					select 1 from usuarios us where us.cidade_id = ci.id
				)
		)  
"
        );
    }

}
?>