 function crossJoin($imoveis, $fotos) {
        #Create final return array
        $imoveis_fotos = array();
        #Para cada imóvel
        foreach($imoveis as $im){
            #Array de fotos temporárias que tem o mesmo f_id que o imovel em questao
            $fotos_temp = array();
            #Para cada foto
            foreach($fotos as $f) {
                #Se o imovel e a foto tiverem o mesmo id, adiciona a foto em questão no array temporario
                if($im["tb_imoveis"]["f_id"] == $f["tb_imoveis_fotos"]["f_id"]){
                    $fotos_temp[] = $f
                }
            }
            #Para cada foto do imóvel
            foreach($fotos_temp as $foto){
                #Se a foto em questão for uma foto destaque, limpa o array temporário e coloca essa foto
                if($foto["tb_fotos_imoveis"]["f_destaque"] == 1){
                    $fotos_temp = []
                    $fotos_temp = $foto
                }
            }
            #Se nenhum foto tiver f_destaque setado (o que implica no tamanho dele ser maior que um), coloca a primeira foto
            # encontrada como item da variável $fotos_temp.
            if(sizeof($fotos_temp) > 1){
                $fotos_temp = $fotos_temp[0]
            }
            #Para cada imovel, adiciona um elemento no array que é o id, as informaçoes do imovel em questão e a foto.
            $imoveis_fotos[] = array(
                "id" => $im["tb_imoveis"]["f_id"],
                "info" => $im,
                "fotos" => $fotos_temp,
            );
            $fotos_temp = []
        }
        return $imoveis_fotos;
    }

function getFotos($f_id, $f_filial){
        return $this->query(
"
    select
        *
    from
        tb_imoveis_fotos
    where
        f_id = ".$f_id."
        and f_filial = '".$f_filial."';
    order by
        f_destaque DESC, ordem ASC;
"
        );
    }
