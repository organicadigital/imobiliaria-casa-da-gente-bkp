$(document).ready(function(){

    $('a[rel="external"]').attr('target','_blank');
    
    //busca
    $(".busca select").each(function(){
        
        $(this).parent().find("label").click(function(){
            $(this).parent().find("select").slideDown();
        });
        $(this).blur(function(){
            
            var valor = $(this).val();
            var html = $(this).find("option[value='"+valor+"']").html();
            $(this).parent().find("label").html(html);
            $(this).slideUp();
            
        });
        $(this).change(function(){
            
            var valor = $(this).val();
            var html = $(this).find("option[value='"+valor+"']").html();
            $(this).parent().find("label").html(html);
            $(this).slideUp();
            
        });
        
    });
    $("#codigo_busca").focus(function(){
        if($(this).val() == "código"){
            $(this).val("");
        }
    });
    $("#codigo_busca").blur(function(){
        if($(this).val() == ""){
            $(this).val("código");
        }
    });
    $("#cidade_busca").change(function(){
        if($(this).val()=="cidade"){
            $("label[for=bairro_busca]").html("selecione uma cidade");
            $("#bairro_busca").html("<option value='bairro' selected='selected'>selecione uma cidade</option>");
        }else{
            $("label[for=bairro_busca]").html("carregando");
            $.ajax({
                url: urlRoot+"busca/getBairros/"+$(this).val()
                , type: "POST"
                , success: function(data){
                    $("label[for=bairro_busca]").html("bairro");
                    $("#bairro_busca").html(data);
                }
            });
        }
    });
    
    //menu
    //somente o primeiro li do ul principal tem submenu
    var tempo;
    function inicia_fechamento(){
        tempo = setTimeout(function(){
            $("#menu_principal>ul>li.aberto .submenu").slideUp();
            $("#menu_principal>ul>li").removeClass("aberto");
        }, 2000); 
    }
    $("#menu_principal>ul>li").mouseover(function(){
        clearTimeout(tempo);
        if(!$(this).hasClass("aberto")){
            $("#menu_principal>ul>li.aberto .submenu").slideUp();
            $("#menu_principal>ul>li").removeClass("aberto");
            $(this).addClass("aberto");
            $(this).children(".submenu").slideDown();
            inicia_fechamento();
        }
    });
    $("#menu_principal>ul>li>.submenu>ul>li").mouseover(function(){
        clearTimeout(tempo);
        $("#menu_principal .submenu2.aberto").hide();
        $("#menu_principal .submenu2.aberto").removeClass("aberto");
        $(this).find(".submenu2").addClass("aberto");
        $(this).find(".submenu2.aberto").show();
    });
    $("#menu_principal>ul>li>.submenu>ul>li>.submenu2>ul>li").mouseover(function(){
        clearTimeout(tempo);
        $("#menu_principal .submenu3.aberto").hide();
        $("#menu_principal .submenu3.aberto").removeClass("aberto");
        $(this).find(".submenu3").addClass("aberto");
        $(this).find(".submenu3.aberto").show();
    });
    $("#menu_principal>ul>li>.submenu>ul>li").mouseout(function(){
        inicia_fechamento();
    });
    
    //imoveis
    var i = 0;
    var animacoes = new Array();
    $(".imovel .imagem").each(function(){
        
        animacoes[i] = null;
        $(this).attr("animacao1", i);
        i++;
        animacoes[i] = null;
        $(this).attr("animacao2", i);
        i++;
        
    });
    $(".imovel .imagem").hover(
        function(){
            
            var objeto = $(this);
            clearTimeout(animacoes[objeto.attr("animacao2")]);
            animacoes[objeto.attr("animacao1")] = setTimeout(function(){
                $(objeto).parent().find(".informacoes").animate({
                    marginLeft: "300px"
                    , width: "hide"
                }, 500);
            }, 500);
            
        }, function(){
            
            var objeto = $(this);
            clearTimeout(animacoes[objeto.attr("animacao1")]);
            animacoes[objeto.attr("animacao2")] = setTimeout(function(){
                $(objeto).parent().find(".informacoes").animate({
                    marginLeft: "129px"
                    , width: "show"
                }, 500);
            }, 500);
            
        }
    );
        
    //paginacao
    
    
});
function vazio(){}
function processaBuscaInterna(offset){
    
    urlBusca = urlRoot+"busca/index/";
    urlBusca += $("#tipo_busca").val()+"/";
    urlBusca += $("#codigo_busca").val()+"/";
    urlBusca += $("#cidade_busca").val()+"/";
    urlBusca += $("#bairro_busca").val()+"/";
    urlBusca += $("#dormitorios_busca").val()+"/";
    urlBusca += $("#garagem_busca").val()+"/";
    urlBusca += $("#valor_de_busca").val()+"/";
    urlBusca += $("#valor_ate_busca").val()+"/";
    urlBusca += $("#busca_imovel input:radio:checked").val()+"/";
    urlBusca += offset;
    window.location = urlBusca;
    
}