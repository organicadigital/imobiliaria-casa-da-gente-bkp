function envia_nos_ligamos(){
    
    $.ajax({
        url: urlRoot+"nosLigamos/processaEnviaNosLigamos"
        , type: "POST"
        , data: {
            'data[Contato][nome]': $('#nome_text').val()
            , 'data[Contato][email]': $('#email_text').val()
            , 'data[Contato][telefone]': $('#telefone_text').val()
            , 'data[Contato][mensagem]': $('#mensagem_textarea').val()
            , 'data[Contato][imovel]': $('#imovel_hidden').val()
        }
        , success: function(data){
            alert(data);
        }
    });
    
}