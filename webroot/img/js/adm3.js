/*atualizar fotos*/
function atualizar_fotos(){
    
    $.ajax({
        url: urlRoot+"adm/getFotosAtualizar"
        , type: "POST"
        , success: function(data){
            $('#lad').html(data);
        }
    });
    
}

function atualizar_foto(foto_id, filial){
    
    $.ajax({
        url: urlRoot+"adm/atualizarFoto/"+foto_id+"/"+filial
        , type: "POST"
        , success: function(data){
            $('#lad').html(data);
        }
    });
    
}

function atualizar_senha(){
    
    $.ajax({
        url: urlRoot+"adm/processaAtualizacaoSenha"
        , type: "POST"
        , data: {
            'data[Usuario][nova_senha]': $('#nova_senha').val()
            , 'data[Usuario][senha_atual]': $('#senha_atual').val()
            , 'data[Usuario][confirma_nova_senha]': $('#confirma_nova_senha').val()
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });
    
}

/*condominios*/
function condominios_cadastrar(){
    
    var lancamento = false;
    if($('#lancamento_checkbox').attr("checked")){
        lancamento = true;
    }
    var destaque = false;
    if($('#destaque_checkbox').attr("checked")){
        destaque = true;
    }
    $.ajax({
        url: urlRoot+"adm/processaCondominiosCadastrar"
        , type: "POST"
        , data: {
            'data[Condominio][cidade_id]': $('#cidade_combo').val()
            ,'data[Condominio][titulo]': $('#titulo_text').val()
            , 'data[Condominio][descricao]': $('#descricao_textarea').val()
            , 'data[Condominio][imoveis]': $('#imoveis_textarea').val()
            , 'data[Condominio][lancamento]': lancamento
            , 'data[Condominio][destaque]': destaque
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });
    
}

function condominios_cadastrar2(){
    
    var lancamento = false;
    if($('#lancamento_checkbox').attr("checked")){
        lancamento = true;
    }
    var destaque = false;
    if($('#destaque_checkbox').attr("checked")){
        destaque = true;
    }
    $.ajax({
        url: urlRoot+"adm/processaCondominiosCadastrar2"
        , type: "POST"
        , data: {
            'data[Condominio][cidade_id]': $('#cidade_combo').val()
            ,'data[Condominio][titulo]': $('#titulo_text').val()
            , 'data[Condominio][descricao]': $('#descricao_textarea').val()
            , 'data[Condominio][imoveis]': $('#imoveis_textarea').val()
            , 'data[Condominio][lancamento]': lancamento
            , 'data[Condominio][destaque]': destaque
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });

}

function condominios_editar(){
    
    var lancamento = false;
    if($('#lancamento_checkbox').attr("checked")){
        lancamento = true;
    }
    var destaque = false;
    if($('#destaque_checkbox').attr("checked")){
        destaque = true;
    }
    $.ajax({
        url: urlRoot+"adm/processaCondominiosEditar"
        , type: "POST"
        , data: {
            'data[Condominio][cidade_id]': $('#cidade_combo').val()
            , 'data[Condominio][titulo]': $('#titulo_text').val()
            , 'data[Condominio][descricao]': $('#descricao_textarea').val()
            , 'data[Condominio][imoveis]': $('#imoveis_textarea').val()
            , 'data[Condominio][id]': $('#condominio_id').val()
            , 'data[Condominio][lancamento]': lancamento
            , 'data[Condominio][destaque]': destaque
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });
    
}

function condominios_cadastrar_foto(){
    
    var destaque = false;
    if($('#destaque_checkbox').attr("checked")){
        destaque = true;
    }
    $.ajaxFileUpload({
        url: urlRoot+"adm/processaCondominiosEditarFotos"
        , secureuri: false
        , fileElementId: 'foto_file'
        , dataType: 'json'
        , data: {
            'data[Foto][legenda]': $("#legenda_text").val()
            , 'data[Foto][ordem]': $("#ordem_text").val()
            , 'data[Foto][destaque]': destaque
            , 'data[Foto][condominio_id]': $("#condominio_id").val()
        }
        , success: function(data, status){
            if(data=="ok"){
                alert('Foto Adicionada com Sucesso.');
                window.location.reload();
            }else{
                alert('Imagem inválida');
            }
            
        }
    });

}

function condominios_editar_foto(){
    
    var destaque = false;
    if($('#destaque_checkbox').attr("checked")){
        destaque = true;
    }
    $.ajax({
        url: urlRoot+"adm/processaCondominiosEditarFoto"
        , type: "POST"
        , data: {
            'data[Foto][legenda]': $('#legenda_text').val()
            , 'data[Foto][ordem]': $('#ordem_text').val()
            , 'data[Foto][id]': $('#foto_id').val()
            , 'data[Foto][condominio_id]': $('#condominio_id').val()
            , 'data[Foto][destaque]': destaque
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });

}

















/*obras_concluidas*/
function obras_concluidas_cadastrar(){

    var lancamento = false;
    if($('#lancamento_checkbox').attr("checked")){
        lancamento = true;
    }
    var destaque = false;
    if($('#destaque_checkbox').attr("checked")){
        destaque = true;
    }
    $.ajax({
        url: urlRoot+"adm/processaObrasConcluidasCadastrar"
        , type: "POST"
        , data: {
            'data[ObraConcluida][titulo]': $('#titulo_text').val()
            , 'data[ObraConcluida][descricao]': $('#descricao_textarea').val()
            , 'data[ObraConcluida][imoveis]': $('#imoveis_textarea').val()
            , 'data[ObraConcluida][lancamento]': lancamento
            , 'data[ObraConcluida][destaque]': destaque
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });

}

function obras_concluidas_cadastrar2(){

    var lancamento = false;
    if($('#lancamento_checkbox').attr("checked")){
        lancamento = true;
    }
    var destaque = false;
    if($('#destaque_checkbox').attr("checked")){
        destaque = true;
    }
    $.ajax({
        url: urlRoot+"adm/processaObrasConcluidasCadastrar2"
        , type: "POST"
        , data: {
            'data[ObraConcluida][titulo]': $('#titulo_text').val()
            , 'data[ObraConcluida][descricao]': $('#descricao_textarea').val()
            , 'data[ObraConcluida][imoveis]': $('#imoveis_textarea').val()
            , 'data[ObraConcluida][lancamento]': lancamento
            , 'data[ObraConcluida][destaque]': destaque
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });

}

function obras_concluidas_editar(){

    var lancamento = false;
    if($('#lancamento_checkbox').attr("checked")){
        lancamento = true;
    }
    var destaque = false;
    if($('#destaque_checkbox').attr("checked")){
        destaque = true;
    }
    $.ajax({
        url: urlRoot+"adm/processaObrasConcluidasEditar"
        , type: "POST"
        , data: {
            'data[ObraConcluida][titulo]': $('#titulo_text').val()
            , 'data[ObraConcluida][descricao]': $('#descricao_textarea').val()
            , 'data[ObraConcluida][imoveis]': $('#imoveis_textarea').val()
            , 'data[ObraConcluida][id]': $('#obra_concluida_id').val()
            , 'data[ObraConcluida][lancamento]': lancamento
            , 'data[ObraConcluida][destaque]': destaque
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });

}

function obras_concluidas_cadastrar_foto(){

    var destaque = false;
    if($('#destaque_checkbox').attr("checked")){
        destaque = true;
    }
    $.ajaxFileUpload({
        url: urlRoot+"adm/processaObrasConcluidasEditarFotos"
        , secureuri: false
        , fileElementId: 'foto_file'
        , dataType: 'json'
        , data: {
            'data[Foto][legenda]': $("#legenda_text").val()
            , 'data[Foto][ordem]': $("#ordem_text").val()
            , 'data[Foto][destaque]': destaque
            , 'data[Foto][obra_concluida_id]': $("#obra_concluida_id").val()
        }
        , success: function(data, status){

            if(data=="ok"){
                alert('Foto Adicionada com Sucesso.');
                window.location.reload();
            }else{
                alert('Imagem inválida');
            }

        }
    });

}

function obras_concluidas_editar_foto(){

    var destaque = false;
    if($('#destaque_checkbox').attr("checked")){
        destaque = true;
    }
    $.ajax({
        url: urlRoot+"adm/processaObrasConcluidasEditarFoto"
        , type: "POST"
        , data: {
            'data[Foto][legenda]': $('#legenda_text').val()
            , 'data[Foto][ordem]': $('#ordem_text').val()
            , 'data[Foto][id]': $('#foto_id').val()
            , 'data[Foto][obra_concluida_id]': $('#obra_concluida_id').val()
            , 'data[Foto][destaque]': destaque
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });

}


















/*home - destaques*/
function updateHomeDestaque(id, eh_condominio, eh_destaque, filial){
    
    $.ajax({
        url: urlRoot+"adm/processaHomeUpdateDestaque"
        , type: "POST"
        , data: {
            'data[Imovel][id]': id
            , 'data[Imovel][eh_condominio]': eh_condominio
            , 'data[Imovel][eh_destaque]': eh_destaque
            , 'data[Imovel][filial]': filial
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });

}
function removerTodosDestaques(){
    
    $.ajax({
        url: urlRoot+"adm/processaHomeRemoverDestaques"
        , type: "POST"
        , success: function(data){
            $('#lad').html(data);
        }
    });

}
/*home - banners*/
function home_banners_cadastrar(){
    
    $.ajaxFileUpload({
        url: urlRoot+"adm/processaHomeBannersCadastrar"
        , secureuri: false
        , fileElementId: 'foto_file'
        , dataType: 'json'
        , data: {
            'data[Banner][linha1]': $("#linha1_text").val()
            , 'data[Banner][linha2]': $("#linha2_text").val()
            , 'data[Banner][link]': $("#link_text").val()
            , 'data[Banner][ordem]': $("#ordem_text").val()
        }
        , success: function(data, status){
            
            if(data=="ok"){
                alert('Banner Adicionado com Sucesso.');
                window.location = urlRoot+"adm/homeBanners";
            }else{
                alert('Imagem inválida');
            }
            
        }
    });

}

function home_banners_editar(){
    
    $.ajax({
        url: urlRoot+"adm/processaHomeBannersEditar"
        , type: "POST"
        , data: {
            'data[Banner][id]': $('#banner_id').val()
            , 'data[Banner][linha1]': $('#linha1_text').val()
            , 'data[Banner][linha2]': $('#linha2_text').val()
            , 'data[Banner][link]': $('#link_text').val()
            , 'data[Banner][ordem]': $('#ordem_text').val()
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });

}

/*home - indicadores*/
function indicadores_atualizar(){
    
    $.ajax({
        url: urlRoot+"adm/processaHomeIndicadores"
        , type: "POST"
        , data: {
            'data[Indicador][linha1]': $('#linha1_text').val()
            , 'data[Indicador][linha2]': $('#linha2_text').val()
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });
    
}

/*Imóveis de luxo*/
function updateImoveisDeLuxo(id, eh_luxo, filial){
    
    $.ajax({
        url: urlRoot+"adm/processaImoveisDeLuxo"
        , type: "POST"
        , data: {
            'data[Imovel][id]': id
            , 'data[Imovel][eh_luxo]': eh_luxo
            , 'data[Imovel][filial]': filial
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });

}

/*Obras Concluídas*/
/*function updateObrasConcluidas(id, eh_construtora, filial){
    
    $.ajax({
        url: urlRoot+"adm/processaObrasConcluidas"
        , type: "POST"
        , data: {
            'data[Imovel][id]': id
            , 'data[Imovel][eh_construtora]': eh_construtora
            , 'data[Imovel][filial]': filial
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });

}*/

/*Ofertas*/
function updateOfertas(id, eh_oferta, filial){
    
    $.ajax({
        url: urlRoot+"adm/processaOfertas"
        , type: "POST"
        , data: {
            'data[Imovel][id]': id
            , 'data[Imovel][eh_oferta]': eh_oferta
            , 'data[Imovel][filial]': filial
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });

}

/*news*/
function criar_news(){
    var imoveis = "";
    $("input[type=checkbox]:checked").each(function(){
        imoveis += $("#id_"+$(this).val()).val();
        imoveis += "@@";
        imoveis += $("#titulo_"+$(this).val()).val();
        imoveis += "@@";
        imoveis += $("#descricao_"+$(this).val()).val();
        imoveis += "@@";
        imoveis += $("#cidade_"+$(this).val()).val();
        imoveis += "@@";
        imoveis += $("#url_"+$(this).val()).val();
        imoveis += "@@";
        imoveis += $("input[name=fotos_"+$(this).val()+"]:checked").val();
        imoveis += "@@";
        imoveis += $("#filial_"+$(this).val()).val();
        imoveis += "@@";
        imoveis += $("#ordem_"+$(this).val()).val();
        imoveis += "---";
    });
    imoveis = imoveis.substring(0, imoveis.length-3);
    $.ajax({
        url: urlRoot+"adm/processaNews"
        , type: "POST"
        , data: {
            'data[News][imoveis]': imoveis
            , 'data[News][email]': $("#email_text").val()
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });

}



/* Lojas */
function lojas_texto_atualizar(){
    
    $.ajax({
        url: urlRoot+"adm/processaLojasTextoPaginaEditar"
        , type: "POST"
        , data: {
            'data[Texto][texto]': $('#texto_loja_textarea').val()
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });
    
}

function lojasEnderecoCadastrar(){
    $.ajax({
        url: urlRoot+"adm/processaLojasEnderecosCadastrar"
        , type: "POST"
        , data: {
            'data[Endereco][cidade_id]': $('#cidade_combo').val()
            ,'data[Endereco][endereco]': $('#endereco_text').val()
            , 'data[Endereco][telefone]': $('#telefone_text').val()
            , 'data[Endereco][cep]': $('#cep_text').val()
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });
}

function lojasEnderecoEditar(){
    $.ajax({
        url: urlRoot+"adm/processaLojasEnderecosEditar"
        , type: "POST"
        , data: {
            'data[Endereco][id]': $('#endereco_id').val()
            ,'data[Endereco][cidade_id]': $('#cidade_combo').val()
            ,'data[Endereco][endereco]': $('#endereco_text').val()
            , 'data[Endereco][telefone]': $('#telefone_text').val()
            , 'data[Endereco][cep]': $('#cep_text').val()
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });
};

function confirmarExclusaoEndereco(id) {
    var resp = confirm("Confirma a exclusao do endereco?");
    if (resp == true) {
        $.ajax({
            url: urlRoot+"adm/lojasEnderecosExcluir"
            , type: "POST"
            , data: {
                'data[Endereco][id]': id
            }
            , success: function(data){
                $('#lad').html(data);
            }
        });
    }
};

function lojas_foto_editar(id) {
    var foto_file = "foto_file" + id;
    $.ajaxFileUpload({
        url: urlRoot + "adm/processaLojasFotosEditar"
        , secureuri: false
        , fileElementId: foto_file
        , dataType: 'json'
        , data: {
            'data[Foto][id]': id
        }
        , success: function(data, status) {
            if (data == "ok") {
                alert('Foto editada com Sucesso.');
                window.location = urlRoot + "adm/lojasFotos";
            } else {
                alert('Imagem inv�lida');
            }
        }
    });
}

function lojas_corretor_cadastrar(){
    
    $.ajax({
        url: urlRoot+"adm/processaLojasCorretoresCadastrar"
        , type: "POST"
        , data: {
            'data[Corretor][filial]': $('#filial_corretor_select').val()
            , 'data[Corretor][texto]': $('#texto_corretor_text').val()
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });
}

function lojas_corretor_editar(){
    
    $.ajax({
        url: urlRoot+"adm/processaLojasCorretoresEditar"
        , type: "POST"
        , data: {
            'data[Corretor][id]': $('#corretor_id').val()
            , 'data[Corretor][filial]': $('#filial_corretor_select').val()
            , 'data[Corretor][texto]': $('#texto_corretor_text').val()
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });
}

// CIDADES

function cidadesCadastrar(){
    $.ajax({
        url: urlRoot+"adm/processaCidadesCadastrar"
        , type: "POST"
        , data: {
            'data[Cidade][nome]': $('#nome_text').val()
            , 'data[Cidade][emails]': $('#emails_text').val()
            , 'data[Cidade][litoral]': $('#litoral_combo').val()
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });

}

function cidadesEditar(){
    $.ajax({
        url: urlRoot+"adm/processaCidadesEditar"
        , type: "POST"
        , data: {
            'data[Cidade][id]': $('#cidade_id').val()
            , 'data[Cidade][nome]': $('#nome_text').val()
            , 'data[Cidade][emails]': $('#emails_text').val()
            , 'data[Cidade][litoral]': $('#litoral_combo').val()
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });

}

function confirmarExclusaoCidade(id) {
    var resp = confirm("Confirma a exclusao da cidade?");
    if (resp == true) {
        $.ajax({
            url: urlRoot+"adm/cidadesExcluir"
            , type: "POST"
            , data: {
                'data[Cidade][id]': id
            }
            , success: function(data){
                $('#lad').html(data);
            }
        });
    }
};

function cidadeDoLitoral(id, litoral){
    $.ajax({
        url: urlRoot+"adm/processaCidadeLitoral"
        , type: "POST"
        , data: {
            'data[Cidade][id]': id
            , 'data[Cidade][litoral]': litoral
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });
}


// USU�RIOS

function usuariosCadastrar(){
    $.ajax({
        url: urlRoot+"adm/processaUsuariosCadastrar"
        , type: "POST"
        , data: {
            'data[Usuario][login]': $('#login_text').val()
            , 'data[Usuario][cidade]': $('#cidade_combo').val()
            , 'data[Usuario][permissao]': $('#permissao_combo').val()
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });

}

function usuariosEditar(){
    $.ajax({
        url: urlRoot+"adm/processaUsuariosEditar"
        , type: "POST"
        , data: {
            'data[Usuario][id]': $('#usuario_id').val()
            , 'data[Usuario][login]': $('#login_text').val()
            , 'data[Usuario][cidade]': $('#cidade_combo').val()
            , 'data[Usuario][permissao]': $('#permissao_combo').val()
        }
        , success: function(data){
            $('#lad').html(data);
        }
    });

}

function confirmarExclusaoUsuario(id) {
    var resp = confirm("Confirma a exclusao do usuario?");
    if (resp == true) {
        $.ajax({
            url: urlRoot+"adm/usuariosExcluir"
            , type: "POST"
            , data: {
                'data[Usuario][id]': id
            }
            , success: function(data){
                $('#lad').html(data);
            }
        });
    }
};

function changePermissao() {
    $("#permissao_combo").change(function(){
        var permissao = $(this).val();
        if (permissao === 'COMUM') {
            
        }
    });
};

$(document).ready(function(){

    $('#usuario_text').val("");
    $('#senha_password').val("");
    $("input[type=text][class!=edit][class!='numerico edit'], textarea[class!=edit]").val("");
    $("label[class!=reset]").click(function(){
        $(this).fadeOut();
    });
    $('input[type=text], input[type=password], textarea').focus(function(){
        $(this).parent().find("label").fadeOut();
    });
    $('input[type=text], input[type=password], textarea').blur(function(){
        if($(this).val() == ""){
            $(this).parent().find("label").fadeIn();
        }
    });
    
    //tipos de dados
    $(".numerico").numeric();
    
    $('#senha_atual').val("");
    $('#senha_text').val("");
    $('#nova_senha').val("");
    $('#confirma_nova_senha').val("");
});