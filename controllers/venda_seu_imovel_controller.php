<?php
class VendaSeuImovelController extends AppController {
    
    var $name = 'VendaSeuImovel';
    var $uses = array("Cidade", "Imovel", "Endereco");
    var $helpers = array("Text");
    var $components = array('Validador', 'Email');

    function beforeFilter() {
        $this->layout = "internas";
        $this->set("enderecos", $this->Endereco->getAll());
    }

    function index() {

        $this->set("titulo", "Venda seu Imóvel");
        $this->set("css", "venda");
        $this->set("javascript", "venda");
        //geral
        $cidades2 = $this->Cidade->getAll2();
        $this->set("cidades2", $cidades2);
        $cidades = $this->Cidade->getAll();
        $this->set("cidades", $cidades);
        $this->set("tipos", $this->Imovel->getTipos());
        $tipos_cidade = array();
        foreach($cidades2 as $c){
            $tipos = $this->Cidade->getTipos($c["tb_imoveis"]["nome"]);
            foreach($tipos as $t){
                $tipos_cidade[$c["tb_imoveis"]["nome"]][] = $t;
            }
        }
        $this->set("tipos_cidade", $tipos_cidade);
        
    }
    
    function processaEnviaVendaSeuImovel(){
        
        $this->layout = "ajax";
        $eh_valido = true;
        if($this->data['Venda']['nome'] ==""){
            $eh_valido = false;
            $erro="Nome é um campo obrigatório.";
        }
        if($this->data['Venda']['ddd'] ==""){
            $eh_valido = false;
            $erro="DDD é um campo obrigatório.";
        }
        if($this->data['Venda']['telefone'] ==""){
            $eh_valido = false;
            $erro="Telefone é um campo obrigatório.";
        }
        if(!$this->Validador->emailValido($this->data["Venda"]["email"])){
            $eh_valido = false;
            $erro="Preencha um e-mail válido.";
        }
        if($this->data['Venda']['tipo'] ==""){
            $eh_valido = false;
            $erro="Tipo é um campo obrigatório.";
        }
        if($this->data['Venda']['cidade'] =="cidade"){
            $eh_valido = false;
            $erro="Selecione uma cidade.";
        }
        if($eh_valido){
    
            $this->set("nome", $this->data['Venda']['nome']);
            $this->set("ddd", $this->data['Venda']['ddd']);
            $this->set("telefone", $this->data['Venda']['telefone']);
            $this->set("email", $this->data['Venda']['email']);
            $this->set("tipo", $this->data['Venda']['tipo']);
            $this->set("cidade", $this->data['Venda']['cidade']);
            
            $para = "gramado@imobiliariacasadagente.com.br";
            $this->Email->to = $para;
            $this->Email->subject = 'Venda seu Imóvel - Site';
            $this->Email->from = "site@imobiliariacasadagente.com.br";
            $this->Email->replyTo = $this->data['Venda']['email'];
            $this->Email->template = 'envia_venda';
            $this->Email->sendAs = 'html';
            $this->Email->smtpOptions = array(
                'port' => '587',
                'timeout' => '15',
                'host' => 'mail.imobiliariacasadagente.com.br',
                'username' => 'site@imobiliariacasadagente.com.br',
                'password' => 's1tecdg32955555'
            );
            $this->Email->delivery = 'smtp';
            $this->Email->send();

        }else{
            $this->set("erro", $erro);
        }
        $this->set("eh_valido", $eh_valido);
    }
}
?>