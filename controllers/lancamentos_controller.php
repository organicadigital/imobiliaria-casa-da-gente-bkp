<?php
App::import('Sanitize');
class LancamentosController extends AppController {
    
    var $name = 'Lancamentos';
    var $uses = array("Cidade", "Imovel", "Endereco");
    var $helpers = array("Text");

    function beforeFilter() {
        $this->layout = "internas";
        $this->set("enderecos", $this->Endereco->getAll());
    }

    function index($limit=9, $offset=0) {

        $limit = Sanitize::escape($limit);
        $offset = Sanitize::escape($offset);
        $this->set("limit", $limit);
        $this->set("offset", $offset);
        
        $this->set("titulo", "Lançamentos");
        //geral
        $cidades2 = $this->Cidade->getAll2();
        $this->set("cidades2", $cidades2);
        $cidades = $this->Cidade->getAll();
        $this->set("cidades", $cidades);
        $this->set("tipos", $this->Imovel->getTipos());
        $tipos_cidade = array();
        foreach($cidades2 as $c){
            $tipos = $this->Cidade->getTipos($c["tb_imoveis"]["nome"]);
            foreach($tipos as $t){
                $tipos_cidade[$c["tb_imoveis"]["nome"]][] = $t;
            }
        }
        $this->set("tipos_cidade", $tipos_cidade);
        
        //imoveis
        $imoveis = $this->Imovel->getLancamentos($limit, $offset);
        $paginas = $this->Imovel->getQtdLancamentos($limit);
        $this->set("imoveis", $imoveis);
        $this->set("totalPaginas", ceil($paginas[0][0]["qtd"]));
        $this->set("paginaAtual", ($offset/$limit)+1);
    }
    
}
?>