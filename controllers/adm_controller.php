<?php
class AdmController extends AppController {

    var $name = 'Adm';
    var $uses = array("Usuario", "Cidade", "Condominio", "Indicador", "Banner", "Imovel", "HomeDestaque", "ImoveisDeLuxo", "ObraConcluida", "Oferta", "Texto", "Corretor", "Endereco");
    var $components = array("Autenticador", "Validador", "GeradorImagem", "Email");
    var $helpers = array("Text");

    function beforeFilter() {
        $this->layout = "adm";
        $this->set("titulo", "AdministraÃ§Ã£o Casa da Gente");
    }

    //loggin
    function login() {
        $this->layout = "login";
    }

    function processaLogin() {

        $this->layout = "ajax";
        //entrada de dados
        $login = $this->data['Usuario']['usuario_text'];
        $senha = $this->data['Usuario']['senha_password'];

        //verifica se Ã© vÃ¡lido
        $logou = false;
        $resultado = $this->Usuario->verificaLogin($login, $senha);
        if ($resultado != array()) {

            $logou = true;
            $this->Session->write("Usuario.logado", true);
            $this->Session->write("Usuario.id", $resultado[0]["usuarios"]["id"]);
            $this->Session->write("Usuario.nivel_de_acesso", $resultado[0]["usuarios"]["cidade_id"]);
            $this->Session->write("Usuario.permissao", $resultado[0]["usuarios"]["permissao"]);
        }
        $this->set("logou", $logou);
    }

    function logout() {

        $this->Session->destroy();
        $this->redirect('/adm/login');
    }

    function index() {

        $this->Autenticador->autentica();
    }

    //atualizar fotos
    function atualizarFotos(){
        
        $this->Autenticador->autentica();
        //get fotos a serem atualizadas
        $this->set("fotos", $this->Imovel->getAllFotos());
        
    }
    /*function getFotosAtualizar(){
        
        $this->layout = "ajax";
        $this->Autenticador->autentica();
        
        //deleta todas as imagens antigas
        $this->deleteAll("img/imoveis", true);
        
        //get fotos a serem atualizadas
        $this->set("fotos", $this->Imovel->getAllFotos());
        
    }*/
    
    function atualizarFoto($id) {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        
        //delete as imagens antigas
        if(file_exists("img/imoveis/".$id."_1.jpg")){
            unlink("img/imoveis/".$id."_1.jpg");
        }
        if(file_exists("img/imoveis/".$id."_2.jpg")){
            unlink("img/imoveis/".$id."_2.jpg");
        }
        if(file_exists("img/imoveis/".$id."_3.jpg")){
            unlink("img/imoveis/".$id."_3.jpg");
        }
        
        //recria a imagem nova
        $tamanhos = array();
        $tamanhos[] = array("w" => 439, "h" => 298);
        $tamanhos[] = array("w" => 282, "h" => 239);
        $tamanhos[] = array("w" => 81, "h" => 55);
        for ($i = 1; $i <= 3; $i++) {

            $this->GeradorImagem->redimensionarRecortarSemFaixa(
                    "../../fotos_imoveis/" . $id . ".jpg"
                    , "img/imoveis"
                    , $id . "_" . $i
                    , ".jpg"
                    , $tamanhos[$i - 1]["w"]
                    , $tamanhos[$i - 1]["h"]
            );
            
        }
        $this->Imovel->updateCreateMiniatura($idFoto);
    }

    //condominios
    function condominios() {

        $this->Autenticador->autentica();      
        $this->set("condominios", $this->Condominio->getAll());
        
    }
    
    function condominiosCadastrar() {
        $this->Autenticador->autentica();
        $this->set("cidades", $this->Cidade->getAll());
    }

    function processaCondominiosCadastrar() {
        $this->layout = "ajax";
        $this->Autenticador->autentica();
    
        $cidade = $this->data["Condominio"]["cidade_id"];
        $titulo = $this->data["Condominio"]["titulo"];
        $descricao = $this->data["Condominio"]["descricao"];
        $imoveis = $this->data["Condominio"]["imoveis"];
    
        $ok = 'false';
        if ($cidade != "" && $titulo != "" && $descricao != "" && $imoveis != "") {
            $this->Condominio->insert($cidade, $titulo, $descricao, $imoveis
                    , $this->data["Condominio"]["lancamento"]
                    , $this->data["Condominio"]["destaque"]
            );
            $ok = 'true';
        }
        $this->set("ok", $ok);
    }

    function processaCondominiosCadastrar2() {
        $this->layout = "ajax";
        $this->Autenticador->autentica();
        
        $cidade = $this->data["Condominio"]["cidade_id"];
        $titulo = $this->data["Condominio"]["titulo"];
        $descricao = $this->data["Condominio"]["descricao"];
        $imoveis = $this->data["Condominio"]["imoveis"];
    
        $ok = 'false';
        if ($cidade != "" && $titulo != "" && $descricao != "" && $imoveis != "") {
            $this->Condominio->insert($cidade, $titulo, $descricao, $imoveis
                    , $this->data["Condominio"]["lancamento"]
                    , $this->data["Condominio"]["destaque"]
            );
            $ok = 'true';
        }
        $this->set("ok", $ok);
        $resultado = $this->Condominio->getLastInsertId();
        $this->set("id", $resultado[0][0]["id"]);
    }

    function condominiosEditar($id) {
        $this->Autenticador->autentica();
        $this->set("condominio", $this->Condominio->get($id));
        $this->set("cidades", $this->Cidade->getAll());
    }

    function processaCondominiosEditar() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        $this->Condominio->update(
                $this->data["Condominio"]["id"]
                , $this->data["Condominio"]["titulo"]
                , $this->data["Condominio"]["descricao"]
                , $this->data["Condominio"]["imoveis"]
                , $this->data["Condominio"]["lancamento"]
                , $this->data["Condominio"]["destaque"]
                , $this->data["Condominio"]["cidade_id"]
        );
    }

    function condominiosExcluir($id) {

        $this->Autenticador->autentica();
        $resultado = $this->Condominio->getFotos($id);
        foreach ($resultado as $r) {

            unlink("img/condominios/" . $r["fotos_condominios"]["id"] . "_1.jpg");
            unlink("img/condominios/" . $r["fotos_condominios"]["id"] . "_2.jpg");
            unlink("img/condominios/" . $r["fotos_condominios"]["id"] . "_3.jpg");
            $this->Condominio->deleteFoto($r["fotos_condominios"]["id"]);
        }
        $this->Condominio->delete($id);
    }

    function condominiosEditarFotos($id) {

        $this->Autenticador->autentica();
        $this->set("fotos", $this->Condominio->getFotos($id));
        $this->set("condominio_id", $id);
    }

    function processaCondominiosEditarFotos() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        $idCondominio = $this->data["Foto"]["condominio_id"];
        $eh_valido = 'false';
        if ($this->GeradorImagem->ehValida($this->data["Foto"]["arquivo"], 1)) {

            $eh_valido = 'true';
            //inseri no banco
            if ($this->data["Foto"]["destaque"] == "true") {
                $this->Condominio->desmarcaDestaque($this->data["Foto"]["condominio_id"]);
            }
            $this->Condominio->insertFoto(
                $this->data["Foto"]["condominio_id"]
                , $this->data["Foto"]["legenda"]
                , $this->data["Foto"]["destaque"]
                , $this->data["Foto"]["ordem"]
            );
            $tamanhos = array();
            $tamanhos[] = array("w" => 439, "h" => 298);
            $tamanhos[] = array("w" => 282, "h" => 239);
            $tamanhos[] = array("w" => 81, "h" => 55);
            //get last id
            $resultado = $this->Condominio->getLastInsertId();
            $id = $resultado[0][0]["id"];
            $this->GeradorImagem->uploadDaImagem(
                    $this->data["Foto"]["arquivo"]
                    , "img/temp"
                    , $id
                    , ".jpg"
            );
            for ($i = 1; $i <= 3; $i++) {

                $this->GeradorImagem->redimensionarRecortarSemFaixa(
                        "img/temp/" . $id . ".jpg"
                        , "img/condominios"
                        , $id . "_" . $i
                        , ".jpg"
                        , $tamanhos[$i - 1]["w"]
                        , $tamanhos[$i - 1]["h"]
                );
            }
            unlink("img/temp/" . $id . ".jpg");
            $this->set("condominio_id", $this->data["Foto"]["condominio_id"]);
        }
        $this->set("eh_valido", $eh_valido);
        $this->set("idCondominio", $idCondominio);
    }

    function condominiosExcluirFoto($id, $condominio_id) {

        $this->Autenticador->autentica();
        unlink("img/condominios/" . $id . "_1.jpg");
        unlink("img/condominios/" . $id . "_2.jpg");
        unlink("img/condominios/" . $id . "_3.jpg");
        $this->Condominio->deleteFoto($id);
        $this->set("condominio_id", $condominio_id);
    }

    function condominiosEditarFoto($id) {

        $this->Autenticador->autentica();
        $this->set("foto", $this->Condominio->getFoto($id));
    }

    function processaCondominiosEditarFoto() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        if ($this->data["Foto"]["destaque"] == "true") {
            $this->Condominio->desmarcaDestaque($this->data["Foto"]["condominio_id"]);
        }
        $this->Condominio->updateFoto(
                $this->data["Foto"]["id"]
                , $this->data["Foto"]["legenda"]
                , $this->data["Foto"]["ordem"]
                , $this->data["Foto"]["destaque"]
        );
        $this->set("condominio_id", $this->data["Foto"]["condominio_id"]);
    }

















    //obrasConcluidas
    function obrasConcluidas() {

        $this->Autenticador->autentica();
        $this->set("obras_concluidas", $this->ObraConcluida->getAll());

    }

    function ObrasConcluidasCadastrar() {

        $this->Autenticador->autentica();

    }

    function processaObrasConcluidasCadastrar() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        $this->ObraConcluida->insert(
                $this->Session->read('Usuario.nivel_de_acesso')
                , $this->data["ObraConcluida"]["titulo"]
                , $this->data["ObraConcluida"]["descricao"]
                , $this->data["ObraConcluida"]["imoveis"]
                , $this->data["ObraConcluida"]["lancamento"]
                , $this->data["ObraConcluida"]["destaque"]
        );
    }

    function processaObrasConcluidasCadastrar2() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        $this->ObraConcluida->insert(
                $this->Session->read('Usuario.nivel_de_acesso')
                , $this->data["ObraConcluida"]["titulo"]
                , $this->data["ObraConcluida"]["descricao"]
                , $this->data["ObraConcluida"]["imoveis"]
                , $this->data["ObraConcluida"]["lancamento"]
                , $this->data["ObraConcluida"]["destaque"]
        );
        $resultado = $this->ObraConcluida->getLastInsertId();
        $this->set("id", $resultado[0][0]["id"]);
    }

    function ObrasConcluidasEditar($id) {

        $this->Autenticador->autentica();
        $this->set("obra_concluida", $this->ObraConcluida->get($id));

    }

    function processaObrasConcluidasEditar() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        $this->ObraConcluida->update(
                $this->data["ObraConcluida"]["id"]
                , $this->data["ObraConcluida"]["titulo"]
                , $this->data["ObraConcluida"]["descricao"]
                , $this->data["ObraConcluida"]["imoveis"]
                , $this->data["ObraConcluida"]["lancamento"]
                , $this->data["ObraConcluida"]["destaque"]
        );
    }

    function obrasConcluidasExcluir($id) {

        $this->Autenticador->autentica();
        $resultado = $this->ObraConcluida->getFotos($id);
        foreach ($resultado as $r) {

            unlink("img/obras_concluidas/" . $r["fotos_obras_concluidas"]["id"] . "_1.jpg");
            unlink("img/obras_concluidas/" . $r["fotos_obras_concluidas"]["id"] . "_2.jpg");
            unlink("img/obras_concluidas/" . $r["fotos_obras_concluidas"]["id"] . "_3.jpg");
            $this->ObraConcluida->deleteFoto($r["fotos_obras_concluidas"]["id"]);
        }
        $this->ObraConcluida->delete($id);
    }

    function obrasConcluidasEditarFotos($id) {

        $this->Autenticador->autentica();
        $this->set("fotos", $this->ObraConcluida->getFotos($id));
        $this->set("obra_concluida_id", $id);
    }

    function processaObrasConcluidasEditarFotos() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        $eh_valido = false;
        if ($this->GeradorImagem->ehValida($this->data["Foto"]["arquivo"], 1)) {

            $eh_valido = true;
            //inseri no banco
            if ($this->data["Foto"]["destaque"] == "true") {
                $this->ObraConcluida->desmarcaDestaque($this->data["Foto"]["obra_conluida_id"]);
            }
            $this->ObraConcluida->insertFoto(
                $this->data["Foto"]["obra_concluida_id"]
                , $this->data["Foto"]["legenda"]
                , $this->data["Foto"]["destaque"]
                , $this->data["Foto"]["ordem"]
            );
            $tamanhos = array();
            $tamanhos[] = array("w" => 439, "h" => 298);
            $tamanhos[] = array("w" => 282, "h" => 239);
            $tamanhos[] = array("w" => 81, "h" => 55);
            //get last id
            $resultado = $this->ObraConcluida->getLastInsertId();
            $id = $resultado[0][0]["id"];
            $this->GeradorImagem->uploadDaImagem(
                    $this->data["Foto"]["arquivo"]
                    , "img/temp"
                    , $id
                    , ".jpg"
            );
            for ($i = 1; $i <= 3; $i++) {

                $this->GeradorImagem->redimensionarRecortarSemFaixa(
                        "img/temp/" . $id . ".jpg"
                        , "img/obras_concluidas"
                        , $id . "_" . $i
                        , ".jpg"
                        , $tamanhos[$i - 1]["w"]
                        , $tamanhos[$i - 1]["h"]
                );
            }
            unlink("img/temp/" . $id . ".jpg");
            $this->set("obra_concluida_id", $this->data["Foto"]["obra_concluida_id"]);
        }
        $this->set("eh_valido", $eh_valido);
    }

    function obrasConcluidasExcluirFoto($id, $obra_concluida_id) {

        $this->Autenticador->autentica();
        unlink("img/obras_concluidas/" . $id . "_1.jpg");
        unlink("img/obras_concluidas/" . $id . "_2.jpg");
        unlink("img/obras_concluidas/" . $id . "_3.jpg");
        $this->ObraConcluida->deleteFoto($id);
        $this->set("obra_concluida_id", $obra_concluida_id);
    }

    function obrasConcluidasEditarFoto($id) {

        $this->Autenticador->autentica();
        $this->set("foto", $this->ObraConcluida->getFoto($id));
    }

    function processaObrasConcluidasEditarFoto() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        if ($this->data["Foto"]["destaque"] == "true") {
            $this->ObraConcluida->desmarcaDestaque($this->data["Foto"]["obra_concluida_id"]);
        }
        $this->ObraConcluida->updateFoto(
                $this->data["Foto"]["id"]
                , $this->data["Foto"]["legenda"]
                , $this->data["Foto"]["ordem"]
                , $this->data["Foto"]["destaque"]
        );
        $this->set("obra_concluida_id", $this->data["Foto"]["obra_concluida_id"]);
        
    }










    //home
    function home() {

        $this->Autenticador->autentica();
    }

    function homeDestaques() {

        $this->Autenticador->autentica();
        $this->set("destaques", $this->HomeDestaque->getAll());
        
    }

    function processaHomeUpdateDestaque() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        if($this->data["Imovel"]["eh_condominio"]){
            if($this->data["Imovel"]["eh_destaque"]){
                $this->HomeDestaque->delete($this->data["Imovel"]["id"], "1", $this->data["Imovel"]["filial"]);
            }else{
                $this->HomeDestaque->insert($this->data["Imovel"]["id"], "1", $this->data["Imovel"]["filial"]);
            }
        }else{
            if($this->data["Imovel"]["eh_destaque"]){
                $this->HomeDestaque->delete($this->data["Imovel"]["id"], "0", $this->data["Imovel"]["filial"]);
            }else{
                $this->HomeDestaque->insert($this->data["Imovel"]["id"], "0", $this->data["Imovel"]["filial"]);
            }
        }
        
    }

    function processaHomeRemoverDestaques() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        $this->HomeDestaque->deleteAll();
        
    }

    function homeBanners() {

        $this->Autenticador->autentica();
        $this->set("banners", $this->Banner->getAll());
    }

    function homeBannersCadastrar() {

        $this->Autenticador->autentica();
    }

    function processaHomeBannersCadastrar() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        $eh_valido = false;
        if ($this->GeradorImagem->ehValida($this->data["Foto"]["arquivo"], 1)) {

            $eh_valido = true;
            $this->Banner->insert(
                    $this->data["Banner"]["linha1"]
                    , $this->data["Banner"]["linha2"]
                    , $this->data["Banner"]["link"]
                    , $this->data["Banner"]["ordem"]
            );
            $tamanhos = array();
            $tamanhos[] = array("w" => 610, "h" => 288);
            //get last id
            $resultado = $this->Condominio->getLastInsertId();
            $id = $resultado[0][0]["id"];
            $this->GeradorImagem->uploadDaImagem(
                    $this->data["Foto"]["arquivo"]
                    , "img/temp"
                    , $id
                    , ".jpg"
            );
            $this->GeradorImagem->redimensionarRecortarSemFaixa(
                    "img/temp/" . $id . ".jpg"
                    , "img/banners"
                    , $id
                    , ".jpg"
                    , $tamanhos[0]["w"]
                    , $tamanhos[0]["h"]
            );
            unlink("img/temp/" . $id . ".jpg");
        }
        $this->set("eh_valido", $eh_valido);
    }

    function homeBannersEditar($id) {

        $this->Autenticador->autentica();
        $this->set("banner", $this->Banner->get($id));
    }

    function processaHomeBannersEditar() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        $this->Banner->update(
                $this->data["Banner"]["id"]
                , $this->data["Banner"]["linha1"]
                , $this->data["Banner"]["linha2"]
                , $this->data["Banner"]["link"]
                , $this->data["Banner"]["ordem"]
        );
    }

    function processaHomeBannersExcluir($id) {

        $this->Autenticador->autentica();
        $this->Banner->delete($id);
        unlink("img/banners/" . $id . ".jpg");
    }

    function homeIndicadores() {

        $this->Autenticador->autentica();
        $this->set("indicadores", $this->Indicador->get());
    }

    function processaHomeIndicadores() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        $this->Indicador->update(
                $this->data["Indicador"]["linha1"]
                , $this->data["Indicador"]["linha2"]
        );
    }
    
    function imoveisDeLuxo() {

        $this->Autenticador->autentica();
        $this->set("imoveis", $this->Imovel->getAll());
        
    }

    function processaImoveisDeLuxo() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        if($this->data["Imovel"]["eh_luxo"]){
            $this->ImoveisDeLuxo->delete($this->data["Imovel"]["id"], $this->data["Imovel"]["filial"]);
        }else{
            $this->ImoveisDeLuxo->insert($this->data["Imovel"]["id"], $this->data["Imovel"]["filial"]);
        }
        
    }

    /*function obrasConcluidas() {

        $this->Autenticador->autentica();
        $this->set("imoveis", $this->Imovel->getAll());
        
    }

    function processaObrasConcluidas() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        if($this->data["Imovel"]["eh_construtora"]){
            $this->ObraConcluida->delete($this->data["Imovel"]["id"], $this->data["Imovel"]["filial"]);
        }else{
            $this->ObraConcluida->insert($this->data["Imovel"]["id"], $this->data["Imovel"]["filial"]);
        }
    }*/

    function ofertas() {

        $this->Autenticador->autentica();
        
        $t = $this->Imovel->getAll();
        
        //echo "<pre>";
        //print_r($t);
        //echo "</pre>";
        
        $this->set("imoveis", $t);    
    }

    function processaOfertas() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        if($this->data["Imovel"]["eh_oferta"]){
            $this->Oferta->delete($this->data["Imovel"]["id"], $this->data["Imovel"]["filial"]);
        }else{
            $this->Oferta->insert($this->data["Imovel"]["id"], $this->data["Imovel"]["filial"]);
        }
    }
    
    function news() {

        $this->Autenticador->autentica();
        $imoveis = $this->Imovel->getAll();
        $fotos = $this->Imovel->getAllFotos();
        $resultado = $this->Imovel->crossJoin($imoveis, $fotos);
        $array_imoveis = array();
        foreach($resultado as $r){
          $array_imoveis[] = array(
              "informacoes" => $r["info"],
              "fotos" => $r["foto"]
          );
        }
        $this->set("imoveis", $array_imoveis);
    }
    
    function compareordem($a,$b){
    	#if ($a["ordem"]==$b["ordem"]) return 0;
			return ((int)$a["ordem"]<(int)$b["ordem"])?-1:1;
    }

    function processaNews(){
        
        $this->Autenticador->autentica();
        $this->layout = "ajax";
        $eh_valido = true;
        if(!$this->Validador->emailValido($this->data["News"]["email"])){
            $eh_valido = false;
            $erro="Preencha um e-mail vÃ¡lido.";
        }
        if($this->data["News"]["imoveis"] == ""){
            $eh_valido = false;
            $erro="Selecione um imÃ³vel.";
        }
        if($eh_valido){
            $imoveis = explode("---", $this->data["News"]["imoveis"]);
            $imoveis_array = array();
            foreach($imoveis as $i){
                $informacoes = $imoveis = explode("@@", $i);
                $imoveis_array[] = array(
                    "codigo"      => $informacoes[0]
                    , "titulo"    => $informacoes[1]
                    , "descricao" => $informacoes[2]
                    , "cidade"    => $informacoes[3]
                    , "url"       => $informacoes[4]
                    , "foto"      => $informacoes[5]
                    , "filial"      => $informacoes[6]
                    , "ordem"      => $informacoes[7]
                );
            }
            
            usort($imoveis_array, array($this, "compareordem"));
           	
            $this->set("imoveis", $imoveis_array);
            $this->Email->to = $this->data["News"]["email"];
            $this->Email->subject = 'News';
            $this->Email->from = "site@imobiliariacasadagente.com.br";
            $this->Email->template = 'envia_news';
            $this->Email->sendAs = 'html';
            $this->Email->smtpOptions = array(
              'port' => '587',
              'timeout' => '15',
              'host' => 'mail.imobiliariacasadagente.com.br',
              'username' => 'site@imobiliariacasadagente.com.br',
              'password' => 's1tecdg32955555'
            );
            $this->Email->delivery = 'smtp';
            $this->Email->send();
            
        }else{
            $this->set("erro", $erro);
        }
        $this->set("eh_valido", $eh_valido);
        
    }

    private function deleteAll($directory, $empty = false) {
        if (substr($directory, -1) == "/") {
            $directory = substr($directory, 0, -1);
        }
        if (!file_exists($directory) || !is_dir($directory)) {
            return false;
        } elseif (!is_readable($directory)) {
            return false;
        } else {
            $directoryHandle = opendir($directory);
            while ($contents = readdir($directoryHandle)) {
                if ($contents != '.' && $contents != '..') {
                    $path = $directory . "/" . $contents;
                    if (is_dir($path)) {
                        $this->deleteAll($path);
                    } else {
                        unlink($path);
                    }
                }
            }
            closedir($directoryHandle);
            if ($empty == false) {
                if (!rmdir($directory)) {
                    return false;
                }
            }
            return true;
        }
    }


    /* Lojas */
    function lojas() {

        $this->Autenticador->autentica();
    }

    function lojasTextoPagina() {

        $this->Autenticador->autentica();
        $this->set("texto", $this->Texto->getAll());
    }

    function processaLojasTextoPaginaEditar() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        $this->Texto->update(
            "1"
            , $this->data["Texto"]["texto"]
        );
    }

    function lojasFotos() {
        $this->Autenticador->autentica();
        $this->set("enderecos", $this->Endereco->getAll());
    }

    function processaLojasFotosEditar() {
        $this->layout = "ajax";
        $this->Autenticador->autentica();
        $eh_valido = false;
        if ($this->GeradorImagem->ehValida($this->data["Foto"]["arquivo"], 1)) {
            $eh_valido = true;
            $tamanhos = array();
            $tamanhos[] = array("w" => 282, "h" => 239);
            $id = $this->data["Foto"]["id"];
            $this->GeradorImagem->uploadDaImagem(
                    $this->data["Foto"]["arquivo"]
                    , "img/temp"
                    , $id
                    , ".jpg"
            );
            unlink("img/lojas/" . $id . ".jpg");
            $this->GeradorImagem->redimensionarRecortarSemFaixa(
                    "img/temp/" . $id . ".jpg"
                    , "img/lojas"
                    , $id
                    , ".jpg"
                    , $tamanhos[0]["w"]
                    , $tamanhos[0]["h"]
            );
            unlink("img/temp/" . $id . ".jpg");
        }
        $this->set("eh_valido", $eh_valido);
    }

    function lojasCorretores() {
        $this->Autenticador->autentica();
        $this->set("corretores", $this->Corretor->getAll());
    }

    function lojasCorretoresCadastrar() {
        $this->Autenticador->autentica();
        $this->set("cidades", $this->Cidade->getAll());
    }

    function processaLojasCorretoresCadastrar() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        $this->Corretor->insert(
            $this->data["Corretor"]["filial"]
            , $this->data["Corretor"]["texto"]
        );
    }

    function lojasCorretoresEditar($id) {

        $this->Autenticador->autentica();
        $this->set("corretor", $this->Corretor->get($id));
        $this->set("cidades", $this->Cidade->getAll());
        
    }

    function processaLojasCorretoresEditar() {

        $this->layout = "ajax";
        $this->Autenticador->autentica();
        $this->Corretor->update(
            $this->data["Corretor"]["id"]
            , $this->data["Corretor"]["filial"]
            , $this->data["Corretor"]["texto"]
        );
    }

    function lojasCorretoresExcluir($id) {

        $this->Autenticador->autentica();
        $this->Corretor->delete($id);
    }
    
    function atualizarSenha(){

        $this->Autenticador->autentica();        
    }
    
    function processaAtualizacaoSenha(){
        
        $this->layout = "ajax";
        $this->Autenticador->autentica();
        
        $senhaAtual = $this->data['Usuario']['senha_atual'];
        $novaSenha = $this->data['Usuario']['nova_senha'];
        $confirmaNovaSenha = $this->data['Usuario']['confirma_nova_senha'];
        
        $troca = 0;
        if ($novaSenha != "" && $confirmaNovaSenha != "" && $senhaAtual != "") {
            $idUsuario = $this->Session->read("Usuario.id");
            $usuarioAtual = $this->Usuario->getUsuario($idUsuario, $senhaAtual);
            if ($usuarioAtual[0]) {
                if ($novaSenha == $confirmaNovaSenha) {
                    $this->Usuario->atualizarSenha($idUsuario, $novaSenha);
                    $troca = 2;
                } else {
                    $troca = 1;
                }
            } else {
                $troca = 3;
            }
        }
        
        $this->set("troca", $troca);
    }

    // buscar cidades
    function cidades() {
        $this->Autenticador->autentica();       
        $this->set("cidades", $this->Cidade->getAll());
    }
    
    function cidadesCadastrar() {
        $this->Autenticador->autentica();
    }
    
    function processaCidadesCadastrar(){
        $this->layout = "ajax";
        $this->Autenticador->autentica();
        
        $nome = $this->data['Cidade']['nome'];
        $emails = $this->data['Cidade']['emails'];
        if ($emails != "") {
            $emails = str_replace(";", ",", $emails); 
        }
        $litoral = $this->data['Cidade']['litoral'];
        
        $ok = 'false';
        if ($nome != "" && $litoral != "") {
            $this->Cidade->insert($nome, $emails, $litoral);
            $ok = 'true';
        }
        
        $this->set("ok", $ok);
    }
    
    function cidadesEditar($id) {
        $this->Autenticador->autentica();        
        $this->set("cidade", $this->Cidade->get($id));
    }
    
    function processaCidadesEditar(){
        $this->layout = "ajax";
        $this->Autenticador->autentica();
        
        $id = $this->data['Cidade']['id'];
        $nome = $this->data['Cidade']['nome'];
        $emails = $this->data['Cidade']['emails'];
        if ($emails != "") {
            $emails = str_replace(";", ",", $emails); 
        }
        $litoral = $this->data['Cidade']['litoral'];
        
        $ok = 'false';
        if ($nome != "" && $litoral != "") {
            $this->Cidade->update($id, $nome, $emails, $litoral);
            $ok = 'true';
        }
        
        $this->set("ok", $ok);
    }
    
    function cidadesExcluir() {
        $this->layout = "ajax";
        $this->Autenticador->autentica();
        
        $idCidade = $this->data['Cidade']['id'];
        $canDeleted = $this->Cidade->canCidadeDeleted($idCidade);
        
        $ok = 'false';
        if ($canDeleted[0] == "") {
            $this->Cidade->delete($idCidade);
            $ok = 'true';
        }
        
        $this->set("ok", $ok);
    }
    
    function processaCidadeLitoral(){
        $this->layout = "ajax";
        $this->Autenticador->autentica();
        
        $idCidade = $this->data['Cidade']['id'];
        $litoral = $this->data['Cidade']['litoral'];
        
        $ok = 'false';
        if ($idCidade != "" && $litoral != "") {
            $this->Cidade->setLitoral($idCidade, $litoral);
            $ok = 'true';
        }
        
        $this->set("ok", $ok);
    }

    
    // USUARIOS

    function usuarios() {
        $this->Autenticador->autentica();        
        $this->set("usuarios", $this->Usuario->getAll());
    }

    function usuariosCadastrar() {
        $this->Autenticador->autentica();
        $this->set("cidades", $this->Cidade->getAll());
    }
    
    function processaUsuariosCadastrar() {
        $this->layout = "ajax";
        $this->Autenticador->autentica();
        
        $login = $this->data["Usuario"]["login"];
        $cidade = $this->data["Usuario"]["cidade"];
        $permissao = $this->data["Usuario"]["permissao"];
        
        $ok = 'false';
        if ($login != "" && $cidade != "" && $permissao != "") {
            $this->Usuario->insert($login, 'casadagente', $cidade, $permissao);
            $ok = 'true';
        }
        $this->set("ok", $ok);
    }
    
    function usuariosEditar($id) {
        $this->Autenticador->autentica();
        $this->set("usuario", $this->Usuario->get($id));
        $this->set("cidades", $this->Cidade->getAll());
    }
    
    function processaUsuariosEditar() {
        $this->layout = "ajax";
        $this->Autenticador->autentica();
        
        $id = $this->data["Usuario"]["id"];
        $login = $this->data["Usuario"]["login"];
        $cidade = $this->data["Usuario"]["cidade"];
        $permissao = $this->data["Usuario"]["permissao"];
        
        $ok = 'false';
        if ($login != "" && $cidade != "" && $permissao != "") {
            $this->Usuario->updateEdit($id, $login, $cidade, $permissao);
            $ok = 'true';
        }
        $this->set("ok", $ok);
    }
    
    function usuariosExcluir() {
        $this->layout = "ajax";
        $this->Autenticador->autentica();
        $this->Usuario->delete($this->data["Usuario"]["id"]);
    }

    
    // ### endere?s ###
    
    function lojasEnderecos() {
        $this->Autenticador->autentica();
        
        //echo "<pre>";
        //print_r($this->Endereco->getAll());
        //echo "</pre>";
        
        $this->set("enderecos", $this->Endereco->getAll());
    }
    
    function lojasEnderecosCadastrar() {
        $this->Autenticador->autentica();
        $this->set("cidades", $this->Cidade->getAll());
    }

    function processaLojasEnderecosCadastrar() {
        $this->layout = "ajax";
        $this->Autenticador->autentica();
        
        $cidade = $this->data["Endereco"]["cidade_id"];
        $endereco = $this->data["Endereco"]["endereco"];
        $telefone = $this->data["Endereco"]["telefone"];
        $cep = $this->data["Endereco"]["cep"];
        
        $ok = 'false';
        if ($cidade != "" && $endereco != "" && $telefone != "" && $cep != "") {
            $this->Endereco->insert( $cidade, $endereco, $telefone, $cep );
            $ok = 'true';
        }
        $this->set("ok", $ok);
    }

    function lojasEnderecosEditar($id) {
        $this->Autenticador->autentica();
        $this->set("endereco", $this->Endereco->get($id));
        $this->set("cidades", $this->Cidade->getAll());
    }

    function processaLojasEnderecosEditar() {
        $this->layout = "ajax";
        $this->Autenticador->autentica();
        
        $id = $this->data["Endereco"]["id"];
        $cidade = $this->data["Endereco"]["cidade_id"];
        $endereco = $this->data["Endereco"]["endereco"];
        $telefone = $this->data["Endereco"]["telefone"];
        $cep = $this->data["Endereco"]["cep"];
        
        $ok = 'false';
        if ($cidade != "" && $endereco != "" && $telefone != "" && $cep != "") {
            $this->Endereco->update( $id, $cidade, $endereco, $telefone, $cep );
            $ok = 'true';
        }
        $this->set("ok", $ok);
    }
    
    function lojasEnderecosExcluir() {
        $this->layout = "ajax";
        $this->Autenticador->autentica();
        $this->Endereco->delete($this->data["Endereco"]["id"]);
    }
    
}

?>