<?php
App::import('Sanitize');
class ImovelController extends AppController {
    
    var $name = "Imovel";
    var $uses = array("Cidade", "Imovel", "Condominio", "ObraConcluida", "Endereco");
    var $helpers = array("Text");
    var $components = array("GeradorImagem");

    function beforeFilter() {
        $this->layout = "imovel";
        $this->set("enderecos", $this->Endereco->getAll());
    }

    function index($url_sem_chamada, $identificador, $eh_condominio, $eh_obra_concluida, $filial) {

        $url_sem_chamada = Sanitize::escape($url_sem_chamada);
        $identificador = Sanitize::escape($identificador);
        $eh_condominio = Sanitize::escape($eh_condominio);
        $eh_obra_concluida = Sanitize::escape($eh_obra_concluida);
        $filial = Sanitize::escape($filial);
        
        //geral
        $cidades2 = $this->Cidade->getAll2();
        $this->set("cidades2", $cidades2);
        $cidades = $this->Cidade->getAll();
        $this->set("cidades", $cidades);
        $this->set("tipos", $this->Imovel->getTipos());
        $tipos_cidade = array();
        foreach($cidades2 as $c){
            $tipos = $this->Cidade->getTipos($c["tb_imoveis"]["nome"]);
            foreach($tipos as $t){
                $tipos_cidade[$c["tb_imoveis"]["nome"]][] = $t;
            }
        }
        $this->set("tipos_cidade", $tipos_cidade);
        
        //imovel
        $valor="Consultar";
        if($eh_condominio){
            
            $resultado = $this->Condominio->get($identificador);
            $resultado2 = $this->Condominio->getFotos($identificador);
            $cidade = $resultado[0]["cidades"]["nome"];
            $titulo = "Condomínio em ".$cidade." | ".$resultado[0]["condominios"]["titulo"];
            $titulo_imovel = $resultado[0]["condominios"]["titulo"];
            $descricao_imovel = $resultado[0]["condominios"]["descricao"];
            
        }elseif($eh_obra_concluida){
            
            $resultado = $this->ObraConcluida->get($identificador);
            $resultado2 = $this->ObraConcluida->getFotos($identificador);
            $cidade = $resultado[0]["cidades"]["nome"];
            $titulo = "Obra Concluída em ".$cidade." | ".$resultado[0]["obras_concluidas"]["titulo"];
            $titulo_imovel = $resultado[0]["obras_concluidas"]["titulo"];
            $descricao_imovel = $resultado[0]["obras_concluidas"]["descricao"];
            
        }else{
            
            $resultado = $this->Imovel->get($identificador, $filial);
            $resultado2 = $this->Imovel->getFotos($identificador, $filial);
            $informacoes = explode("!", $resultado[0]["tb_imoveis"]["f_descricao"], 2);
            $titulo_imovel = trim(utf8_encode($informacoes[0]));
            $descricao_imovel = trim(utf8_encode($informacoes[1]));
            $cidade = $resultado[0]["tb_imoveis"]["f_cidade"];
            $titulo = $resultado[0]["tb_imoveis"]["f_tipo"]." em ".$cidade." - ".$titulo_imovel;
            if($resultado[0]["tb_imoveis"]["f_tipo"] != ""){
                $valor = number_format($resultado[0]["tb_imoveis"]["f_valor"],2,",",".");
            }

            $resultadoFtMini = $this->gerarMiniaturasFotos($resultado[0]["tb_imoveis"]["f_codigo"], $filial);
        }
        $this->set("ft_mini", $resultadoFtMini);
        $this->set("titulo", $titulo);
        $this->set("titulo_imovel", $titulo_imovel);
        $this->set("descricao_imovel", $descricao_imovel);
        $this->set("valor_imovel", $valor);
        $this->set("cidade_imovel", $cidade);
        $this->set("identificador", $identificador);
        $this->set("eh_condominio", $eh_condominio);
        $this->set("eh_obra_concluida", $eh_obra_concluida);
        $this->set("filial", $filial);
        $this->set("imovel", $resultado[0]);
        $this->set("fotos", $resultado2);
        
    }
    
    function gerarMiniaturasFotos($id, $filial) {
    	$resultadoFtMini = $this->Imovel->getFotosSemMiniatura($id, $filial);
    	foreach($resultadoFtMini as $mini){
    		$idFoto = $mini["tb_imoveis_fotos"]["f_id_foto"];
			$this->atualizarFoto($idFoto);
			$this->Imovel->updateCreateMiniatura($idFoto);
        }
    }
    
    function atualizarFoto($id) {
        
        //delete as imagens antigas
        if(file_exists("../webroot/img/imoveis/".$id."_1.jpg")){
            unlink("../webroot/img/imoveis/".$id."_1.jpg");
        }
        if(file_exists("../webroot/img/imoveis/".$id."_2.jpg")){
            unlink("../webroot/img/imoveis/".$id."_2.jpg");
        }
        if(file_exists("../webroot/img/imoveis/".$id."_3.jpg")){
            unlink("../webroot/img/imoveis/".$id."_3.jpg");
        }
        
        //recria a imagem nova
        $tamanhos = array();
        $tamanhos[] = array("w" => 439, "h" => 298);
        $tamanhos[] = array("w" => 282, "h" => 239);
        $tamanhos[] = array("w" => 81, "h" => 55);
        for ($i = 1; $i <= 3; $i++) {

                    // "../webroot/img/imoveis" . $id . ".jpg"
            $this->GeradorImagem->redimensionarRecortarSemFaixa(
                    "../webroot/img/imoveis/" . $id . ".jpg"
                    , "img/imoveis"
                    , $id . "_" . $i
                    , ".jpg"
                    , $tamanhos[$i - 1]["w"]
                    , $tamanhos[$i - 1]["h"]
            );
        }
    }
    
    function getMaisImoveis(){
        
        $this->layout = "ajax";
        $imoveis = $this->Imovel->getMaisImoveis(
            $tipo = utf8_decode($this->data["Busca"]["tipo"])
            , utf8_decode($this->data["Busca"]["codigo"])
            , utf8_decode($this->data["Busca"]["extra"])
            , utf8_decode($this->data["Busca"]["cidade"])
            , utf8_decode($this->data["Busca"]["bairro"])
            , utf8_decode($this->data["Busca"]["dormitorios"])
            , utf8_decode($this->data["Busca"]["garagem"])
            , utf8_decode($this->data["Busca"]["valor_de"])
            , utf8_decode($this->data["Busca"]["valor_ate"])
            , utf8_decode($this->data["Busca"]["offset"])
            , 3
        );
        $paginas = $this->Imovel->getQtdPaginasMaisImoveis(
            $tipo = utf8_decode($this->data["Busca"]["tipo"])
            , utf8_decode($this->data["Busca"]["codigo"])
            , utf8_decode($this->data["Busca"]["extra"])
            , utf8_decode($this->data["Busca"]["cidade"])
            , utf8_decode($this->data["Busca"]["bairro"])
            , utf8_decode($this->data["Busca"]["dormitorios"])
            , utf8_decode($this->data["Busca"]["garagem"])
            , utf8_decode($this->data["Busca"]["valor_de"])
            , utf8_decode($this->data["Busca"]["valor_ate"])
            , 3
        );
        $this->set("offset", $this->data["Busca"]["offset"]);
        $this->set("paginaAtual", ($this->data["Busca"]["offset"]/3)+1);
        $this->set("totalPaginas", ceil($paginas[0][0]["qtd"]));
        $this->set("imoveis", $imoveis);
        
    }
    
    function getMaisImoveis3(){
        
        $this->layout = "ajax";
        $imoveis = $this->Imovel->getMaisImoveis(
            $tipo = utf8_decode($this->data["Busca"]["tipo"])
            , $this->data["Busca"]["codigo"]
            , $this->data["Busca"]["extra"]
            , $this->data["Busca"]["cidade"]
            , $this->data["Busca"]["bairro"]
            , $this->data["Busca"]["dormitorios"]
            , $this->data["Busca"]["garagem"]
            , $this->data["Busca"]["valor_de"]
            , $this->data["Busca"]["valor_ate"]
            , $this->data["Busca"]["offset"]
            , 9
        );
        $paginas = $this->Imovel->getQtdPaginasMaisImoveis(
            $tipo = utf8_decode($this->data["Busca"]["tipo"])
            , $this->data["Busca"]["codigo"]
            , $this->data["Busca"]["extra"]
            , $this->data["Busca"]["cidade"]
            , $this->data["Busca"]["bairro"]
            , $this->data["Busca"]["dormitorios"]
            , $this->data["Busca"]["garagem"]
            , $this->data["Busca"]["valor_de"]
            , $this->data["Busca"]["valor_ate"]
            , 9
        );
        $this->set("offset", $this->data["Busca"]["offset"]);
        $this->set("paginaAtual", ($this->data["Busca"]["offset"]/9)+1);
        $this->set("totalPaginas", ceil($paginas[0][0]["qtd"]));
        $this->set("imoveis", $imoveis);
        
    }
    
    function getMaisImoveis2(){
        
        $this->layout = "ajax";
        $imoveis = $this->Imovel->getDestaques(
            $this->data["Busca"]["offset"]
            , 3
        );
        $paginas = $this->Imovel->getQtdDestaques(3);
        $this->set("offset", $this->data["Busca"]["offset"]);
        $this->set("paginaAtual", ($this->data["Busca"]["offset"]/3)+1);
        $this->set("totalPaginas", ceil($paginas[0][0]["qtd"]));
        $this->set("imoveis", $imoveis);
        
    }
    
    function getImoveisCondominio(){
        
        $this->layout = "ajax";
        $resultado = $this->Condominio->get($this->data["Busca"]["condominio_id"]);
        $ids_imoveis = explode(", ", $resultado[0]["condominios"]["imoveis"]);
        $imoveis = array();
        for($i=$this->data["Busca"]["offset"]; $i<$this->data["Busca"]["offset"]+3; $i++){
            if($i<count($ids_imoveis)){
                $imovel = $this->Imovel->getMaisImoveis4(
                    $ids_imoveis[$i]
                );
                $imoveis[] = $imovel[0];
            }
        }
        $paginas = count($ids_imoveis)/3;
        $this->set("offset", $this->data["Busca"]["offset"]);
        $this->set("paginaAtual", ($this->data["Busca"]["offset"]/3)+1);
        $this->set("totalPaginas", ceil(count($ids_imoveis)/3));
        $this->set("imoveis", $imoveis);
        $this->set("identificador", $this->data["Busca"]["condominio_id"]);
        
    }
    
    function getImoveisObraConcluida(){

        $this->layout = "ajax";
        $resultado = $this->ObraConcluida->get($this->data["Busca"]["obra_concluida_id"]);
        $ids_imoveis = explode(", ", $resultado[0]["obras_concluidas"]["imoveis"]);
        $imoveis = array();
        for($i=$this->data["Busca"]["offset"]; $i<$this->data["Busca"]["offset"]+3; $i++){
            if($i<count($ids_imoveis)){
                $imovel = $this->Imovel->getMaisImoveis4(
                    $ids_imoveis[$i]
                );
                $imoveis[] = $imovel[0];
            }
        }
        $paginas = count($ids_imoveis)/3;
        $this->set("offset", $this->data["Busca"]["offset"]);
        $this->set("paginaAtual", ($this->data["Busca"]["offset"]/3)+1);
        $this->set("totalPaginas", ceil(count($ids_imoveis)/3));
        $this->set("imoveis", $imoveis);
        $this->set("identificador", $this->data["Busca"]["obra_concluida_id"]);

    }

    function test(){
        $tpw = $this->Imovel->getTiposNew();
        exit(var_dump($tpw));
    }

}
?>