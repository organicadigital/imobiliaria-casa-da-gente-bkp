<?php

error_reporting(0);

class ImagesController extends AppController {

    var $name = 'Images';
    var $uses = array();

    function index(){
//        Configure::write('debug', 1);        
        $path = ''; 

        $imagem_original =  $path . $_GET["url_image"];
        // $imagem_original =  $path . 'iconv(in_charset, out_charset, str);
        $imagem_size = getimagesize($imagem_original);

        if ($imagem_size[0] > 90) {

            $logo_img =  $path . "img/casa_da_gente_logo_small.png";
            ///$logo_img =  $path . "img/imobiliaria_casa_da_gente.png";
            //$logo_img =  $path . "img/logo_marca.gif";
            $padding = 10;
            $logo = imagecreatefrompng($logo_img);
            $imagem = imagecreatefromjpeg($imagem_original);


            $logo_size = getimagesize($logo_img);

            $logo_width = $logo_size[0];
            $logo_height = $logo_size[1];
            
            $dest_x = $imagem_size[0] - $logo_width - $padding;
            $dest_y = $imagem_size[1] - $logo_height - $padding;

            $this->autoRender = false;
            imagecopy($imagem, $logo, $dest_x, $dest_y, 0, 0, $logo_width, $logo_height);

            header("content-type: image/jpeg");
            imagejpeg($imagem);
            imagedestroy($imagem);
            imagedestroy($logo);
        } else {

            //$logo_img =  $path . "img/imobiliaria_casa_da_gente.png";
            $logo_img =  $path . "img/logo_marca_small.png";

            $padding = 3;
            $logo = imagecreatefrompng($logo_img);
            $imagem = imagecreatefromjpeg($imagem_original);

            $logo_size = getimagesize($logo_img);

            $logo_width = $logo_size[0];
            $logo_height = $logo_size[1];
            
            $dest_x = $imagem_size[0] - $logo_width - $padding;
            $dest_y = $imagem_size[1] - $logo_height - $padding;

            $this->autoRender = false;
            imagecopy($imagem, $logo, $dest_x, $dest_y, 0, 0, $logo_width, $logo_height);

            header("content-type: image/jpeg");
            imagejpeg($imagem);
            imagedestroy($imagem);
            imagedestroy($logo);

        }
    }
}
?>