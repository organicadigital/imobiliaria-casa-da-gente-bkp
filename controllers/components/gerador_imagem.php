<?php

class GeradorImagemComponent extends Object {
    /*
     * Este componet é utilizado para redimensionar, recortar e validar imagens.
     */

    private $extensoes_permitidas = array(".jpg", ".jpeg");

    function initialize($controller, $settings = array()) {
        $this->controller = $controller;
    }

    /*
     * Recebe como parametro um $file_name e retorna
     * uma imagem conforme o seu mime
     */

    private function carrega_file_name($file_name) {

        $info_arquivo = getimagesize($file_name);
        switch ($info_arquivo['mime']) {
            case "image/gif":
                $imagem = imagecreatefromgif($file_name);
                break;
            case "image/jpeg":
                $imagem = imagecreatefromjpeg($file_name);
                break;
            case "image/png":
                $imagem = imagecreatefrompng($file_name);
                break;
        }
        return $imagem;
    }

    /*
     * verifica se o arquivo não esta em branco
     * se tem um tamanho menor que o informado
     * e se tem uma extensão válida
     */

    function ehValida($arquivo, $maximo_mb) {

        $valida = false;
        if ($arquivo != "") {
            if ($arquivo["error"] != 4) {
                if ($arquivo["size"] < $maximo_mb * 1024 * 1024) {
                    foreach ($this->extensoes_permitidas as $extensao) {
                        if (strripos($arquivo["name"], $extensao) !== false) {
                            $valida = true;
                        }
                    }
                }
            }
        }
        return $valida;
    }

    /*
     * faz o upload de arquivo, para o caminho_destino, com o nome_destino e extensao_destino
     * o tamanho em pixels da imagem criada será a mesma da imagem enviada.
     * É aceito como entrada as extensões .jpg, .jpeg, .JPG, .JPEG, .gif, .GIF, .png e .PNG
     * É aceito como saída as extensões .jpg, .gif e .png
     */

    function uploadDaImagem($arquivo, $caminho_destino, $nome_destino, $extensao_destino) {

        //cria imagem temporária
        $extensao_original = strrchr($arquivo["name"], '.');
        $file_name = $caminho_destino . "/" . $nome_destino . "_t" . $extensao_original;
        move_uploaded_file($arquivo["tmp_name"], $file_name);

        //cria a nova imagem
        $info_arquivo = getimagesize($file_name);
        $largura_original = $info_arquivo[0];
        $altura_original = $info_arquivo[1];
        $imagem_de_fundo = imagecreatetruecolor($largura_original, $altura_original);
        $imagem = $this->carrega_file_name($file_name);
        imagecopyresampled(
                $imagem_de_fundo
                , $imagem
                , 0, 0, 0, 0
                , $largura_original, $altura_original
                , $largura_original, $altura_original
        );
        switch ($extensao_destino) {
            case ".jpg":
                imagejpeg($imagem_de_fundo, $caminho_destino . "/" . $nome_destino . $extensao_destino, 100);
                break;
            case ".gif":
                imagegif($imagem_de_fundo, $caminho_destino . "/" . $nome_destino . $extensao_destino);
                break;
            default :
                imagepng($imagem_de_fundo, $caminho_destino . "/" . $nome_destino . $extensao_destino);
                break;
        }

        //exclui imagem temporária
        unlink($file_name);
    }

    /*
     * recebe como entrada:
     * * file_name_original: string do caminho+nome do arquivo+extensão do arquivo
     * que será redimencionado e/ou recortado. Exemplo: img/empresas/id_imagem.jpg
     * * lugar_destino: string do caminho de destino sem a barra no final.
     * Exmeplo: img/empresas
     * * nome_destino: nome do arquivo que será salvo em lugar_destino,
     * sem extensão. Exemplo: id_imagem_1
     * * extensão_destino: são aceitos .jpg, .png, .gif
     * * largura_destino e altura_destino: tamanho da imagem a ser gerada.
     *
     * A função cria uma imagem no lugar_destino, com o nome_destino,
     * com extensao_destino, com largura_destino e com altura_destino.
     * A imagem criada possui a imagem original redimencionada da seguinte forma:
     * * não pode-se perder o aspecto ratio da imagem.
     * * a largura é igual a largura_destino e altura maior que altura_destino
     *   ou a largura é maior que largura_destino e altura igual a altura_destino
     * Um pedaço da imagem original pode ser recortado, este recorte é feito
     * com base no centro. Ou seja se sobrar verticalmente se recortar em cima
     * e em baixo, e se sobrar horizontalmente se recorta na direita e na esquerda.
     */

    function redimensionarRecortarSemFaixa($file_name_origem, $lugar_destino, $nome_destino, $extensao_destino, $largura_destino, $altura_destino) {

        //cria imagem de fundo
        $imagem_de_fundo = imagecreatetruecolor($largura_destino, $altura_destino);

        //descobre largura e altura originais
        $info_arquivo = getimagesize($file_name_origem);
        $largura_original = $info_arquivo[0];
        $altura_original = $info_arquivo[1];

        //cria imagem temporaria
        $imagem_temporaria = $this->carrega_file_name($file_name_origem);

        //primeira tentativa: a largura_temporaria é igual a largura_destino
        //e altura_temporaria maior que altura_destino
        $porcentagem = round((100 * $largura_destino) / $largura_original);
        $largura_temporaria = $largura_destino;
        $altura_temporaria = round(($altura_original * $porcentagem) / 100);
        $area_de_recorte_horizontal_da_esquerda = 0;
        $altura_temporaria - $altura_destino;
        $area_de_recorte_vertical_de_cima = round(((($altura_temporaria - $altura_destino) * 100) / $porcentagem) / 2);
        if ($altura_temporaria < $altura_destino) {

            //segunda tentativa: a largura_temporaria é maior que largura_destino
            //e altura_temporaria igual a altura_destino
            $porcentagem = round((100 * $altura_destino) / $altura_original);
            $altura_temporaria = $altura_destino;
            $largura_temporaria = round(($largura_original * $porcentagem) / 100);
            $area_de_recorte_horizontal_da_esquerda = round(((($largura_temporaria - $largura_destino) * 100) / $porcentagem) / 2);
            $area_de_recorte_vertical_de_cima = 0;
        }

        //redimenciona e recorta sobras
        imagecopyresampled(
                $imagem_de_fundo
                , $imagem_temporaria
                , 0, 0
                , $area_de_recorte_horizontal_da_esquerda
                , $area_de_recorte_vertical_de_cima
                , $largura_temporaria, $altura_temporaria
                , $largura_original, $altura_original
        );

        //salva nova imagem
        switch ($extensao_destino) {
            case ".jpg":
                imagejpeg($imagem_de_fundo, $lugar_destino . "/" . $nome_destino . $extensao_destino, 100);
                break;
            case ".png":
                imagepng($imagem_de_fundo, $lugar_destino . "/" . $nome_destino . $extensao_destino);
                break;
            default:
                imagegif($imagem_de_fundo, $lugar_destino . "/" . $nome_destino . $extensao_destino);
                break;
        }
    }

    /*
     * recebe como entrada:
     * $file_name_origem                  = string do caminho+nome do
     *                                      arquivo+extensão do arquivo que será
     *                                      redimencionado e/ou recortado.
     *                                      Exemplo: img/empresas/id_imagem.jpg
     * $lugar_destino                     = string do caminho de destino sem a
     *                                      barra no final.
     *                                      Exmeplo: img/empresas
     * $nome_destino                      = nome do arquivo que será salvo
     *                                      em lugar_destino, sem extensão.
     *                                      Exemplo: id_imagem_1
     * $extensao_destino                  = são aceitos .jpg, .png, .gif
     * $largura_destino e $altura_destino = tamanho da imagem a ser gerada.
     * $cor_de_fundo                      = esta cor aparece nos espaços não
     *                                      preenchidos.
     *
     * A função cria uma imagem no lugar_destino, com o nome_destino,
     * com extensao_destino, com largura_destino e com altura_destino.
     * Nos lugares que a imagem temporária não preencher espaço deve aparecer a
     * cor_de_fundo. Se a cor_de_fundo for null e extensão igual a .gif ou .png,
     * deve-se deixar transparente, se .jpg deve-se colocar branco.
     * A imagem criada possui a imagem original redimencionada da seguinte forma:
     * = não pode-se perder o aspecto ratio da imagem.
     * = a largura é igual a largura_destino e altura menor que altura_destino.
     * = a largura é menor que a largura_destino e altura igual a altura_destino.
     */

    function redimensionarSemRecortarComFaixa(
    $file_name_origem
    , $lugar_destino
    , $nome_destino
    , $extensao_destino
    , $largura_destino, $altura_destino
    ) {

        //cria imagem de fundo
        $imagem_de_fundo = imagecreatetruecolor($largura_destino, $altura_destino);
        $cor = imagecolorallocate($imagem_de_fundo, 255, 255, 255);
        imagefill($imagem_de_fundo, 0, 0, $cor);

        //descobre largura e altura originais
        $info_arquivo = getimagesize($file_name_origem);
        $largura_original = $info_arquivo[0];
        $altura_original = $info_arquivo[1];

        //cria imagem temporaria
        $imagem_temporaria = $this->carrega_file_name($file_name_origem);
        imagealphablending($imagem_temporaria, false);
        imagesavealpha($imagem_temporaria, true);

        //primeira tentativa: a largura_temporaria é igual a largura_destino
        //e altura_temporaria menor que altura_destino
        $porcentagem = round((100 * $largura_destino) / $largura_original);
        $largura_temporaria = $largura_destino;
        $altura_temporaria = round(($altura_original * $porcentagem) / 100);
        $area_de_recorte_horizontal_da_esquerda = 0;
        $altura_temporaria - $altura_destino;
        $area_de_recorte_vertical_de_cima = round(((($altura_temporaria - $altura_destino) * 100) / $porcentagem) / 2);
        if ($altura_temporaria > $altura_destino) {

            //segunda tentativa: a largura_temporaria é menor que largura_destino
            //e altura_temporaria igual a altura_destino
            $porcentagem = round((100 * $altura_destino) / $altura_original);
            $altura_temporaria = $altura_destino;
            $largura_temporaria = round(($largura_original * $porcentagem) / 100);
            $area_de_recorte_horizontal_da_esquerda = round(((($largura_temporaria - $largura_destino) * 100) / $porcentagem) / 2);
            $area_de_recorte_vertical_de_cima = 0;
        }

        //redimenciona
        imagecopyresampled(
                $imagem_de_fundo
                , $imagem_temporaria
                , round(($largura_destino - $largura_temporaria) / 2)
                , round(($altura_destino - $altura_temporaria) / 2)
                , 0, 0
                , $largura_temporaria, $altura_temporaria
                , $largura_original, $altura_original
        );

        //salva nova imagem
        switch ($extensao_destino) {
            case ".jpg":
                imagejpeg($imagem_de_fundo, $lugar_destino . "/" . $nome_destino . $extensao_destino, 100);
                break;
            case ".png":
                imagepng($imagem_de_fundo, $lugar_destino . "/" . $nome_destino . $extensao_destino);
                break;
            default:
                imagegif($imagem_de_fundo, $lugar_destino . "/" . $nome_destino . $extensao_destino);
                break;
        }
    }

}

?>