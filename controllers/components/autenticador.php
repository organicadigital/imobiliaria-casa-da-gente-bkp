<?php
class AutenticadorComponent extends Object {

    /*
     * Este componet é utilizado no método beforeFilter de todos os controllers
     * que necessitam que o usuário esteja logado. Caso o usuário não esteja
     * logado ou tente acessar um nível de acesso superior ao seu, o usuário é
     * redirecionado para o login, que por sua vez tem outras ações.
     *
     * Os possíveis níveis de acesso são:
     * 1 = libera se é Gramado
     * 2 = libera se é Canela
     * null = libera para qualquer um que esteja logado
     */

    function initialize($controller, $settings = array()){
        $this->controller = $controller;
        $this->Cidade = ClassRegistry::init('Cidade');
    }

    function autentica($nivel_de_acesso_requerido=null){

        $logado = $this->controller->Session->read('Usuario.logado');
        $nivel_de_acesso = $this->controller->Session->read('Usuario.nivel_de_acesso');
        if($logado){

            if( ( $nivel_de_acesso_requerido !=  $nivel_de_acesso ) && ( $nivel_de_acesso_requerido != null ) ){
                $this->controller->Session->destroy();
                $this->controller->redirect('/adm');
            }
            $this->controller->set("usuario", $this->controller->Session->read("Usuario"));
            $this->controller->set("nivel_de_acesso", $nivel_de_acesso);
            $resultado = $this->Cidade->get($nivel_de_acesso);
            $this->controller->set("cidade_acesso", $resultado[0]["cidades"]["nome"]);
            $this->controller->set("permissao", $this->controller->Session->read('Usuario.permissao'));
        }else{
            $this->controller->redirect('/adm/login');
        }
        $this->controller->set('logado', $logado);

    }
}
?>