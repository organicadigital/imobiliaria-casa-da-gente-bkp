<?php
App::import('Sanitize');
class ImoveisController extends AppController {
    
    var $name = 'Imoveis';
    var $uses = array("Cidade", "Imovel", "Endereco");
    var $helpers = array("Text");

    function beforeFilter() {
        $this->layout = "internas";
        $this->set("enderecos", $this->Endereco->getAll());
    }

    function index($cidade=null, $tipo=null, $extra=null, $limit=null, $offset=null) {
        
        $cidade = Sanitize::escape($cidade);
        $tipo = Sanitize::escape($tipo);
        $extra = Sanitize::escape($extra);
        $limit = Sanitize::escape($limit);
        $offset = Sanitize::escape($offset);
        $this->set("cidade", $cidade);
        $this->set("tipo", $tipo);
        $this->set("extra", $extra);
        $this->set("limit", $limit);
        $this->set("offset", $offset);
        $tipo = utf8_decode($tipo);

        //geral
        $cidades2 = $this->Cidade->getAll2();
        $this->set("cidades2", $cidades2);
        $cidades = $this->Cidade->getAll();
        $this->set("cidades", $cidades);
        $this->set("tipos", $this->Imovel->getTipos());
        $tipos_cidade = array();
        foreach($cidades2 as $c){
            $tipos = $this->Cidade->getTipos($c["tb_imoveis"]["nome"]);
            foreach($tipos as $t){
                $tipos_cidade[$c["tb_imoveis"]["nome"]][] = $t;
            }
        }
        $this->set("tipos_cidade", $tipos_cidade);
        
        //imoveis
        $titulo = "Imóveis de Canela e Gramado";
        if($extra!="null"){
            $ex = "Ofertas de ";
            if($extra=="lancamentos"){
                $ex = "Lançamentos de ";
            }
            if($extra=="concominios"){
                $ex = "Condomínios de ";
            }
            $titulo = $ex.$tipo." em ".$cidade;
        }
        //consultas complexas ao banco
        if($extra!="null"){
            if($extra=="lancamentos"){
                $imoveis = $this->Imovel->getByCidadeTipoExtraLancamentos($cidade, $tipo, $limit, $offset);
                $paginas = $this->Imovel->getQtdByCidadeTipoExtraLancamentos($cidade, $tipo, $limit);
            }else{
                $imoveis = $this->Imovel->getByCidadeTipoExtraOfertas($cidade, $tipo, $limit, $offset);
                $paginas = $this->Imovel->getQtdByCidadeTipoExtraOfertas($cidade, $tipo, $limit);
            }
        }elseif($tipo!="null"){
            if($tipo=="lancamentos"){
                $titulo = "Lançamentos em ".$cidade;
                $imoveis = $this->Imovel->getByCidadeExtraLancamentos($cidade, $limit, $offset);
                $paginas = $this->Imovel->getQtdByCidadeExtraLancamentos($cidade, $limit);
            }elseif($tipo=="ofertas"){
                $titulo = "Ofertas em ".$cidade;
                $imoveis = $this->Imovel->getByCidadeExtraOfertas($cidade, $limit, $offset);
                $paginas = $this->Imovel->getQtdByCidadeExtraOfertas($cidade, $limit);
            }elseif($tipo=="condominios"){
                if($cidade != "null"){
                    $titulo = "Condomínios em ".$cidade;
                }else{
                    $titulo = "Condomínios";
                }
                $imoveis = $this->Imovel->getByCidadeExtraCondominios($cidade, $limit, $offset);
                $paginas = $this->Imovel->getQtdByCidadeExtraCondominios($cidade, $limit);
            }elseif($tipo=="obras_concluidas"){
                if($cidade != "null"){
                    $titulo = "Obras Concluídas em ".$cidade;
                }else{
                    $titulo = "Obras Concluídas";
                }
                $imoveis = $this->Imovel->getByCidadeExtraObrasConcluidas($cidade, $limit, $offset);
                $paginas = $this->Imovel->getQtdByCidadeExtraObrasConcluidas($cidade, $limit);
            }else{
                $imoveis = $this->Imovel->getByCidadeTipo($cidade, $tipo, $limit, $offset);
                $paginas = $this->Imovel->getQtdByCidadeTipo($cidade, $tipo, $limit);
            }
        }elseif($cidade!="null"){
            $imoveis = $this->Imovel->getByCidade($cidade, $limit, $offset);
            $paginas = $this->Imovel->getQtdByCidade($cidade, $limit);
        }else{
            $imoveis = $this->Imovel->getAll3($limit, $offset);
            $paginas = $this->Imovel->getQtdAll3($limit);
        }
        
        $this->set("titulo", $titulo);
        $this->set("imoveis", $imoveis);
        $this->set("totalPaginas", ceil($paginas[0][0]["qtd"]));
        $this->set("paginaAtual", ($offset/$limit)+1);
        
    }
}

?>