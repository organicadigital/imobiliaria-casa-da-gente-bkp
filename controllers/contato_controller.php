<?php
App::import('Sanitize');
class ContatoController extends AppController {
    
    var $name = 'Contato';
    var $uses = array("Cidade", "Imovel", "Endereco", "Corretor");
    var $helpers = array("Text");
    var $components = array('Validador', 'Email');

    function beforeFilter() {
        $this->layout = "internas";
        $this->set("enderecos", $this->Endereco->getAll());
    }

    function index($imovel="") {

        $imovel = Sanitize::escape($imovel);
        $this->set("titulo", "Contato");
        $this->set("css", "contato");
        $this->set("javascript", "contato");
        $this->set("imovel", $imovel);
        //geral
        $cidades2 = $this->Cidade->getAll2();
        $this->set("cidades2", $cidades2);
        $cidades = $this->Cidade->getAll();
        $this->set("cidades", $cidades);
        $this->set("tipos", $this->Imovel->getTipos());
        $tipos_cidade = array();
        foreach($cidades2 as $c){
            $tipos = $this->Cidade->getTipos($c["tb_imoveis"]["nome"]);
            foreach($tipos as $t){
                $tipos_cidade[$c["tb_imoveis"]["nome"]][] = $t;
            }
        }
        $this->set("tipos_cidade", $tipos_cidade);
        $this->set("corretores", $this->Corretor->getAll());
    }
    
    function processaEnviaContato(){
        
        $this->layout = "ajax";
        $eh_valido = true;
        if($this->data['Contato']['nome'] ==""){
            $eh_valido = false;
            $erro="Nome é um campo obrigatório.";
        }
        if(!$this->Validador->emailValido($this->data["Contato"]["email"])){
            $eh_valido = false;
            $erro="Preencha um e-mail válido.";
        }
        if($this->data['Contato']['telefone'] ==""){
            $eh_valido = false;
            $erro="Telefone é um campo obrigatório.";
        }
        if($this->data['Contato']['mensagem'] ==""){
            $eh_valido = false;
            $erro="Mensagem é um campo obrigatório.";
        }
        if($eh_valido){
            $codigo = $this->data['Contato']['codigo'];
            $this->set("nome", $this->data['Contato']['nome']);
            $this->set("email", $this->data['Contato']['email']);
            $this->set("telefone", $this->data['Contato']['telefone']);
            $this->set("mensagem", $this->data['Contato']['mensagem']);
            $this->set("imovel", $codigo);
            
            $cc = array();
            if ($codigo) {
            	$imovelAtual = $this->Imovel->getImovelPorCodigo($codigo);
            	if ($imovelAtual[0]["cidade"]["emails"] != "") {
	            	$emails = explode(",", $imovelAtual[0]["cidade"]["emails"]);
	            	foreach($emails as $email){
	            		array_push($cc ,$email);
	            	} 
            	}
            }
            

            $para = "gramado@imobiliariacasadagente.com.br";
            $this->Email->to = $para;
            //$this->Email->cc = $cc;
            $this->Email->subject = 'Contato - Site';
            $this->Email->from = "site@imobiliariacasadagente.com.br";
            $this->Email->replyTo = "gramado@imobiliariacasadagente.com.br";
            $this->Email->template = 'envia_contato';
            $this->Email->sendAs = 'html';
            $this->Email->smtpOptions = array(
                'port' => '587',
                'timeout' => '15',
                'host' => 'mail.imobiliariacasadagente.com.br',
                'username' => 'site@imobiliariacasadagente.com.br',
                'password' => 's1tecdg32955555'
            );
            $this->Email->delivery = 'smtp';
            $this->Email->send();

        }else {
          $this->set("erro", $erro);
        }
        $this->set("eh_valido", $eh_valido);
    }
    
}
?>