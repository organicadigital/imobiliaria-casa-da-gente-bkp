<?php
class InstitucionalController extends AppController {
    
    var $name = 'Institucional';
    var $uses = array("Cidade", "Imovel", "Endereco");
    var $helpers = array("Text");

    function beforeFilter() {
        $this->layout = "internas";
        $this->set("enderecos", $this->Endereco->getAll());
    }

    function index() {

        $this->set("titulo", "Institucional");
        $this->set("css", "institucional");
        //geral
        $cidades2 = $this->Cidade->getAll2();
        $this->set("cidades2", $cidades2);
        $cidades = $this->Cidade->getAll();
        $this->set("cidades", $cidades);
        $this->set("tipos", $this->Imovel->getTipos());
        $tipos_cidade = array();
        foreach($cidades2 as $c){
            $tipos = $this->Cidade->getTipos($c["tb_imoveis"]["nome"]);
            foreach($tipos as $t){
                $tipos_cidade[$c["tb_imoveis"]["nome"]][] = $t;
            }
        }
        $this->set("tipos_cidade", $tipos_cidade);
        
    }
    
}
?>