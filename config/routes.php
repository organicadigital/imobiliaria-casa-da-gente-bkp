<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/views/pages/home.ctp)...
 */
Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
Router::connect('/images', array('controller' => 'images', 'action' => 'index'));
Router::connect('/images/*', array('controller' => 'images', 'action' => 'index'));
Router::connect('/teste', array('controller' => 'imovel', 'action' => 'test'));
Router::connect(
        '/imovel/:urlsemchamada/:identificador/:ehcondominio/:ehobraconcluida/:filial'
        , array(
    "controller" => "imovel"
    , "action" => "index"
        )
        , array(
    "pass" => array("urlsemchamada", "identificador", "ehcondominio", "ehobraconcluida", "filial")
        )
);
Router::connect(
        '/imoveis/:cidade-:tipo-:extra/:limit/:offset'
        , array(
    "controller" => "imoveis"
    , "action" => "index"
        )
        , array(
    "pass" => array("cidade", "tipo", "extra", "limit", "offset")
        )
);
Router::connect(
        '/lancamentos/:limit/:offset'
        , array(
    "controller" => "lancamentos"
    , "action" => "index"
        )
        , array(
    "pass" => array("limit", "offset")
        )
);
/*
Router::connect(
        '/localizacao'
        , array(
    "controller" => "localizacao"
    , "action" => "index"
        )
        , array(
    "pass" => array("cidade", "tipo", "extra", "limit", "offset")
        )
);
*/