<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title><?php echo $titulo; ?> | Imobiliária em Gramado e Canela. Imóveis, Casas, Terrenos, Apartamentos, Salas Comerciais e muito mais.</title>
        <meta name="description" content="Imobiliária em Gramado e Canela. Imóveis, Casas, Terrenos, Apartamentos, Salas Comerciais e muito mais." />
        <meta name="keywords" content="imobiliaria, gramado, canela, imóveis, rs, serra gaucha, terrenos, apartamento, apartamentos, lago negro, região hortensias, hortencias, natal luz, festa colonial, festival de cinema, férias, julho, inverno, neve, quarto, casa geminada, próximo, rua coberta, carrieri, locação, venda, preço, baixo, oportunidade, investimento, promocao, promocoes, desconto, verao, janeiro, nova petropolis, ferias, lojas, salas, comercial, negócios" />
        <link rel="shortcut icon" href="<?php echo $this->webroot; ?>favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/reset.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/geral1.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/internas1.css">
        <?php if(isset($css)){ ?>
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/<?php echo $css; ?>.css">
        <?php } ?>
        <!--[if IE]>
            <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/ie.css">
        <![endif]-->
        <!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/ie7.css">
        <![endif]-->
        <!--[if IE 8]>
            <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/ie8.css">
        <![endif]-->
        <script src="<?php echo $this->webroot; ?>js/modernizr.js"></script>
        <script src="<?php echo $this->webroot; ?>js/innershiv.js"></script>
        <script src="<?php echo $this->webroot; ?>js/jquery.js"></script>
        <script>
            var urlRoot = "<?php echo $this->webroot; ?>";
        </script>
        <script src="<?php echo $this->webroot; ?>js/geral1.js"></script>
        <?php if(isset($javascript)){ ?>
        <script src="<?php echo $this->webroot; ?>js/<?php echo $javascript; ?>.js"></script>
        <?php } ?>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-75434081-1', 'auto');
          ga('send', 'pageview');

        </script>
</script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//cdn.zopim.com/?1CogBQfTjGFtI7DkgzcrzhzuyTf5ymSn';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
    </head>
    <body>
        <div class="pagina">
            <header id="header_principal">
                <?php if ($_SERVER['REQUEST_URI'] == '/'){ ?>
                    <h1 class="logo">
                        <img src="<?php echo $this->webroot; ?>img/casa_da_gente_logo.png" alt="Imobiliária Casa da Gente" />
                    </h1>
                <?php } else { ?>
                    <h1 class="logo">
                        <a href="/">
                            <img src="<?php echo $this->webroot; ?>img/casa_da_gente_logo.png" alt="Imobiliária Casa da Gente" />
                        </a>
                    </h1>                    
                <?php } ?>
                <div class="logodezanos">
                    &nbsp;
                </div> 
                <h1 class="telefones" style="padding-top:10px">
                    <img src="<?php echo $this->webroot; ?>img/phone-gramado.png" alt="telefones" />
                </h1>
              </h1>
                
                <?php echo $this->element("menus"); ?>
            </header>
        </div>
        <div id="conteudo_principal">
            <?php echo $content_for_layout ?>
        </div>
        <?php echo $this->element("rodape"); ?>
        <img src="img/submenu.png" alt="submenu" class="oculto" />
    </body>
</html>