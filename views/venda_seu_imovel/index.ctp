<h1  class="titulo">
    <span class="pagina">
        <span class="texto">Venda seu Imóvel</span>
        <span class="linha_h2 tamanho6"></span>
    </span>
</h1>
<span class="divisoria1">
    <span class="divisoria2"></span>
</span>
<?php echo $this->element("busca_interna", array("fundo_f2f2f2"=>1)); ?>
<span class="divisoria1 sem_espaco">
    <span class="divisoria2"></span>
</span>
<section id="conteudo_contato" class="fundo_e6e6e6">
    <div class="pagina">
        <div class="topo">
            <span id="linha_esquerda"></span>
            <span id="linha_direita"></span>
            <span id="arabesco"></span>
        </div>
        <p class="titulo_formulario">
            Nós da Casa da Gente, teremos o prazer de vender o seu imóvel. Preencha o cadastro e nos envie.
        </p>
        <form action="javascript:submit_venda_seu_imovel()" method="post" id="formulario_contato">
            <div class="linha_formulario">
                <label for="nome_text">Nome:</label>
                <input type="text" name="data[Venda][nome]" id="nome_text" class="tamanho1" />
            </div>
            <div class="linha_formulario">
                <label for="telefone_text">Telefone:</label>
                <label for="ddd_text" class="oculto">DDD</label>
                <input type="text" name="data[Venda][ddd]" id="ddd_text" class="tamanho2" />
                <input type="text" name="data[Venda][telefone]" id="telefone_text" class="tamanho3" />
            </div>
            <div class="linha_formulario">
                <label for="email_text">E-mail:</label>
                <input type="text" name="data[Venda][email]" id="email_text" class="tamanho4" />
            </div>
            <div class="linha_formulario">
                <label for="tipo_text">Tipo:</label>
                <input type="text" name="data[Venda][tipo]" id="tipo_text" class="tamanho4" />
            </div>
            <div class="linha_formulario">
                <label>Cidade:</label>
                <div class="busca">
                    <label for="cidade_select" class="formatacao2">cidade</label>
                    <select id="cidade_select" name="data[Venda][cidade]" size="3">
                        <option value="cidade" selected="selected">cidade</option>
                        <option value="Gramado">Gramado</option>
                        <option value="Canela">Canela</option>
                    </select>
                </div>
            </div>
            <div class="linha_formulario">
                <label for="btn_contato" class="btn_contato">
                    <input type="submit" value="enviar" id="btn_contato" />
                </label>
            </div>
        </form>
    </div>
</section>