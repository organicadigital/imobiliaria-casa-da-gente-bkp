<div id="container_interno" class="recuo1">
    <h1>Obras Concluídas</h1>
    <div class="recuo2">
        <a href="<?php echo $this->webroot; ?>adm/obrasConcluidasCadastrar" title="CADASTRAR" class="botao botao_cadastrar">CADASTRAR</a>
        <?php if(count($obras_concluidas)>0){ ?>
        <table>
            <thead>
                <tr>
                    <th>Título</th>
                    <th colspan="3">Ações</th>
                </tr>
            </thead>
            <tbody>
               <?php
                    $i = 0;
                    $i2 = 0;
                    foreach($obras_concluidas as $c){
                        $i++;
                        if($c["obras_concluidas"]["cidade_id"]==$nivel_de_acesso){
                        $i2++;
                        if($i2%2){
                ?>
                <tr class="listra">
                        <?php }else{ ?>
                <tr>
                        <?php } ?>
                    <td><?php echo $c["obras_concluidas"]["titulo"]; ?></td>
                    <td>
                        <a href="<?php echo $this->webroot; ?>adm/obrasConcluidasEditar/<?php echo $c["obras_concluidas"]["id"]; ?>" title="Editar">Editar</a>
                    </td>
                    <td>
                        <a href="<?php echo $this->webroot; ?>adm/obrasConcluidasEditarFotos/<?php echo $c["obras_concluidas"]["id"]; ?>" title="Editar Fotos">Editar Fotos</a>
                    </td>
                    <td>
                        <a href="<?php echo $this->webroot; ?>adm/obrasConcluidasExcluir/<?php echo $c["obras_concluidas"]["id"]; ?>" title="Excluir">Excluir</a>
                    </td>
                </tr>
                <?php }} ?>
            </tbody>
        </table>
        <?php } ?>
    </div>
</div>