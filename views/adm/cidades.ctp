<div id="container_interno" class="recuo1">
    <h1 style="clear:both;">Cidades</h1>
    <div class="recuo2">
	    <a href="<?php echo $this->webroot; ?>adm/cidadesCadastrar" title="CADASTRAR" class="botao botao_cadastrar">CADASTRAR</a>
        <table>
            <thead>
                <tr>
                    <th>Cidade</th>
                    <th>E-mails</th>
                    <th>Litoral?</th>
                    <th colspan="2">Ações</th>
                </tr>
            </thead>
            <tbody>
            	<?php
            	$i = 0;
				foreach($cidades as $cidade){
					?>
					<tr<?php echo $i % 2 == 0 ? ' class="listra"' : '';?>>
	                    <td><?php echo $cidade["cidades"]["nome"]; ?></td>
	                    <td><?php echo $cidade["cidades"]["emails"]; ?></td>
	                    <td>
	                    	<a href="javascript:cidadeDoLitoral(<?php echo $cidade["cidades"]["id"]; ?>, <?php echo $cidade["cidades"]["litoral"] == 0 ? 1 : 0; ?>)"><?php echo $cidade["cidades"]["litoral"] == 0 ? 'Não' : 'Sim'; ?></a>
	                    </td>
	                    <td>
	                        <a href="<?php echo $this->webroot; ?>adm/cidadesEditar/<?php echo $cidade["cidades"]["id"]; ?>" title="Editar">Editar</a>
	                    </td>
	                    <td>
	                        <a href="javascript:confirmarExclusaoCidade(<?php echo $cidade["cidades"]["id"]; ?>)" title="Excluir">Excluir</a>
	                    </td>
	                </tr>
					<?php
					$i++;
				};
				?>
            </tbody>
		</table>
    </div>
</div>