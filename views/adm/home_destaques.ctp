<div id="container_interno" class="recuo1">
    <h1>Home - Destaques</h1>
    <div class="recuo2">
        <a href="javascript:removerTodosDestaques()" class="botao botao_remover_todos">Remover todos destaques</a>
        <?php if(count($destaques)>0){ ?>
        <table>
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Colocar na Home</th>
                </tr>
            </thead>
            <tbody>
               <?php
                    $i = 0;
                    $i2 = 0;
                    foreach($destaques as $d){
                        $i++;
                        if($d["cidade"]==$cidade_acesso){
                        $i2++;
                        if($i2%2){
                ?>
                <tr class="listra">
                        <?php }else{ ?>
                <tr>
                        <?php } ?>
                    <td><?php echo $d["titulo"]; ?></td>
                    <td>
                        <a href="javascript:updateHomeDestaque(<?php echo $d["id"]; ?>, <?php if($d["eh_condominio"]){echo "1";}else{echo "0";} ?>, <?php if($d["eh_destaque"]){echo "1";}else{echo "0";} ?>, '<?php echo $d["filial"]; ?>')" title="Atualizar">
                            <?php if($d["eh_destaque"]){echo "Sim";}else{echo "Não";} ?>
                        </a>
                    </td>
                </tr>
                <?php }} ?>
            </tbody>
        </table>
        <?php } ?>
    </div>
</div>