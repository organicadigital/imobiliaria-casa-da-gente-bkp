<div id="container_interno" class="recuo1">
    <h1>Usuários</h1>
    <div class="recuo2">
        <a href="<?php echo $this->webroot; ?>adm/usuariosCadastrar" title="CADASTRAR" class="botao botao_cadastrar">CADASTRAR</a>
        <?php if(count($usuarios)>0){ ?>
	        <table>
	            <thead>
	                <tr>
	                    <th>Login</th>
	                    <th>Permiss&atilde;o</th>
	                    <th colspan="2">Ações</th>
	                </tr>
	            </thead>
	            <tbody>
	            	<?php
	            	$i = 0;
					foreach($usuarios as $usuario){
						?>
						<tr<?php echo $i % 2 == 0 ? ' class="listra"' : '';?>>
		                    <td><?php echo $usuario["usuarios"]["login"]; ?></td>
		                    <td><?php echo $usuario["usuarios"]["permissao"]; ?></td>
		                    <td>
		                        <a href="<?php echo $this->webroot; ?>adm/usuariosEditar/<?php echo $usuario["usuarios"]["id"]; ?>" title="Editar">Editar</a>
		                    </td>
		                    <td>
		                        <a href="javascript:confirmarExclusaoUsuario(<?php echo $usuario["usuarios"]["id"]; ?>)" title="Excluir">Excluir</a>
		                    </td>
		                </tr>
						<?php
						$i++;
					};
					?>
	            </tbody>
	        </table>
        <?php } ?>
    </div>
</div>