<div id="container_interno" class="recuo1">
    <h1>Corretores</h1>
    <div class="recuo2">
        <a href="<?php echo $this->webroot; ?>adm/lojasCorretoresCadastrar" title="CADASTRAR" class="botao botao_cadastrar">CADASTRAR</a>
        <?php if(count($corretores)>0){ ?>
        <table>
            <thead>
                <tr>
                    <th>Filial</th>
                    <th>Texto</th>
                    <th colspan="2">Ações</th>
                </tr>
            </thead>
            <tbody>
               <?php
                    $i = 0;
                    $i2 = 0;
                    foreach($corretores as $c){
                        $i++;
                        if($i%2){
                ?>
                <tr class="listra">
                        <?php }else{ ?>
                <tr>
                        <?php } ?>
                    <td><?php echo $c["cidade"]["nome"]; ?></td>
                    <td><?php echo $text->truncate($c["corretores"]["texto"], 30, array("ending"=>"...", "exact" => false)); ?></td>
                    <td>
                        <a href="<?php echo $this->webroot; ?>adm/lojasCorretoresEditar/<?php echo $c["corretores"]["id"]; ?>" title="Editar">Editar</a>
                    </td>
                    <td>
                        <a href="<?php echo $this->webroot; ?>adm/lojasCorretoresExcluir/<?php echo $c["corretores"]["id"]; ?>" title="Excluir">Excluir</a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php } ?>
    </div>
</div>