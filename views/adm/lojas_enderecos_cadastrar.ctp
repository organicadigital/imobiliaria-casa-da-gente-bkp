<div id="container_interno" class="recuo1">
    <h1>Lojas Endereços - Cadastrar</h1>
    <form action="javascript:lojasEnderecoCadastrar()" class="recuo2" method="post">
    	<div class="linha_formulario">
            <select name="data[Endereco][cidade_id]" id="cidade_combo">
            	<option value="">Cidade</option>
            	<?php
				foreach($cidades as $cidade){
					?>
					<option value="<?php echo $cidade["cidades"]["id"]; ?>"><?php echo $cidade["cidades"]["nome"]; ?></option>
					<?php
				};
				?>
            	
            </select>
        </div>
        <div class="linha_formulario">
        	<label for="endereco_text">Endereço</label>
            <input type="text" name="data[Endereco][endereco]" id="endereco_text" maxlength="255" />
        </div>
        <div class="linha_formulario">
        	<label for="telefone_text">Telefone</label>
            <input type="text" name="data[Endereco][telefone]" id="telefone_text" maxlength="255" />
        </div>
        <div class="linha_formulario">
        	<label for="cep_text">Cep</label>
            <input type="text" name="data[Endereco][cep]" id="cep_text" maxlength="255" />
        </div>
        <div class="linha_formulario">
            <a href="javascript:lojasEnderecoCadastrar()" title="SALVAR" class="botao botao_salvar alinha_esquerda">SALVAR</a>
            <a href="<?php echo $this->webroot; ?>adm/lojasEnderecos" title="CANCELAR" class="botao botao_cancelar alinha_esquerda">CANCELAR</a>
        </div>
    </form>
</div>