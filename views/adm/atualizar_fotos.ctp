<div id="container_interno" class="recuo1">
    <h1>Atualizar Fotos</h1>
    <div class="recuo2">
        <?php if(count($fotos)>0){ ?>
        <table>
            <thead>
                <tr>
                    <th>Código Imóvel</th>
                    <th>Código Foto</th>
                    <th colspan="3">Atualizar</th>
                </tr>
            </thead>
            <tbody>
               <?php
                    $i = 0;
                    foreach($fotos as $f){
                        $i++;
                        if(
                            file_exists("img/imoveis/".$f["tb_imoveis_fotos"]["f_id_foto"]."_1.jpg")
                            and file_exists("img/imoveis/".$f["tb_imoveis_fotos"]["f_id_foto"]."_2.jpg")
                            and file_exists("img/imoveis/".$f["tb_imoveis_fotos"]["f_id_foto"]."_3.jpg")
                        ){
                            if($i%2){
                ?>
                <tr class="listra">
                            <?php }else{ ?>
                <tr>
                            <?php } ?>
                            <?php }else{?>
                <tr class="red">
                            <?php } ?>    
                    <td><?php echo $f["tb_imoveis_fotos"]["f_codigo"]; ?></td>
                    <td><?php echo $f["tb_imoveis_fotos"]["f_id_foto"]; ?></td>
                    <td>
                        <a href="javascript:atualizar_foto('<?php echo $f["tb_imoveis_fotos"]["f_id_foto"]; ?>')" title="Atualizar">Atualizar</a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php } ?>
    </div>
</div>