<div id="container_interno" class="recuo1">
    <h1>Imóveis de Luxo</h1>
    <div class="recuo2">
        <?php if(count($imoveis)>0){ ?>
        <table>
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Título</th>
                    <th>É um imóvel de luxo?</th>
                </tr>
            </thead>
            <tbody>
               <?php
                    $i = 0;
                    $i2 = 0;
                    foreach($imoveis as $im){
                        $i++;
                        if($im["tb_imoveis"]["f_cidade"]==$cidade_acesso){
                        $i2++;
                        $informacoes = explode("!", $im["tb_imoveis"]["f_descricao"], 2);
                        $titulo = trim(utf8_encode($informacoes[0]));
                        $descricao = trim(utf8_encode($informacoes[1]));
                        if($i2%2){
                ?>
                <tr class="listra">
                        <?php }else{ ?>
                <tr>
                        <?php } ?>
                    <td><?php echo $im["tb_imoveis"]["f_codigo"]; ?></td>
                    <td><?php echo $titulo; ?></td>
                    <td>
                        <a href="javascript:updateImoveisDeLuxo(<?php echo $im["tb_imoveis"]["f_id"]; ?>, <?php if($im[0]["eh_luxo"]){echo "1";}else{echo "0";} ?>, '<?php echo $im["tb_imoveis"]["f_filial"]; ?>')" title="Atualizar">
                            <?php if($im[0]["eh_luxo"]){echo "Sim";}else{echo "Não";} ?>
                        </a>
                    </td>
                </tr>
                <?php }} ?>
            </tbody>
        </table>
        <?php } ?>
    </div>
</div>