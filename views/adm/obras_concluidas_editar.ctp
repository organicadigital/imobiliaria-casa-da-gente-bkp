<script>
$(document).ready(function(){
    <?php if($obra_concluida["0"]["obras_concluidas"]["titulo"]!=""){ ?>
    $("label[for=titulo_text]").hide();
    <?php } ?>
    <?php if($obra_concluida["0"]["obras_concluidas"]["descricao"]!=""){ ?>
    $("label[for=descricao_textarea]").hide();
    <?php } ?>
    <?php if($obra_concluida["0"]["obras_concluidas"]["imoveis"]!=""){ ?>
    $("label[for=imoveis_textarea]").hide();
    <?php } ?>
});
</script>
<div id="container_interno" class="recuo1">
    <h1>Obras Concluídas - Editar</h1>
    <form action="javascript:obras_concluidas_editar()" class="recuo2" method="post">
        <div class="linha_formulario">
            <label for="titulo_text">título</label>
            <input type="text" name="data[ObraConcluida][titulo]" id="titulo_text" maxlength="255" value="<?php echo $obra_concluida[0]["obras_concluidas"]["titulo"]; ?>" class="edit" />
        </div>
        <div class="linha_formulario">
            <label for="descricao_textarea">descrição</label>
            <textarea name="data[ObraConcluida][descricao]" id="descricao_textarea" cols="1" rows="1"  class="edit"><?php echo $obra_concluida[0]["obras_concluidas"]["descricao"]; ?></textarea>
        </div>
        <div class="linha_formulario">
            <label for="imoveis_textarea">imóveis - separar por vírgulas</label>
            <textarea name="data[ObraConcluida][imoveis]" id="imoveis_textarea" cols="1" rows="1"  class="edit"><?php echo $obra_concluida[0]["obras_concluidas"]["imoveis"]; ?></textarea>
        </div>
        <div class="linha_formulario">
            <div class="linha_formulario2">
                <?php if($obra_concluida[0]["obras_concluidas"]["lancamento"]){ ?>
                <input type="checkbox" name="data[ObraConcluida][lancamento_checkbox]" id="lancamento_checkbox" checked="checked" />
                <?php }else{ ?>
                <input type="checkbox" name="data[ObraConcluida][lancamento_checkbox]" id="lancamento_checkbox" />
                <?php } ?>
                <label for="lancamento_checkbox" class="reset">Lançamento</label>
            </div>
            <div class="linha_formulario2">
                <?php if($obra_concluida[0]["obras_concluidas"]["destaque"]){ ?>
                <input type="checkbox" name="data[ObraConcluida][destaque_checkbox]" id="destaque_checkbox" checked="checked" />
                <?php }else{ ?>
                <input type="checkbox" name="data[ObraConcluida][destaque_checkbox]" id="destaque_checkbox" />
                <?php } ?>
                <label for="destaque_checkbox" class="reset">Destaque</label>
            </div>
        </div>
        <div class="linha_formulario">
            <input type="hidden" name="data[ObraConcluida][id]" id="obra_concluida_id" value="<?php echo $obra_concluida["0"]["obras_concluidas"]["id"]; ?>" />
            <a href="javascript:obras_concluidas_editar()" title="SALVAR" class="botao botao_salvar alinha_esquerda">SALVAR</a>
        </div>
    </form>
</div>