<script>
$(document).ready(function(){
    <?php if($endereco[0]["enderecos"]["cidade"]!=""){ ?>
    $("label[for=cidade_text]").hide();
    <?php } ?>
    <?php if($endereco[0]["enderecos"]["endereco"]!=""){ ?>
    $("label[for=endereco_text]").hide();
    <?php } ?>
    <?php if($endereco[0]["enderecos"]["telefone"]!=""){ ?>
    $("label[for=telefone_text]").hide();
    <?php } ?>
    <?php if($endereco[0]["enderecos"]["cep"]!=""){ ?>
    $("label[for=cep_text]").hide();
    <?php } ?>
});
</script>
<div id="container_interno" class="recuo1">
    <h1>Lojas Endereços - Editar</h1>
    <form action="javascript:lojasEnderecoEditar()" class="recuo2" method="post">
    	<div class="linha_formulario">
            <select name="data[Endereco][cidade_id]" id="cidade_combo">
            	<option value="">Cidade</option>
            	<?php
				foreach($cidades as $cidade){
					?>
					<option value="<?php echo $cidade["cidades"]["id"]; ?>" <?php echo $endereco[0]["enderecos"]["cidade_id"] == $cidade["cidades"]["id"] ? 'selected="selected"' : ''; ?>><?php echo $cidade["cidades"]["nome"]; ?></option>
					<?php
				};
				?>
            	
            </select>
        </div>
        <div class="linha_formulario">
        	<label for="endereco_text">Endereço</label>
            <input type="text" name="data[Endereco][endereco]" id="endereco_text" maxlength="255" value="<?php echo $endereco[0]["enderecos"]["endereco"]; ?>" class="edit" />
        </div>
        <div class="linha_formulario">
        	<label for="telefone_text">Telefone</label>
            <input type="text" name="data[Endereco][telefone]" id="telefone_text" maxlength="255" value="<?php echo $endereco[0]["enderecos"]["telefone"]; ?>" class="edit" />
        </div>
        <div class="linha_formulario">
        	<label for="cep_text">Cep</label>
            <input type="text" name="data[Endereco][cep]" id="cep_text" maxlength="255" value="<?php echo $endereco[0]["enderecos"]["cep"]; ?>" class="edit" />
        </div>
        <div class="linha_formulario">
        	<input type="hidden" name="data[Endereco][id]" id="endereco_id" value="<?php echo $endereco["0"]["enderecos"]["id"]; ?>" />
            <a href="javascript:lojasEnderecoEditar()" title="SALVAR" class="botao botao_salvar alinha_esquerda">SALVAR</a>
            <a href="<?php echo $this->webroot; ?>adm/lojasEnderecos" title="CANCELAR" class="botao botao_cancelar alinha_esquerda">CANCELAR</a>
        </div>
    </form>
</div>