<div id="container_interno" class="recuo1">
    <h1>Home - Banners</h1>
    <div class="recuo2">
        <a href="<?php echo $this->webroot; ?>adm/homeBannersCadastrar" title="CADASTRAR" class="botao botao_cadastrar">CADASTRAR</a>
        <?php if(count($banners)>0){ ?>
        <table>
            <thead>
                <tr>
                    <th>Linha1</th>
                    <th colspan="2">Ações</th>
                </tr>
            </thead>
            <tbody>
               <?php
                    $i = 0;
                    foreach($banners as $b){
                        $i++;
                        if($i%2){
                ?>
                <tr class="listra">
                        <?php }else{ ?>
                <tr>
                        <?php } ?>
                    <td><?php echo $b["banners"]["linha1"]; ?></td>
                    <td>
                        <a href="<?php echo $this->webroot; ?>adm/homeBannersEditar/<?php echo $b["banners"]["id"]; ?>" title="Editar">Editar</a>
                    </td>
                    <td>
                        <a href="<?php echo $this->webroot; ?>adm/processaHomeBannersExcluir/<?php echo $b["banners"]["id"]; ?>" title="Excluir">Excluir</a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php } ?>
    </div>
</div>