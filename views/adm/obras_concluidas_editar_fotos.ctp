<div id="container_interno" class="recuo1">
    <h1>Obras Concluídas - Editar Fotos</h1>
    <form action="javascript:obras_concluidas_cadastrar_foto()" class="recuo2 espaco_superior" method="post">
        <div class="linha_formulario">
            <input type="file" name="data[Foto][arquivo]" id="foto_file" />
            <span class="obs">Imagem deve estar na extensão .jpg e com tamanho inferior a 1 Mb.</span>
        </div>
        <div class="linha_formulario">
            <label for="legenda_text">legenda</label>
            <input type="text" name="data[Foto][legenda]" id="legenda_text" maxlength="150" />
        </div>
        <div class="linha_formulario">
            <label for="ordem_text">ordem</label>
            <input type="text" name="data[Foto][ordem]" id="ordem_text" class="numerico" />
        </div>
        <div class="linha_formulario">
            <div class="linha_formulario2">
                <input type="checkbox" name="data[Foto][destaque_checkbox]" id="destaque_checkbox" />
                <label for="destaque_checkbox" class="reset">Destaque</label>
            </div>
        </div>
        <div class="linha_formulario">
            <input type="hidden" name="data[Foto][obra_concluida_id]" id="obra_concluida_id" value="<?php echo $obra_concluida_id; ?>" />
            <a href="javascript:obras_concluidas_cadastrar_foto()" title="ADICIONAR FOTO" class="botao botao_foto alinha_esquerda">ADICIONAR FOTO</a>
        </div>
    </form>
    <div class="recuo2">
        <?php if(count($fotos)>0){ ?>
        <table>
            <thead>
                <tr>
                    <th>Foto</th>
                    <th>Legenda</th>
                    <th>Ordem</th>
                    <th>Destaque</th>
                    <th colspan="2">Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 0;
                    foreach($fotos as $f){
                        $i++;
                        if($i%2){
                ?>
                <tr class="listra">
                        <?php }else{ ?>
                <tr>
                        <?php } ?>
                    <td>
                        <img src="<?php echo $this->webroot; ?>img/obras_concluidas/<?php echo $f["fotos_obras_concluidas"]["id"]; ?>_3.jpg" alt="foto" />
                    </td>
                    <td><?php echo $f["fotos_obras_concluidas"]["legenda"]; ?></td>
                    <td><?php echo $f["fotos_obras_concluidas"]["ordem"]; ?></td>
                    <td>
                        <?php if($f["fotos_obras_concluidas"]["destaque"]){echo "sim";}else{echo "não";} ?>
                    </td>
                    <td>
                        <a href="<?php echo $this->webroot; ?>adm/obrasConcluidasEditarFoto/<?php echo $f["fotos_obras_concluidas"]["id"]; ?>" title="Editar">Editar</a>
                    </td>
                    <td>
                        <a href="<?php echo $this->webroot; ?>adm/obrasConcluidasExcluirFoto/<?php echo $f["fotos_obras_concluidas"]["id"]; ?>/<?php echo $f["fotos_obras_concluidas"]["obra_concluida_id"]; ?>" title="Excluir">Excluir</a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php } ?>
    </div>
</div>