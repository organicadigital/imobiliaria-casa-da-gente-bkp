<h1  class="titulo">
    <span class="pagina">
        <span class="texto">Nós ligamos</span>
        <span class="linha_h2 tamanho7"></span>
    </span>
</h1>
<span class="divisoria1">
    <span class="divisoria2"></span>
</span>
<?php echo $this->element("busca_interna", array("fundo_f2f2f2"=>1)); ?>
<span class="divisoria1 sem_espaco">
    <span class="divisoria2"></span>
</span>
<section id="conteudo_contato" class="fundo_e6e6e6">
    <div class="pagina">
        <div class="coluna1">
            <h2>
                <span class="imagem"></span>
                <span class="pequeno">nós</span>
                <span class="grande">LIGAMOS</span>
            </h2>
        </div>
        <div class="coluna2">
            <p class="titulo_formulario">É só você preencher os campos abaixo e logo entraremos em contato.</p>
            <form action="javascript:envia_nos_ligamos()" method="post" id="formulario_contato">
                <div class="linha_formulario espaco">
                    <label for="nome_text">Nome:</label>
                    <input type="text" name="data[Contato][nome]" id="nome_text" />
                    <input type="hidden" name="data[Contato][imovel]" id="imovel_hidden" value="<?php echo $imovel; ?>" />
                </div>
                <div class="linha_formulario">
                    <label for="email_text">E-mail:</label>
                    <input type="text" name="data[Contato][email]" id="email_text" />
                </div>
                <div class="linha_formulario">
                    <label for="telefone_text">Telefone:</label>
                    <input type="text" name="data[Contato][telefone]" id="telefone_text" />
                </div>
                <div class="linha_formulario">
                    <label for="mensagem_textarea">Mensagem:</label>
                    <textarea name="data[Contato][mensagem]" id="mensagem_textarea"></textarea>
                </div>
                <div class="linha_formulario">
                    <label for="btn_contato" class="btn_contato">
                        <input type="submit" value="enviar" id="btn_contato" />
                    </label>
                </div>
            </form>
        </div>
    </div>
</section>