<script>
$(document).ready(function(){
    <?php if($_GET["cod"]){ ?>
    
    var posForm = $("#formulario-contato").position().top;
    if (posForm) {
    	$('html, body').animate({scrollTop: posForm}, 2000);
    	$("#nome_text").focus();
    }
    
    <?php } ?>
});
</script>
<h1  class="titulo">
    <span class="pagina">
        <span class="texto">Contato</span>
        <span class="linha_h2 tamanho5"></span>
    </span>
</h1>
<span class="divisoria1">
    <span class="divisoria2"></span>
</span>
<?php echo $this->element("busca_interna", array("fundo_f2f2f2"=>1)); ?>
<span class="divisoria1 sem_espaco">
    <span class="divisoria2"></span>
</span>
<section id="conteudo_contato" class="fundo_e6e6e6">
    <div class="pagina">
        <div class="coluna1">
            <span class="linha_horizontal"></span>
            <h2>
                <span class="imagem"></span>
                <span class="pequeno">atendimento por</span>
                <span class="grande">E-MAIL</span>
            </h2>
            <?php if(count($enderecos)>0){ ?>
            	<?php
				foreach($enderecos as $endereco){
					?>
					<section class="imobiliaria">
		                <h3><?php echo $endereco["c"]["nome"]; ?></h2>
		                    <p>
		                        <?php echo $endereco["enderecos"]["endereco"]; ?>
		                        <br /><?php echo $endereco["enderecos"]["telefone"]; ?>
		                        <br /><?php echo $endereco["enderecos"]["cep"]; ?>
		                    </p>
		                    <span class="interruptor"></span>
		                    <ul>
		                        <?php foreach($corretores as $corretor){ 
		                        	if ($corretor["corretores"]["filial"] == $endereco["enderecos"]["cidade_id"]) {
		                        	?>
			                        <li>
			                            <address>
			                                <?php echo $corretor["corretores"]["texto"]; ?>
			                            </address>
			                        </li>
		                        	<?php
		                        	}
		                         } ?>
		                    </ul>
		            </section>
					<?php
				};
			}; ?>
        </div>
        <div class="coluna2" id="formulario-contato">
            <p class="titulo_formulario">É só você preencher os campos abaixo e logo entraremos em contato.</p>
            <form action="javascript:envia_contato()" method="post" id="formulario_contato">
            	<input type="hidden" name="data[Contato][codigo]" value="<?php echo $_GET["cod"]; ?>" id="codigo_hidden"/>
                <div class="linha_formulario espaco">
                    <label for="nome_text">Nome:</label>
                    <input type="text" name="data[Contato][nome]" id="nome_text" />
                    <input type="hidden" name="data[Contato][imovel]" id="imovel_hidden" value="<?php echo $imovel; ?>" />
                </div>
                <div class="linha_formulario">
                    <label for="email_text">E-mail:</label>
                    <input type="text" name="data[Contato][email]" id="email_text" />
                </div>
                <div class="linha_formulario">
                    <label for="telefone_text">Telefone:</label>
                    <input type="text" name="data[Contato][telefone]" id="telefone_text" />
                </div>
                <div class="linha_formulario">
                    <label for="mensagem_textarea">Mensagem:</label>
                    <textarea name="data[Contato][mensagem]" id="mensagem_textarea"><?php if($_GET["cod"]){ ?>Gostaria de mais informa&ccedil;&otildees a respeito do im&oacute;vel <?php echo $_GET["cod"]; ?>.<?php } ?></textarea>
                </div>
                <div class="linha_formulario">
                    <label for="btn_contato" class="btn_contato">
                        <input type="submit" value="enviar" id="btn_contato" />
                    </label>
                </div>
            </form>
        </div>
    </div>
</section>