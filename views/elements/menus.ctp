<nav id="menu_secundario">
    <ul>
        <li class="sem_borda primeiro">
          
        
        </li>
        
    </ul>
</nav>
<nav id="menu_principal">
    <span class="canto_esquerdo"></span>
    <ul>
        <li class="primeiro">
            <a href="<?php echo $this->webroot; ?>imoveis/null-null-null/9/0" title="imóveis">Imóveis</a>
            <div class="submenu">
                <ul>
                    <?php foreach($cidades2 as $c){ ?>
                    <li>
                        <a href="<?php echo $this->webroot; ?>imoveis/<?php echo $c["tb_imoveis"]["nome"]; ?>-null-null/9/0" title="<?php echo $c["tb_imoveis"]["nome"]; ?>">
                            <span class="seta"></span>
                            <span class="tem_submenu"><?php echo $c["tb_imoveis"]["nome"]; ?></span>
                        </a>
                        <div class="submenu2">
                            <ul>
                                <li>
                                    <a href="<?php echo $this->webroot; ?>imoveis/<?php echo $c["tb_imoveis"]["nome"]; ?>-lancamentos-null/9/0" title="Lançamentos">Lançamentos</a>
                                </li>
                                <li>
                                    <a href="<?php echo $this->webroot; ?>imoveis/<?php echo $c["tb_imoveis"]["nome"]; ?>-ofertas-null/9/0" title="Ofertas">Ofertas</a>
                                </li>
                                <li>
                                    <a href="<?php echo $this->webroot; ?>imoveis/<?php echo $c["tb_imoveis"]["nome"]; ?>-condominios-null/9/0" title="Condomínios">Condomínios</a>
                                </li>
                                <?php
                                if(isset($tipos_cidade[$c["tb_imoveis"]["nome"]])){
                                    foreach($tipos_cidade[$c["tb_imoveis"]["nome"]] as $t){
                                ?>
                                <li>
                                    <a href="<?php echo $this->webroot; ?>imoveis/<?php echo $c["tb_imoveis"]["nome"]; ?>-<?php echo utf8_encode($t); ?>-null/9/0" title="<?php echo utf8_encode($t); ?> de <?php echo $c["tb_imoveis"]["nome"]; ?>"><?php echo utf8_encode($t); ?></a>
                                    <div class="submenu3">
                                        <ul>
                                            <li>
                                                <a href="<?php echo $this->webroot; ?>imoveis/<?php echo $c["tb_imoveis"]["nome"]; ?>-<?php echo utf8_encode($t); ?>-null/9/0" title="Todos <?php echo utf8_encode($t); ?> de <?php echo $c["tb_imoveis"]["nome"]; ?>">Todos</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo $this->webroot; ?>imoveis/<?php echo $c["tb_imoveis"]["nome"]; ?>-<?php echo utf8_encode($t); ?>-lancamentos/9/0" title="Lançamentos de <?php echo utf8_encode($t); ?> de <?php echo $c["tb_imoveis"]["nome"]; ?>">Lançamentos</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo $this->webroot; ?>imoveis/<?php echo $c["tb_imoveis"]["nome"]; ?>-<?php echo utf8_encode($t); ?>-ofertas/9/0" title="Ofertas de <?php echo utf8_encode($t); ?> de <?php echo $c["tb_imoveis"]["nome"]; ?>">Ofertas</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <?php }} ?>
                            </ul>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </li>
        <li>
            <a href="<?php echo $this->webroot; ?>lancamentos" title="lançamentos">Lançamentos</a>
            <div class="submenu">
                <ul>
                    <?php foreach($cidades2 as $c){ ?>
                    <li>
                        <a href="<?php echo $this->webroot; ?>imoveis/<?php echo $c["tb_imoveis"]["nome"]; ?>-lancamentos-null/9/0" title="Lançamentos em <?php echo $c["tb_imoveis"]["nome"]; ?>">
                            <span class="seta"></span>
                            <span class="tem_submenu"><?php echo $c["tb_imoveis"]["nome"]; ?></span>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </li>
        <li>
            <a href="<?php echo $this->webroot; ?>imoveis/null-condominios-null/9/0" title="condomínios">Condomínios</a>
            <div class="submenu">
                <ul>
                    <?php foreach($cidades2 as $c){ ?>
                    <li>
                        <a href="<?php echo $this->webroot; ?>imoveis/<?php echo $c["tb_imoveis"]["nome"]; ?>-condominios-null/9/0" title="Condomínios em <?php echo $c["tb_imoveis"]["nome"]; ?>">
                            <span class="seta"></span>
                            <span class="tem_submenu"><?php echo $c["tb_imoveis"]["nome"]; ?></span>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </li>
        <li>
            <a href="<?php echo $this->webroot; ?>imoveis/null-obras_concluidas-null/9/0" title="obras concluídas">Obras Concluídas</a>
            <div class="submenu"> </div>
        </li>
        <li>
            <a href="<?php echo $this->webroot; ?>vendaSeuImovel" title="venda seu imóvel">Venda seu Imóvel</a>
        </li>
        <li>
            <a href="<?php echo $this->webroot; ?>nossasLojas" title="Localização">Localização</a>
        </li>
        <li class="ultimo">
            <a href="<?php echo $this->webroot; ?>contato" title="contato">Contato</a>
        </li>
    </ul>
    <span class="canto_direito"></span>
</nav>